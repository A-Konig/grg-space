﻿using System;
using System.Collections.Generic;
using OpenTK;
using World.Core;
using World.Objects;

namespace World.Controllers
{
    /** Class used for computing shotest availible path
        contains modified A* algorithm from http://www.superscarysnakes.com/blackfuture/2016/10/26/basic-astar/ 
     */
    class PathingController
    {
        /** Map of the playfield, dividing it into cells */
        byte[,] map;

        /** Player */
        private Player player;
        Vector2 lastPos = new Vector2(-1, -1);

        /** Current found path */
        public List<Vector2> Currentpath { get => currentpath; set => currentpath = value; }
        List<Vector2> currentpath;

        /** Should a new path be found */
        public bool ToRepath { get => toRepath; set => toRepath = value; }
        private bool toRepath;

        /** Closed set */
        Dictionary<Vector2, bool> closedSet = new Dictionary<Vector2, bool>();
        /** Open set */
        Dictionary<Vector2, bool> openSet = new Dictionary<Vector2, bool>();

        /** Value of g - cost of path from start to node */
        Dictionary<Vector2, int> gScore = new Dictionary<Vector2, int>();
        /** Cost of f - cost of path from start to end through node */
        Dictionary<Vector2, int> fScore = new Dictionary<Vector2, int>();

        /** Used for reconstructing path */
        Dictionary<Vector2, Vector2> nodeLinks = new Dictionary<Vector2, Vector2>();
        /** Saved objects that should be processed in later times */
        List<GameObject> savedObjects = new List<GameObject>();

        /** Constructor */
        public PathingController(Player player)
        {
            this.player = player;
            ToRepath = true;
        }

        /** Find shortest path */
        public void FindPath(Vector2 pP, Vector2 oP)
        {
            // is player far from path
            FarFromPath();
            // is any object saved for later close enough
            IsSavedObjectClose();

            // should we repath
            if (!ToRepath)
                return;

            Console.WriteLine("Repath");

            closedSet = new Dictionary<Vector2, bool>();
            openSet = new Dictionary<Vector2, bool>();
            gScore = new Dictionary<Vector2, int>();
            fScore = new Dictionary<Vector2, int>();
            nodeLinks = new Dictionary<Vector2, Vector2>();

            currentpath = new List<Vector2>();
            currentpath = AStar(new Vector2(pP.Y, pP.X), new Vector2(oP.Y, oP.X));

            ToRepath = false;

            // WriteToConsole();
        }

        /** Go through all saved objects and check if they're close enough to warrant repathing */
        private void IsSavedObjectClose()
        {
            if (savedObjects.Count == 0)
                return;

            List<GameObject> toRemove = new List<GameObject>();
            foreach (GameObject o in savedObjects)
            {
                float oXp = o.Position.X + o.ColliderBox.X / 2f + o.Offset.X;
                float oXm = o.Position.X - o.ColliderBox.X / 2f + o.Offset.X;
                int oXpi = (int)(oXp / Config.CellSize);
                int oXmi = (int)(oXm / Config.CellSize);
                oXpi = Math.Min(Math.Max(0, oXpi), map.GetLength(1) - 1);
                oXmi = Math.Min(Math.Max(0, oXmi), map.GetLength(1) - 1);

                float oZp = o.Position.Z + o.ColliderBox.Z / 2f + o.Offset.Z;
                float oZm = o.Position.Z - o.ColliderBox.Z / 2f + o.Offset.Z;
                int oZpi = (int)(oZp / Config.CellSize);
                int oZmi = (int)(oZm / Config.CellSize);
                oZpi = Math.Min(Math.Max(0, oZpi), map.GetLength(0) - 1);
                oZmi = Math.Min(Math.Max(0, oZmi), map.GetLength(0) - 1);

                if (currentpath != null)
                {
                    int colPoint = -1;
                    for (int k = 0; k < currentpath.Count; k++)
                        if (currentpath[k].Y <= oXpi && currentpath[k].Y >= oXmi &&
                            currentpath[k].X <= oZpi && currentpath[k].X >= oZmi)
                        {
                            colPoint = k;
                            break;
                        }

                    // not on path anymore, should be removed from saved objects
                    if (colPoint == -1)
                        toRemove.Add(o);
                    // close enough to warrant repath
                    if (colPoint <= Config.PathTolerance)
                        toRepath = true;
                }
            }

            // remove not relevant objects
            foreach (GameObject r in toRemove)
                savedObjects.Remove(r);
        }

        /** Write map to console */
        public void WriteToConsole()
        {
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (currentpath.Contains(new Vector2(i, j)))
                        Console.Write("x ");
                    else
                        Console.Write(map[i, j] + " ");

                }
                Console.WriteLine();
            }
        }

        /** Create representation of static map 
            colliding - all collidable game objects */
        internal void InitializeStaticMap(List<GameObject> colliding)
        {
            int x = Config.CoarseMap.GetLength(1) * 6;
            int y = Config.CoarseMap.GetLength(0) * 6;

            map = new byte[y, x];

            for (int i = 0; i < y; i++)
                for (int j = 0; j < x; j++)
                    if (Config.CoarseMap[(i / 6), (j / 6)] == 1)
                        map[i, j] = 0;
                    else
                        map[i, j] = 1;

            foreach (GameObject o in colliding)
                AddObjectToMap(o);
        }

        /**
         Add object into map
         x - map x length
         y- map y length
         o - object
        */
        public void AddObjectToMap(GameObject o)
        {
            int x = map.GetLength(1);
            int y = map.GetLength(0);

            Vector2 mapPos = MathFunc.GetPositionInMap(o.Position);

            if (mapPos.X >= x || mapPos.Y >= y)
                return;

            if (o.ColliderBox.X == 0 && o.ColliderBox.Z == 0)
                return;

            // how much space it occupies in grid
            float oXp = o.Position.X + o.ColliderBox.X / 2f + o.Offset.X;
            float oXm = o.Position.X - o.ColliderBox.X / 2f + o.Offset.X;
            int oXpi = (int)(oXp / Config.CellSize);
            int oXmi = (int)(oXm / Config.CellSize);
            oXpi = Math.Min(Math.Max(0, oXpi), map.GetLength(1) - 1);
            oXmi = Math.Min(Math.Max(0, oXmi), map.GetLength(1) - 1);

            float oZp = o.Position.Z + o.ColliderBox.Z / 2f + o.Offset.Z;
            float oZm = o.Position.Z - o.ColliderBox.Z / 2f + o.Offset.Z;
            int oZpi = (int)(oZp / Config.CellSize);
            int oZmi = (int)(oZm / Config.CellSize);
            oZpi = Math.Min(Math.Max(0, oZpi), map.GetLength(0) - 1);
            oZmi = Math.Min(Math.Max(0, oZmi), map.GetLength(0) - 1);


            if (MathFunc.IsInMapRange(o))
                for (int i = oXmi; i <= oXpi; i++)
                    for (int j = oZmi; j <= oZpi; j++)
                        map[j, i]++;


            // TODO tady zjistim i "jak daleko" -> jenom když dost blízko, jinak s tim udělam ale co
            // will this object influence path

            if (currentpath != null)
            {
                int colPoint = -1;
                for (int k = 0; k < currentpath.Count; k++)
                    if (currentpath[k].Y <= oXpi && currentpath[k].Y >= oXmi &&
                        currentpath[k].X <= oZpi && currentpath[k].X >= oZmi)
                    {
                        colPoint = k;
                        Console.WriteLine("Added relevant to dynamic objects");
                        break;
                    }

                // close enough to warrant repath
                if (colPoint <= Config.PathTolerance)
                    toRepath = true;
                // saved for later
                else if (colPoint >= 0)
                {
                    savedObjects.Add(o);
                    Console.WriteLine(colPoint);
                }
            }
        }

        /** Remove an object from map */
        public void RemoveObjectFromMap(GameObject o)
        {
            int x = map.GetLength(1);
            int y = map.GetLength(0);

            Vector2 mapPos = (o.Position / Config.CellSize).Xz;

            if (mapPos.X >= x || mapPos.Y >= y)
                return;

            if (o.ColliderBox.X == 0 && o.ColliderBox.Z == 0)
                return;

            // how much space it occupies
            float oXp = o.Position.X + o.ColliderBox.X / 2f + o.Offset.X;
            float oXm = o.Position.X - o.ColliderBox.X / 2f + o.Offset.X;
            int oXpi = (int)(oXp / Config.CellSize);
            int oXmi = (int)(oXm / Config.CellSize);
            oXpi = Math.Min(Math.Max(0, oXpi), map.GetLength(1) - 1);
            oXmi = Math.Min(Math.Max(0, oXmi), map.GetLength(1) - 1);

            float oZp = o.Position.Z + o.ColliderBox.Z / 2f + o.Offset.Z;
            float oZm = o.Position.Z - o.ColliderBox.Z / 2f + o.Offset.Z;
            int oZpi = (int)(oZp / Config.CellSize);
            int oZmi = (int)(oZm / Config.CellSize);
            oZpi = Math.Min(Math.Max(0, oZpi), map.GetLength(0) - 1);
            oZmi = Math.Min(Math.Max(0, oZmi), map.GetLength(0) - 1);

            if (MathFunc.IsInMapRange(o))
                for (int i = oXmi; i <= oXpi; i++)
                    for (int j = oZmi; j <= oZpi; j++)
                        map[j, i]--;

            // new better path could've opened, always need to find a new path
            Console.WriteLine("Removed from dynamic objects");
            if (savedObjects.Contains(o))
                savedObjects.Remove(o);
            ToRepath = true;
        }

        /** Is player too far from path to potentially need a repathing */
        internal bool FarFromPath()
        {
            Vector2 mapPos = MathFunc.GetPositionInMap(player.Position);

            if (lastPos.X < 0)
            {
                lastPos = mapPos;
                ToRepath = true;
                return true;
            }

            // if player left last cell they were in
            if (lastPos[0] != mapPos[0] || lastPos[1] != mapPos[1])
            {
                if (currentpath.Count == 0)
                    return false;

                // moved to next cell in path
                // remove cell from path
                if ((currentpath[0].Y == mapPos[0] && currentpath[0].X == mapPos[1]))
                    currentpath.RemoveAt(0);

                // moved to different cell too far from path
                else if (!(Heuristic(new Vector2(mapPos[1], mapPos[0]), currentpath[0]) < Config.CellTollerance))
                {
                    lastPos = mapPos;
                    ToRepath = true;
                    return true;
                }

            }

            lastPos = mapPos;
            return false;
        }

        /** A* algorithm */
        public List<Vector2> AStar(Vector2 start, Vector2 goal)
        {
            openSet[start] = true;
            gScore[start] = 0;
            fScore[start] = Heuristic(start, goal);

            while (openSet.Count > 0)
            {
                Vector2 current = nextBest();
                if (current.Equals(goal))
                    return Reconstruct(current);

                openSet.Remove(current);
                closedSet[current] = true;

                IEnumerable<Vector2> neighbors = Neighbors(current);
                foreach (Vector2 neighbor in neighbors)
                {
                    if (closedSet.ContainsKey(neighbor))
                        continue;

                    int projectedG = getGScore(current) + 1;

                    if (openSet.ContainsKey(neighbor) && projectedG >= getGScore(neighbor))
                        continue;

                    openSet[neighbor] = true;
                    nodeLinks[neighbor] = current;
                    gScore[neighbor] = projectedG;
                    fScore[neighbor] = projectedG + Heuristic(neighbor, goal);
                }
            }

            return new List<Vector2>();
        }

        /** Heuristic */
        private int Heuristic(Vector2 start, Vector2 goal)
        {
            var dx = goal.X - start.X;
            var dy = goal.Y - start.Y;
            return (int) (Math.Abs(dx) + Math.Abs(dy));
        }

        /** Get g value of node */
        private int getGScore(Vector2 pt)
        {
            int score = int.MaxValue;
            gScore.TryGetValue(pt, out score);
            return score;
        }

        /** Get f value of node */
        private int getFScore(Vector2 pt)
        {
            int score = int.MaxValue;
            fScore.TryGetValue(pt, out score);
            return score;
        }

        /** Find all neighbors */
        public IEnumerable<Vector2> Neighbors(Vector2 center)
        {
            Vector2 pt = new Vector2(center.X, center.Y - 1);
            if (IsValidNeighbor(pt))
                    yield return pt;

            //middle row
            pt = new Vector2(center.X - 1, center.Y);
            if (IsValidNeighbor(pt))
                    yield return pt;

            pt = new Vector2(center.X + 1, center.Y);
            if (IsValidNeighbor(pt))
                    yield return pt;

            //bottom row
            pt = new Vector2(center.X, center.Y + 1);
            if (IsValidNeighbor(pt))
                    yield return pt;
        }

        /** Can node be traversed */
        public bool IsValidNeighbor(Vector2 to)
        {
            float x = to.X;
            float y = to.Y;
            if (x < 0 || x >= map.GetLength(0))
                return false;

            // to int
            int xI = (int)to.X;
            int yI = (int)to.Y;

            if (y < 0 || y >= map.GetLength(1))
                return false;

            // x y in map inverted
            return map[xI,yI] == 0;
        }

        /** Reconstruct path */
        private List<Vector2> Reconstruct(Vector2 current)
        {
            List<Vector2> path = new List<Vector2>();
            while (nodeLinks.ContainsKey(current))
            {
                path.Add(current);
                current = nodeLinks[current];
            }

            path.Reverse();
            return path;
        }

        /** Next best node */
        private Vector2 nextBest()
        {
            int best = int.MaxValue;
            Vector2 bestPt = new Vector2(-1,-1);
            foreach (var node in openSet.Keys)
            {
                var score = getFScore(node);
                if (score < best)
                {
                    bestPt = node;
                    best = score;
                }
            }
            
            return bestPt;
        }

    }
}
