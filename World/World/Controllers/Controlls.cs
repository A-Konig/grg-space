﻿using OpenTK.Input;
using OpenTK;
using World.Core;
using System;

namespace World.Controllers
{
    /** Class responsible for reactions of user input */
    class Controlls
    {
        /** Movement speed */
        float speed = 1.5f;
        /** Mouse sensitivity */
        float sensitivity = 0.03f;
        /** First movement of game */
        bool firstMove = true;
        /** Is user crouched */
        bool crouched = false;

        /** Last position of mouse */
        Vector2 lastPos;
        /** Last keyboard state */
        KeyboardState lastKeys;

        /** Gameplay controller */
        GameplayController gc;
        /** Rendered scene */
        Scene scene;
        /** Movement controller */
        MovementController mc;

        /** Constructor
            gc - used game controller
            scene - rendered scene
            mc - used movement controller
        */
        public Controlls(GameplayController gc, Scene scene, MovementController mc)
        {
            this.gc = gc;
            this.scene = scene;
            this.mc = mc;
        }

        /** Reacts to keyboard input. Enables movement of player in map
            Reacts to keys:
                wsad, shift, space - movement
                e - collect
                f - turn on/off flashlight
                esc - close game
            time - time since last call
            noClip - is noclip enabled
         */
        public bool ReactToKeyboard(float time, bool noClip)
        {

            KeyboardState input = Keyboard.GetState();
            if (input.IsKeyDown(Key.Escape))
                return true;

            // collect
            if (input.IsKeyDown(Key.E))
                gc.Collect(scene.player.Position, scene.player.Reach);

            Vector3 directionVector = new Vector3();
            Vector3 newPos = scene.mainCam.Position;

            // moving
            if (input.IsKeyDown(Key.W))
                directionVector += scene.mainCam.Forward;

            if (input.IsKeyDown(Key.S))
                directionVector -= scene.mainCam.Forward;

            if (input.IsKeyDown(Key.A))
                directionVector -= Vector3.Normalize(Vector3.Cross(scene.mainCam.Forward, Config.WorldUp));

            if (input.IsKeyDown(Key.D))
                directionVector += Vector3.Normalize(Vector3.Cross(scene.mainCam.Forward, Config.WorldUp));

            if (input.IsKeyDown(Key.Space))
                if (noClip)
                    directionVector += Config.WorldUp;

            if (input.IsKeyDown(Key.LShift))
                if (noClip)
                    directionVector -= Config.WorldUp;
                else if (crouched && !lastKeys.IsKeyDown(Key.LShift))
                {
                    newPos.Y *= 2;
                    crouched = !crouched;
                } else if (!crouched && !lastKeys.IsKeyDown(Key.LShift))
                {
                    newPos.Y -= newPos.Y / 2;
                    crouched = !crouched;
                }
            
            newPos += directionVector * speed * time;

            // flashlight
            if (input.IsKeyDown(Key.F) && !lastKeys.IsKeyDown(Key.F))
                scene.ToggleFlash();

            lastKeys = input;

            // crouch
            if (mc.YCollisionDetection(newPos, scene.player, scene.Objects))
                crouched = !crouched;

            // collision with objects
            newPos = mc.Collision(newPos, scene);

            scene.mainCam.Position = newPos;
            scene.player.Position = newPos;

            Vector2 pos = MathFunc.GetPositionInMap(scene.player.Position);
            scene.player.SetMapPosition((int)pos[0], (int)pos[1]);

            return false;
        }

        /** Reacts to mouse input
            width - width of window
            height - height of window */
        public void ReactToMouse(double width, double height)
        {
            Mouse.SetPosition(width, height);
            var mouse = Mouse.GetState();

            if (firstMove)
            {
                lastPos = new Vector2(mouse.X, mouse.Y);
                firstMove = false;
            }
            else
            {
                float deltaX = mouse.X - lastPos.X;
                float deltaY = mouse.Y - lastPos.Y;

                lastPos = new Vector2(mouse.X, mouse.Y);

                scene.mainCam.yaw += deltaX * sensitivity;
                scene.mainCam.pitch -= deltaY * sensitivity;

                if (scene.mainCam.pitch > 89.0f)
                    scene.mainCam.pitch = 89.0f;
                else if (scene.mainCam.pitch < -89.0f)
                    scene.mainCam.pitch = -89.0f;
            }

            scene.mainCam.UpdateAxes();
        }

    }
}
