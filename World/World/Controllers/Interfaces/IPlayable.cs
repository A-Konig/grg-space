﻿using OpenTK;

namespace World.Controllers
{
    /** Interface for game class */
    interface IPlayable
    {
        /** Executes once on load of application */
        bool OnLoad();

        /** Executes every time a frame is updated */
        bool OnUpdateFrame(FrameEventArgs e);

        /** Executes upon resizing the window */
        bool OnResize(int widht, int height);
        
        /** Executes every time a frame is rendered */
        bool RenderFrame();
    }
}
