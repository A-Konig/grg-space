﻿using OpenTK;
using World.Objects;
using World.Core;
using System.Diagnostics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using World.Objects.GameObjects;

namespace World.Controllers
{
    /** Class representing the game */
    class Game : IPlayable
    {
        /** Is noclip turner on */
        bool noClip;

        /** Camera */
        Camera cam;
        /** Player */
        Player player;
        /** Currently rendered scene */
        Scene scene;

        /** Movement controller */
        MovementController mc;
        /** Gameplay controller */
        GameplayController gc;
        /** Controlls */
        Controlls controlls;

        /** Stopwatch */
        Stopwatch watch = new Stopwatch();
        /** Current time */
        long current;
        /** Last time */
        long lastFrameStat = 0;
        /** Frame count */
        long frame = 0;
        /** Fps */
        float fps;

        /** Pathing controller */
        PathingController pc;
        /** Which emittors are active and which are not */
        byte[] activeSystems;


        /** Constructor */
        public Game(bool noClip)
        {
            this.noClip = noClip;

            mc = new MovementController();
            Vector2 pos = MathFunc.GetPositionInMap(Config.StartPos);

            cam = new Camera(Config.StartPos, Config.StartRot);
            player = new Player(Config.StartPos, pos);

            scene = new Scene(cam, player);

            pc = new PathingController(player);
            gc = new GameplayController(pc);
            controlls = new Controlls(gc, scene, mc);

            watch.Start();
        }


        /** Executes once on load of application */
        public bool OnLoad()
        {
            if (!scene.CreateObjects())
            {
                return false;
            }
            gc.objG = scene.mapSetup.Objectives[0];
            gc.objR = scene.mapSetup.Objectives[1];
            gc.SetMapPositions(0);

            // objects in scene
            List<GameObject> colliding = new List<GameObject>();
            colliding.AddRange(scene.Objects);
            colliding.AddRange(scene.TransparentObjects);
            activeSystems = new byte[scene.mapSetup.Systems.Count];

            pc.InitializeStaticMap(colliding);

            pc.FindPath(player.GetMapPosition(), gc.GetCurrentObjective());
            Vector2 pP = player.GetMapPosition();
            Vector2 oP = gc.GetCurrentObjective();

            Console.WriteLine("from: " + pP);
            Console.WriteLine("to: " + oP);

            //GL.Enable(EnableCap.FramebufferSrgb);

            List<Vector2> path = pc.AStar(new Vector2(pP.Y, pP.X), new Vector2(oP.Y, oP.X)); //new Vector2(4, 1), new Vector2(2, 1));
            foreach (Vector2 v in path)
                Console.WriteLine(v.ToString());

            return true;
        }

        /** Executes upon resizing the window */
        public bool OnResize(int width, int height)
        {
            Config.WindowWidht = width;
            Config.WindowHeight = height;
            cam.ComputeProjectionMatrix(width, height);

            return true;
        }

        /** Executes every time a frame is rendered */
        public bool RenderFrame()
        {
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);

            // shadows
            if (!RenderShadowMaps())
                return false;

            // objects
            if (!RenderGameObjects())
                return false;

            return true;
        }

        /** Compute fps count */
        private void FrameCount()
        {
            current = watch.ElapsedMilliseconds;
            frame++;

            if (current > lastFrameStat + 1000)
            {
                fps = (1000 * frame / (current - lastFrameStat));

                lastFrameStat = current;
                frame = 0;

            }
        }

        /** Render game objects */
        public bool RenderGameObjects()
        {
            FrameCount();
            cam.ComputeViewMatrix();

            // spawn random obstacles
            gc.SpawnRandoms(scene, mc);

            // render
            scene.SetBuffer(0);
            if (!scene.DrawScene(false))
                return false;

            CheckParticleSystems();
                
            return true;
        }

        /** Check the state of particle systems, add active ones to dynamic map in pathing controller, newly dead ones get removed from it */
        private void CheckParticleSystems()
        {
            List<ParticleSystem> s = scene.mapSetup.Systems;
            byte[] newActiveSystems = new byte[activeSystems.Length];
            for (int i = 0; i < s.Count; i++)
            {
                GameObject o = s[i];

                if (s[i].alive && activeSystems[i] == 0)
                {
                    pc.AddObjectToMap(o);
                    Console.WriteLine("new alive");
                    o.Active = true;
                }
                else if (!s[i].alive && activeSystems[i] == 1)
                {
                    o.Active = false;
                    pc.RemoveObjectFromMap(o);
                }

                newActiveSystems[i] = (byte)(s[i].alive ? 1 : 0);
            }
            activeSystems = newActiveSystems;

        }

        /** Compute shadows */
        public bool RenderShadowMaps()
        {
            if (!scene.DrawLights())
                return false;

            return true;
        }

        /** Executes every time a frame is updated */
        public bool OnUpdateFrame(FrameEventArgs e)
        {
            if (gc.IsCloseToObjective(player.Position, player.Reach))
                scene.Update(fps, "(E)");
            else
                scene.Update(fps, "");

            NavigationUpdate();
            
            // react to input
            Config.LastUpdateDelta = (float)e.Time;
            if (controlls.ReactToKeyboard((float)e.Time, noClip))
                return true;
            controlls.ReactToMouse(Config.WindowWidht, Config.WindowHeight);

            return false;
        }

        /** Update navigation */
        private void NavigationUpdate()
        {
            pc.FindPath(player.GetMapPosition(), gc.GetCurrentObjective());
            scene.UpdateNavigation(pc.Currentpath);
        }
    }
}
