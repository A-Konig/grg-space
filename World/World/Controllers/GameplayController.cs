﻿using System;
using World.Objects;
using OpenTK;
using World.Core;
using World.Objects.Controllable;
using System.Collections.Generic;

namespace World.Controllers
{
    /** Class coontrolling the game events */
    class GameplayController
    {
        /** Started current mission */
        bool started;
        /** Number of finished missions */
        int missionCount;

        /** Start coordinates in gridmap of current start of mission */
        int sX, sY;
        /** Start coordinates in gridmap of current end of mission */
        int eX, eY;

        /** End of mission */
        public Objective objR;
        /** Start of mission */
        public Objective objG;

        /** Pathing controller */
        public PathingController pc;

        /** Curretntly spawned dynamic objects */
        public List<RandomSpawns> Spawns = new List<RandomSpawns>();

        /** Constructor */
        public GameplayController(PathingController pc)
        {
            this.pc = pc;

            missionCount = 0;
            missionCount++;
        }

        /** Set positions for start and end mission objectives */
        public void SpawnMission()
        {
            objR.Active = false;
            objG.Active = true;

            SetMapPositions((missionCount % 2) * 2);

            objG.SetTransform(Config.ObjectivePositions[(missionCount % 2) * 2], new Vector3(0, Config.ObjectiveRotations[(missionCount % 2) * 2], 0));
            objR.SetTransform(Config.ObjectivePositions[(missionCount % 2) * 2 + 1], new Vector3(0, Config.ObjectiveRotations[(missionCount % 2) * 2 + 1], 0));

            missionCount++;
        }

        /** Set positions of current start and end objectives in map */
        public void SetMapPositions(int v)
        {
            sX = (int)Config.ObjectivePositionsMap[v].X;
            sY = (int)Config.ObjectivePositionsMap[v].Y;

            eX = (int)Config.ObjectivePositionsMap[v + 1].X;
            eY = (int)Config.ObjectivePositionsMap[v + 1].Y;
        }

        /** Toggle if current mission has started */
        public void ToggleMission()
        {
            started = !started;
            pc.ToRepath = true;
        }

        /** Get current objective grid position */
        internal Vector2 GetCurrentObjective()
        {
            if (started)
                return new Vector2(eX, eY);

            return new Vector2(sX, sY);
        }

      
        /** Try to collect objective */
        internal void Collect(Vector3 position, float reach)
        {

            // collecting start
            if ((!started))
            {
                Vector3 start = objG.Position;
                double dist = new Vector3(start.X - position.X, 0, start.Z - position.Z).Length;

                if (dist < reach)
                {
                    objG.Active = false;
                    objR.Active = true;
                    ToggleMission();
                }

                //Console.WriteLine(dist + " vs " + reach);
            }

            // collecting end
            else if (started)
            {
                Vector3 end = objR.Position;
                double dist = new Vector3(end.X - position.X, 0, end.Z - position.Z).Length;

                if (dist < reach)
                {
                    objR.Active = false;
                    ToggleMission();
                    SpawnMission();
                }
            }
        }

        /** Is player close enough to objective to collect */
        internal bool IsCloseToObjective(Vector3 position, float reach)
        {
            // collecting end
            if ((!started))
            {
                Vector3 start = objG.Position;
                double dist = Vector3.Distance(start, position);

                if (dist < reach)
                    return true;
            }

            // collecting start
            else if (started)
            {
                Vector3 end = objR.Position;
                double dist = Vector3.Distance(end, position);

                if (dist < reach)
                    return true;
            }

            return false;
        }

        /** Randomly spawn one of barrier objects */
        internal void SpawnRandoms(Scene scene, MovementController mc)
        {
            // decide if old ones should be despawned
            List<RandomSpawns> toRemove = new List<RandomSpawns>();
            foreach (RandomSpawns obj in Spawns)
                if (obj.life <= 0)
                {
                    obj.RemoveSelfFromScene(scene);
                    toRemove.Add(obj);

                    pc.RemoveObjectFromMap(obj.randomObject);
                }
                else
                    obj.DecreaseLife();

            foreach (RandomSpawns obj in toRemove)
                Spawns.Remove(obj);


            // spawning new ones on set positions
            Random r = new Random();
            if (r.NextDouble() > Config.ProbabilityThreshold)
            {
                // choose one object to spawn
                int index = r.Next(scene.mapSetup.RandomObjects.Count);

                // how to choose life length
                int life = r.Next(500) + 1000;

                // not already spawned
                GameObject obj = scene.mapSetup.RandomObjects[index];
                if (!scene.Objects.Contains(obj))
                {
                    Vector2 p = MathFunc.GetPositionInMap(obj.Position);
                    obj.MapPos = new Vector2(p[0], p[1]);

                    // player is not in its space
                    if (MathFunc.CheckPosCompatibility(scene.player, obj))
                    {
                        RandomSpawns spawn = new RandomSpawns(obj, life);
                        Spawns.Add(spawn);
                        Console.WriteLine("Changed dynamic objects");

                        scene.Objects.Add(obj);
                        pc.AddObjectToMap(obj);
                    }

                }
            }

        }
    }
}
