﻿using OpenTK;
using System.Collections.Generic;
using World.Controllers;
using World.Objects;

namespace World
{
    /** Class controlling the movement of player os it is within certain boundaries */
    class MovementController
    {

        /** Detects collision of player with game objects, using aabb algorithm
            newPos - new player position
            player - player
            objects - game objects */
        private bool CollisionDetection(Vector3 newPos, Player player, List<GameObject> objects)
        {
            // axes aligned box
            float pXp = newPos.X + player.ColliderBox.X / 2f; float pXm = newPos.X - player.ColliderBox.X / 2f;
            float pZp = newPos.Z + player.ColliderBox.Z / 2f; float pZm = newPos.Z - player.ColliderBox.Z / 2f;
            float pYp = newPos.Y + player.ColliderBox.Y / 2f; float pYm = newPos.Y - player.ColliderBox.Y / 2f;

            foreach (GameObject o in objects)
            {
                if ((!o.Active) || (o.Interactalbe))
                    continue;

                float oXp = o.Position.X + o.ColliderBox.X / 2f + o.Offset.X; float oXm = o.Position.X - o.ColliderBox.X / 2f + o.Offset.X;
                float oZp = o.Position.Z + o.ColliderBox.Z / 2f + o.Offset.Z; float oZm = o.Position.Z - o.ColliderBox.Z / 2f + o.Offset.Z;
                float oYp = o.Position.Y + o.ColliderBox.Y / 2f + o.Offset.Y; float oYm = o.Position.Y - o.ColliderBox.Y / 2f + o.Offset.Y;

                // collision with o
                if ((pXm <= oXp && pXp >= oXm) && (pYm <= oYp && pYp >= oYm) && (pZm <= oZp && pZp >= oZm))
                    return true;
            }

            // no collision
            return false;
        }

        /** Detects collision with objects in scene, if collision detected, adjusts new position so it is in a place where no collision occurs
            newPos - new position
            scene - rendered scene */
        internal Vector3 Collision(Vector3 newPos, Scene scene)
        {
            List<GameObject> colliding = new List<GameObject>();
            colliding.AddRange(scene.Objects);
            colliding.AddRange(scene.TransparentObjects);
            colliding.AddRange(scene.Walls);
            colliding.AddRange(scene.mapSetup.Systems);

            // is collision
            if (!CollisionDetection(newPos, scene.player, colliding))
                return newPos;
            else
            {
                // if no movement in Z direction
                Vector3 xMove = new Vector3(newPos.X, newPos.Y, scene.player.Position.Z);
                if (!CollisionDetection(xMove, scene.player, colliding))
                    return xMove;

                // if no movement in X position
                Vector3 zMove = new Vector3(scene.player.Position.X, newPos.Y, newPos.Z);
                if (!CollisionDetection(zMove, scene.player, colliding))
                    return zMove;

            }

            return scene.player.Position;
        }
        
        /** Detects collision with game objectsin Y axis only
            newPos - new position
            player - player
            objects - game objects */
        internal bool YCollisionDetection(Vector3 newPos, Player player, List<GameObject> objects)
        {
            float pXp = player.Position.X + player.ColliderBox.X / 2f; float pXm = player.Position.X - player.ColliderBox.X / 2f;
            float pZp = player.Position.Z + player.ColliderBox.Z / 2f; float pZm = player.Position.Z - player.ColliderBox.Z / 2f;
            float pYp = newPos.Y + player.ColliderBox.Y / 2f; float pYm = newPos.Y - player.ColliderBox.Y / 2f;

            foreach (GameObject o in objects)
            {
                if ((!o.Active) || (o.Interactalbe))
                    continue;

                float oXp = o.Position.X + o.ColliderBox.X / 2f + o.Offset.X; float oXm = o.Position.X - o.ColliderBox.X / 2f + o.Offset.X;
                float oZp = o.Position.Z + o.ColliderBox.Z / 2f + o.Offset.Z; float oZm = o.Position.Z - o.ColliderBox.Z / 2f + o.Offset.Z;
                float oYp = o.Position.Y + o.ColliderBox.Y / 2f + o.Offset.Y; float oYm = o.Position.Y - o.ColliderBox.Y / 2f + o.Offset.Y;

                // collision with o
                if ((pXm <= oXp && pXp >= oXm) && (pYm <= oYp && pYp >= oYm) && (pZm <= oZp && pZp >= oZm))
                    return true;
            }

            // no collision
            return false;
        }

    }
}