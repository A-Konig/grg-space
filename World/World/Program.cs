﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using World.Controllers;
using World.Core;
using World.Core.Helpers;

namespace World
{
    /** Main class representing the application. */
    class Program : GameWindow
    {
        /** Game that will be run */
        IPlayable game;
        /** Is game running in debug mode */
        bool debugMode;
        bool exit;

        /** Constructor */
        public Program(int width, int height, GraphicsMode mode, string title, bool debugMode)
            : base(width, height, mode, title, GameWindowFlags.Default)
        { 
            this.debugMode = debugMode;
            if (!ErrorCheck.TestVersion())
                exit = true;

        }

        /** Executes once on load of application */
        protected override void OnLoad(EventArgs e)
        {
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.ColorMaterial);
            GL.Enable(EnableCap.Lighting);

            game = new Game(debugMode);

            if (exit)
            {
                Console.WriteLine("Not suffictient versions of openGL or glsl.");
                Exit();
            } else
            {
                Console.WriteLine("Creating scene...");
                if (!game.OnLoad())
                {
                    Console.WriteLine("Error while loading scene.");
                    Console.WriteLine("\t" + ErrorCheck.CurrentlyProcessingMSG);
                    Exit();
                }

                Context.SwapInterval = 1;
                CursorVisible = false;
                CursorGrabbed = true;
            }

            base.OnLoad(e);
        }

        /** Executes upon resizing the window */
        protected override void OnResize(EventArgs e)
        {
            if (!game.OnResize(Width, Height))
            {
                Console.WriteLine("Error while resizing window. Last known operation:");
                Console.WriteLine("\t" + ErrorCheck.CurrentlyProcessingMSG);
                Exit();
            }

            base.OnResize(e);
        }

        /** Executes every time a frame is rendered */
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            if (!game.RenderFrame())
            {
                Console.WriteLine("Error while rendering scene. Last known operation:");
                Console.WriteLine("\t" + ErrorCheck.CurrentlyProcessingMSG);
                Exit();
            }

            Context.SwapBuffers();
            base.OnRenderFrame(e);
        }

        /** Executes every time a frame is updated */
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            if (!Focused) // check to see if the window is focused
                return;

            if (game.OnUpdateFrame(e))
                Exit();

            base.OnUpdateFrame(e);
        }

        /** Main method */
        static void Main(string[] args)
        {
            ParameterLoader.ReadConfig();
            bool debugMode = ParameterLoader.ProcessArgs(args);

            using (Program program = new Program(Config.WindowWidht, Config.WindowHeight, new GraphicsMode(new ColorFormat(32), 24, 8, 16), "Spaceship", debugMode))
            {
                program.Run(0, 0);
            }
        }
    }
}