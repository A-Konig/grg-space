﻿#version 420

struct Material {
	int dampening;
	float intensity;
	vec3 specColor;
	sampler2D diffuseTex;
	sampler2D specularTex;
};

struct Light {
	vec4 position;
	vec3 attenuation;
	vec3 direction;
	float cutoff;
	mat4 matrix;
	float brightness;
};

// ambient lighting
uniform float brightness;
// object material
uniform Material material;
// shadow texture
uniform sampler2DArrayShadow shadowTex;

// in lights
in Light flashlight;
in Light[2] lightArr;
// position of fragment from lights point of view (projected)
in vec4[3] fragLightPos;
// position of fragment from cameras point of view
in vec3 camViewModelPos;

// tex coord and normal
in vec2 texCoordPrimary;
in vec3 surfaceNormal;

// out color
out vec4 glFragColor;

// compute value of shadow in this fragment, take into account only current light
// positionLight - position of the light
// l - index of the light
float ShadowTest(vec4 positionLight, int l) {
	// look at depth at given position - project into depth
	// depth stored [0, 1]
	// offset 0.01 to prevent shadow acne
	vec3 shadowCoords = (positionLight.xyz-vec3(0,0,0.01))/positionLight.w;
	shadowCoords = shadowCoords * 0.5 + 0.5;
	vec3 size = textureSize(shadowTex,0) / 2.4;

	// sampling more neihgbouring fragments
	// using poissonDisk sampling
	float c = cos(positionLight.length*100);
	float s = sin(positionLight.length*100);
	mat3 rotZ = mat3(c, -s, 0, s, c, 0, 0, 0, 1);

	vec3 poissonDisk[8];
	poissonDisk[0] = rotZ * vec3(-0.613392 / size.x, 0.617481 / size.y, 0);
	poissonDisk[1] = rotZ * vec3(0.170019 / size.x, -0.040254 / size.y, 0);
	poissonDisk[2] = rotZ * vec3(-0.299417 / size.x, 0.791925 / size.y, 0);
	poissonDisk[3] = rotZ * vec3(0.645680 / size.x, 0.493210 / size.y, 0);
	poissonDisk[4] = rotZ * vec3(-0.651784 / size.x, 0.717887 / size.y, 0);
	poissonDisk[5] = rotZ * vec3(0.421003 / size.x, 0.027070 / size.y, 0);
	poissonDisk[6] = rotZ * vec3(-0.817194 / size.x, -0.271096 / size.y, 0);
	poissonDisk[7] = rotZ * vec3(-0.705374 / size.x, -0.668203 / size.y, 0);

	float lightDepth = 0;
	for (int s = 0; s <poissonDisk.length;s++) {
		vec4 coord;
		coord.xyw = shadowCoords.xyz + poissonDisk[s];
		coord.z = l;
		// lookup shadow
		lightDepth += texture(shadowTex, coord);
	}
	
	return lightDepth/poissonDisk.length;
}

// compute the value of lighting at this fragment
// light - light
// normal - normal
// diffTex - diffuse texture
// color - light color
// posLight - index of light
vec4 ComputeLight(Light light, vec3 normal, vec4 diffTex, vec4 color, int posLight) {
	vec3 dirToLight = normalize(light.position.xyz);
	vec3 viewer = normalize(camViewModelPos);
	float attenuation = light.attenuation.x;
	float distance = 1;

	// if light has position in space (is not a vector)
	if (light.position.w > 0.1)
	{
		// direction from fragment to light
		dirToLight = light.position.xyz - camViewModelPos;
		
		// distance between fragment and light
		distance = length(dirToLight);
		dirToLight = normalize(dirToLight);

		// compute attenuation
		attenuation = 1/(light.attenuation.x + light.attenuation.y * distance + light.attenuation.z * distance * distance);
		
		// angle between direction and light direction
		float angle = dot(-dirToLight, normalize(light.direction));
		if (angle < light.cutoff) {
			attenuation = 0;
		}
	}

	// shadows
	float shadow = 1; 
	shadow = ShadowTest(fragLightPos[posLight], posLight);

	// diffuse lighting - diffuse texture times computed lighting
	float diffuseLighting = max(0, dot(normal, dirToLight));
	vec4 diffuseTot = light.brightness * diffTex * diffuseLighting;

	// specular lighting - specular texture times computed lighting
	vec3 reflected = reflect(dirToLight, normal);
	float specularLighting = pow(max(0, dot(reflected, viewer)), material.dampening);
	vec4 specularTot = vec4(material.specColor, 1) * vec4(specularLighting * material.intensity * diffTex);

	vec4 res = (diffuseTot + specularTot) * shadow * attenuation * color;
	return res;

}

// main
void main() {
	// 0 - color of flashlight, 1 - color of lights in rooms
	vec4[2] colors;
	colors[0] = vec4(1, 1, 1, 0);
	colors[1] = vec4(0.6, 0.2, 0.2, 0);
	
	vec4 tex1 = texture(material.diffuseTex, texCoordPrimary);

	// ambient lighting
	vec4 ambient = vec4(brightness);
	vec3 normal = normalize(surfaceNormal);
	
	// compute light
	glFragColor = ambient * tex1;
	if (flashlight.brightness > 0)
		glFragColor += ComputeLight(flashlight, normal, tex1, colors[0], lightArr.length);
	for (int l = 0; l < lightArr.length; l++)
		glFragColor += ComputeLight(lightArr[l], normal, tex1, colors[1], l);
	
}