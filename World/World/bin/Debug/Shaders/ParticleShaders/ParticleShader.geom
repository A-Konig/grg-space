﻿#version 420
layout ( points ) in;
layout ( triangle_strip, max_vertices = 4 ) out;

// matrices
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

// is billboarding on
uniform int billboard;
// life of incoming particle points
in float[] lifeC;

// texcoords of new points
out vec2 texCoordPrimary;

void main(void){
	// size depending on life
	float c = 0.025;
	if (lifeC[0] < 50) {
		c = 0.01;
	}
	if (lifeC[0] > 100) {
		c = 0.03;
	}

	// compute model matrix of one particle
	vec4 pos = gl_in[0].gl_Position;
	mat4 particleMatrix = mat4(vec4(1, 0, 0, 0), 
							 vec4(0, 1, 0, 0), 
							 vec4(0, 0, 1, 0), 
							 vec4(pos.x, pos.y, pos.z, 1));
	mat4 modelMatrixI = modelMatrix * particleMatrix;
	mat4 modelView = viewMatrix * modelMatrixI;

	// compute right and up vectors
	vec4 right = vec4(modelView[0][0], modelView[1][0], modelView[2][0], 0);
	vec4 up = vec4(modelView[0][1], modelView[1][1], modelView[2][1], 0);

	// new vertices positions
	vec4 offset = vec4(-c, c, 0.0, 0.0);
	vec4 vertexPos1 = offset + pos;
	offset = vec4(-c, -c, 0.0, 0.0);
	vec4 vertexPos2 = offset + pos;
	offset = vec4(c, c, 0.0, 0.0);
	vec4 vertexPos3 = offset + pos;
	offset = vec4(c, -c, 0.0, 0.0);
	vec4 vertexPos4 = offset + pos;

	// transform positions so they billboard
	if (billboard == 1) {
		vec4 r1 = (right + up) * c;
		vec4 r2 = (right - up) * c;

		vertexPos1 = (pos - r2);
		vertexPos2 = (pos - r1);
		vertexPos3 = (pos + r1);
		vertexPos4 = (pos + r2);
	}

	// output new vertices 

	gl_Position = projectionMatrix * modelView * vertexPos1;
	texCoordPrimary = vec2(0, 1);
	EmitVertex();

	gl_Position = projectionMatrix * modelView * vertexPos2;
	texCoordPrimary = vec2(0, 0);
	EmitVertex();
	
	gl_Position = projectionMatrix * modelView * vertexPos3;
	texCoordPrimary = vec2(1, 1);
	EmitVertex();
	
	gl_Position = projectionMatrix * modelView * vertexPos4;
	texCoordPrimary = vec2(1, 0);
	EmitVertex();

	EndPrimitive();
}