﻿#version 400
layout(location=0)in vec3 position;
layout(location=1)in vec3 velocity;
layout(location=2)in float life;
layout(location=3)in float id;
layout(location=4)in float lmax;

// new positions
out vec3 positionNew;
// new velocity
out vec3 velocityNew;
// new life value
out float lifeNew;
// id
out float newId;
// max life
out float lmaxNew;
// color - used when debugging
out vec4 color;

// time difference
uniform float dt;
// min and max angle
uniform float minTheta;
uniform float maxTheta;
// min and max speed
uniform float minSpeed;
uniform float maxSpeed;
// size of bounding box
uniform vec3 sizeBox;

// random noise texture and force texture
uniform sampler2D noise;
uniform sampler2D force;

// compute bounding box collision
bool boundingBox(vec3 newPos) {
	if ((newPos.y > sizeBox.y) || (newPos.y < -sizeBox.y) || (newPos.x > sizeBox.x) || (newPos.x < -sizeBox.x) || (newPos.z > sizeBox.z) || (newPos.z < -sizeBox.z))
		return false;

	return true;
}

// main
void main() {
	// positions in texture
	vec2 texPos = vec2((position.x + sizeBox.x)/(2*sizeBox.x), position.y/sizeBox.y);
	vec2 texPos2 = vec2((position.z + sizeBox.z)/(2*sizeBox.z), position.y/sizeBox.y);
	
	// compute force
	vec3 forceCol = 4.0 * (2.0 * texture(force, texPos).rgb - vec3(1.0));
	vec3 forceCol2 = 4.0 * (2.0 * texture(force, texPos2).rgb - vec3(1.0));

	// new particle
	if (life <= 0) {
		lifeNew = lmax;
		positionNew = vec3(0, 0, 0);
        
		// position in noise texture 
        vec2 noise_coord = vec2(int(id) % 512, id / 512);
        vec2 rand = texture(noise, noise_coord).rg;

		// compute direction
        float theta = minTheta + rand.r*(maxTheta - minTheta);
        float x = sin(theta);
		float y = cos(theta);
        float z = sin(theta) + 0.5 * cos(theta / 4);

		// compute velocity
		velocityNew = vec3(x, y, z) * (minSpeed + rand.g * (maxSpeed - minSpeed));
		velocityNew.y = abs(velocityNew.y);
	}

	// update particle
	else {
		velocityNew = velocity;

		// force from environment
		float x = (- forceCol.g + forceCol.r) / 25;
		float y = abs(forceCol.b / 25 + forceCol2.b / 25);
		float z =  (- forceCol2.g + forceCol2.r) / 25;
		vec3 forceV = vec3(x, y, z);

		// new position
		vec3 posT = position + velocityNew * dt ;

		// is there collision
		if (!boundingBox(posT))
			lifeNew = 0;
		else 
			lifeNew = life - 1;

		// update
		positionNew = posT;
		velocityNew = velocity + forceV * dt;
	}

	newId = id;
	lmaxNew = lmax;
	color = vec4(1, 0, 0, 1);

	vec2 coord = vec2(int(id) % 512, id / 512);
	color = vec4(texture(noise, coord).rgb, 1);
	color = vec4(forceCol,1);
	
	gl_Position = vec4(positionNew,1);
	gl_PointSize = 10;
}