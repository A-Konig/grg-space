﻿#version 420
in vec2 texCoordPrimary;

uniform sampler2D texture1;
uniform sampler2D pathTex;

out vec4 glFragColor;

void main() {
	vec4 pCol = vec4(0, 0, 0, 0);
	vec4 pathColor = texture(pathTex, texCoordPrimary);
	
	vec4 color;
	if (pathColor == pCol)
		color = vec4(1, 0, 0, 1);
	else if (pathColor.a == 0)
		color = vec4(0, 1, 0, 1);
	else 
		color = texture(texture1, texCoordPrimary);
	
	glFragColor = color;
}