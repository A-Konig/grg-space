﻿#version 420
in vec3 normalEye;
in vec2 texCoordPrimary;

uniform sampler2D texture1;

out vec4 glFragColor;

void main() {
	// load color from texture
	vec4 color = texture(texture1, texCoordPrimary);
	color = vec4(color.r * 0.5, color.g * 0.5, color.b * 0.5, color.a);
	glFragColor = color;
}