﻿#version 420
in vec3 normalEye;
in vec2 texCoordPrimary;

uniform sampler2D texture1;

out vec4 glFragColor;

void main() {
	vec4 color = texture(texture1, texCoordPrimary);
	glFragColor = color;
}