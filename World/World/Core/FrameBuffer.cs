﻿using OpenTK.Graphics.OpenGL;
using System;
using World.Core.Helpers;

namespace World.Core
{
    /** Framebuffer used for rendering shadows */
    class FrameBuffer
    {
        /** ID */
        public int ID;
        /** Widht and height */
        public int width, height;

        /** Color and depth buffer locations */
        public int colorBuffer, depthBuffer;

        /** Framebiffer status */
        FramebufferStatus framebufferstatus;

        /** Constructor */
        public FrameBuffer(int width, int height)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Creating frame buffer";

            this.width = width;
            this.height = height;

            ID = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, ID);

            // color buffer
            colorBuffer = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, colorBuffer);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMinFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Byte, IntPtr.Zero);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, colorBuffer, 0);

            // bind depth buffer
            depthBuffer = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2DArray, depthBuffer);
            GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
            GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
            GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureMagFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRefToTexture);

            GL.TexImage3D(TextureTarget.Texture2DArray, 0, PixelInternalFormat.DepthComponent32f, width, height, Config.LightCount, 0, PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
            GL.FramebufferTextureLayer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, depthBuffer, 0, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            // write status
            framebufferstatus = GL.CheckNamedFramebufferStatus(ID, FramebufferTarget.Framebuffer);
            Console.WriteLine("Created frambuffer: " + framebufferstatus);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

        }
    }
}
