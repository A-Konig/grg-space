﻿namespace World.Core
{
    /** Class representing a renderpass */
    class RenderPass
    {
        /** camera */
        public Camera cam;
        /** Lights */
        public Light[] lights;
    }
}
