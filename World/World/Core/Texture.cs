﻿using System.Drawing;
using OpenTK.Graphics.OpenGL;
using World.Core.Helpers;

namespace World.Core
{
    /** Class representing a texture */
    class Texture
    {
        /** ID */
        public int Handle;
        /** Path to texture */
        string path;
        /** Is texture from a picture file */
        bool picture;
        
        /** Byte data of texture */
        byte[,] data;
        /** Width and height of texture */
        int width, height;

        /** Constructor */
        public Texture(string path)
        {
            picture = true;
            this.path = path;
            Handle = GL.GenTexture();
        }

        public Texture(byte[,] data, int width, int height)
        {
            this.width = width;
            this.height = height;
            picture = false;
            this.data = data;
            Handle = GL.GenTexture();
        }

        /** Create texture in openGL */
        public void Use(TextureUnit unit)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Using texture";

            GL.Enable(EnableCap.Texture2D);

            GL.ActiveTexture(unit);
            GL.BindTexture(TextureTarget.Texture2D, Handle);

            if (picture)
            {
                Bitmap b = new Bitmap(path);
                System.Drawing.Imaging.BitmapData bpData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, b.Width, b.Height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, bpData.Scan0);
                b.UnlockBits(bpData);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            }
            else
            {
                byte[,] rgb = new byte[height, width * 4];
                for (int i = 0; i < width; i++)
                    for (int j = 0; j < height; j++)
                        if (data[j, i] == 0)
                        {
                            rgb[j, i * 4] = 127;
                            rgb[j, i * 4 + 1] = 127;
                            rgb[j, i * 4 + 2] = 127;
                            rgb[j, i * 4 + 3] = 127;
                        }
                        else if (data[j,i] == 127)
                        {
                            rgb[j, i * 4] = 127;
                            rgb[j, i * 4 + 1] = 0;
                            rgb[j, i * 4 + 2] = 0;
                            rgb[j, i * 4 + 3] = 0;
                        }
                        else
                        {
                            rgb[j, i * 4] = 0;
                            rgb[j, i * 4 + 1] = 0;
                            rgb[j, i * 4 + 2] = 0;
                            rgb[j, i * 4 + 3] = 0;
                        }

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, rgb);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

            }

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.NearestMipmapNearest);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);

            GL.Disable(EnableCap.Texture2D);
        }

     
    }
}
