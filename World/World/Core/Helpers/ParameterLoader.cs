﻿using System;
using System.Globalization;
using System.IO;

namespace World.Core.Helpers
{
    /** Class for configuration of run settings */
    class ParameterLoader
    {
        /** Path to config file */
        static string configPath = "config.txt";

        /** Process cmd arguments */
        public static bool ProcessArgs(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
                if (args[i].Equals("-d"))
                    return true;
            return false;
        }

        /** Read configuration from config file */
        public static void ReadConfig()
        {
            if (!File.Exists(configPath))
                Console.WriteLine("Config file does not exist. Using default values.");
            else
                try
                {
                    string[] lines = File.ReadAllLines(configPath);
                    string[] res = lines[0].Split(';');
                    Config.WindowWidht = int.Parse(res[0].Trim());
                    Config.WindowHeight = int.Parse(res[1].Trim());
                    float val = float.Parse(lines[1].Trim(), new CultureInfo("en-US"));

                    if (val > 1 || val < 0)
                        Console.WriteLine("Incorrect value of ambient, only values between 0 and 1 accepted. Using default values.");
                    else
                        Config.AmbientLight = val;

                } catch (Exception e)
                {
                    Console.WriteLine("Incorrect config file format. Using default values.");
                }
        }


    }
}
