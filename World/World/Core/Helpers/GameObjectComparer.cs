﻿using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using World.Objects;

namespace World.Core
{
    /** Comparer used to order game objects */
    class GameObjectComparer : IComparer<GameObject>
    {
        /** Player */
        Player player;

        /** Constructor */
        public GameObjectComparer(Player player)
        {
            this.player = player;
        }

        /** Compares 2 game objects using their distance to player position as metric */
        public int Compare(GameObject x, GameObject y)
        {
            float dx = Vector3.Distance(player.Position, x.Position);
            float dy = Vector3.Distance(player.Position, y.Position);

            if (dx < dy)
                return 1;
            if (dy < dx)
                return -1;

            return 0;
        }
    }


}
