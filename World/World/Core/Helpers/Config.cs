﻿using OpenTK;

namespace World.Core
{
    /** Class containing the constants and configurations of application */
    class Config
    {
        /** Time since last frame update */
        public static float LastUpdateDelta;
        /** Window width and height */
        public static int WindowWidht = 1000;
        public static int WindowHeight = 800;

        /** Colors of walls */
        public static float[] LightWall = new float[] { 0.33f, 0.42f, 0.54f, 0 }; 
        public static float[] DarkWall = new float[] { 0.03f, 0.07f, 0.13f, 0 }; 
        
        /** Number of lights */
        public static int LightCount = 3;
        /** Ambient light */
        public static float AmbientLight = 0.1f;

        /** Number of real points on one ship wall */
        public static int PointCountOnWall = 12;
        
        /** Length of PositionColor struct */
        public static int StrucLen = 15;
        /** Offsets in struct PositionColor */
        public static int ColorOffset = 3;
        public static int NormalOffset = 7;
        public static int TextureOffset = 10;
        public static int TangentOffset = 12;

        /** Threshold for probability of random object spawn */
        public static double ProbabilityThreshold = 0.999; //9

        /** Room widht and depth */
        public static float RoomWidth = 4.8f;
        public static float RoomDepth = 8f;
        /** Control room width */
        public static float CtrlRoomWidht = 4.8f;
        /** Corridor widht */
        public static float CorridorWidth = 1.6f;

        /** Types of corridor */
        public static int StraightCorridor = 1;
        public static int JoinCorridor = 2;
        public static int CornerCorridor = 3;

        /** World axes */
        public static Vector3 WorldUp = new Vector3(0f, 1.0f, 0.0f);
        public static Vector3 WorldRight = new Vector3(1f, 0f, 0.0f);
        public static Vector3 WorldForward = new Vector3(0f, 0f, 1f);

        /** Padding */
        public static float Padding = CorridorWidth / 8f;

        /** Size of cell in map */
        public static float CellSize = CorridorWidth / 6f;
        /** Tollerance for diversion from path */
        public static int CellTollerance = 6 * 4;
        /** Tollerance for distance from obstacle on path */
        public static int PathTolerance = 6;
        /** Coarse map */
        public static byte[,] CoarseMap = {
                            { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0 },
                            { 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                            { 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 },
                            { 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1 },
                            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                            { 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1 },
                            { 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 },
                            { 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                            { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0 }
                          };

        /** Starting player position and rotation */
        public static Vector3 StartPos = new Vector3(15 * CorridorWidth + CorridorWidth / 2f, 1.5f * CorridorWidth / 4, CorridorWidth * 4 + CorridorWidth / 2f);
        public static Vector3 StartRot = new Vector3(CorridorWidth + CorridorWidth / 2f, 1.5f * CorridorWidth / 4, CorridorWidth * 4 + CorridorWidth / 2f - 10);
        
        /** Size of player's collider */
        public static Vector3 PlayerCollider = new Vector3(Config.CorridorWidth / 6, Config.CorridorWidth / 2, Config.CorridorWidth / 6);

        /** Positions of objectives in grid */
        public static Vector2[] ObjectivePositionsMap = new Vector2[]
{
            //1
            new Vector2(14 * 6, 3 * 6 + 3),
            new Vector2(6 * 6 + 3, 8 * 6),

            //2
            new Vector2(2 * 6 + 5, 3 * 6 + 3),
            new Vector2(6 * 6, 3 * 6 + 3)
        };
        /** Positions of objectives*/
        public static Vector3[] ObjectivePositions = new Vector3[]
        {
            //1
            new Vector3(14 * CorridorWidth + 0.002f, CorridorWidth * 0.4f, 3 * CorridorWidth + CorridorWidth * 0.5f),
            new Vector3(6 * CorridorWidth + CorridorWidth * 0.5f, CorridorWidth * 0.4f, 8 * CorridorWidth + 0.002f),

            //2
            new Vector3(3 * CorridorWidth - 0.002f, CorridorWidth * 0.4f, 3 * CorridorWidth + CorridorWidth / 2f),
            new Vector3(6 * CorridorWidth + 0.002f, CorridorWidth * 0.4f, 3 * CorridorWidth + CorridorWidth / 2f)
            
        };
        /** Positions of buttons on walls */
        public static Vector3[] ButtonPositions = new Vector3[]
{
            //1
            new Vector3(14 * CorridorWidth + 0.001f, CorridorWidth * 0.4f, 3 * CorridorWidth + CorridorWidth * 0.5f),
            new Vector3(6 * CorridorWidth + CorridorWidth * 0.5f, CorridorWidth * 0.4f, 8 * CorridorWidth + 0.001f),

            //2
            new Vector3(3 * CorridorWidth - 0.001f, CorridorWidth * 0.4f, 3 * CorridorWidth + CorridorWidth / 2f),
            new Vector3(6 * CorridorWidth + 0.001f, CorridorWidth * 0.4f, 3 * CorridorWidth + CorridorWidth / 2f)
        
        };
        /** Rotations of objectives */
        public static int[] ObjectiveRotations = new int[]
        {
            //1
            90,
            0,

            //2
            -90,
            90
        };


    }
}
