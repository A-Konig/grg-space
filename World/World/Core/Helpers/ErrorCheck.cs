﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Management;

namespace World.Core.Helpers
{
    /** Class used while checking for errors */
    class ErrorCheck
    {
        /** Minimum required version of openGL */
        public static Version minVersionGL = new Version(3, 3);
        /** Minimum required version of GLSL */
        public static Version minVersionGLSL = new Version(4, 20);

        /** Message describing currently processing block of code. Will be written in console in case of crash. */
        public static string CurrentlyProcessingMSG;

        /** Test graphic card capabilities */
        public static bool TestVersion()
        {
            string glversion = GL.GetString(StringName.Version);
            string glslversion = GL.GetString(StringName.ShadingLanguageVersion);

            Console.WriteLine("Graphics card: ");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_VideoController");
            foreach (ManagementObject mo in searcher.Get())
            {
                PropertyData currentBitsPerPixel = mo.Properties["CurrentBitsPerPixel"];
                PropertyData description = mo.Properties["Description"];
                if (currentBitsPerPixel != null && description != null)
                {
                    if (currentBitsPerPixel.Value != null)
                        Console.WriteLine("\t" + description.Value);
                }
            }

            Console.WriteLine("Using openGL version: " + glversion);
            Console.WriteLine("Using GLSL version: " + glslversion);

            // opengl version
            string[] arr = glversion.Split('.');
            int major = int.Parse(arr[0]);
            int minor = int.Parse(arr[1]);
            Version GLversion = new Version(major, minor);
            
            if (GLversion < minVersionGL)
            {
                return false;
            }

            // glsl version
            arr = glslversion.Split('.');
            major = int.Parse(arr[0].Trim());
            minor = int.Parse(arr[1].Split(' ')[0].Trim());
            Version GLSLversion = new Version(major, minor);
            
            if (GLSLversion < minVersionGLSL)
            {
                return false;
            }

            return true;
        }

    }
}
