﻿using System;
using OpenTK;
using World.Objects;

namespace World.Core
{
    /** Math helper class */
    class MathFunc
    {

        /** Angle between vector specified by two points and X axis */
        public static double AngleBetweenPoints(Vector3 p2, Vector3 p3)
        {
            Vector3 v2 = p3 - p2;
            double angle = Math.Atan2(v2.Z, v2.X) * 180 / Math.PI;
            return angle;
        }

        /** Get position in grid from position in world */
        public static Vector2 GetPositionInMap(Vector3 position)
        {
            float posX = position.X;
            float posZ = position.Z;

            float x = posX / Config.CellSize; 
            float z = posZ / Config.CellSize; 

            int xI = (int)x;
            int yI = (int)z;

            return new Vector2(xI, yI);
        }

        /** Get position in map from position in grid */
        public static Vector3 GetPositionInWorld(Vector2 position)
        {
            float posX = position.X * Config.CellSize + Config.CellSize / 2f;
            float posZ = position.Y * Config.CellSize + Config.CellSize / 2f;

            return new Vector3(posX, 0, posZ);
        }

        /** Is object in map */
        internal static bool IsInMapRange(GameObject o)
        {
            return IsVectorInMapRange(o.Position + o.Offset, o.ColliderBox);
        }

        /** Is player in map */
        internal static bool IsPlayerInMapRange(Player p)
        {
            return IsVectorInMapRange(p.Position, p.ColliderBox);  
        }

        /** Is object with position position and collider box colliderBox in range of map */
        internal static bool IsVectorInMapRange(Vector3 position, Vector3 colliderBox) {
            float oXp = position.X + colliderBox.X / 2f; float oXm = position.X - colliderBox.X / 2f;
            float oZp = position.Z + colliderBox.Z / 2f; float oZm = position.Z - colliderBox.Z / 2f;

            float pXm = 0; float pXp = 17 * Config.CorridorWidth;
            float pZm = 0; float pZp = 9 * Config.CorridorWidth;

            if ((pXm <= oXp && pXp >= oXm) && (pZm <= oZp && pZp >= oZm))
                return true;

            return false;
        }

        /**
        Check if GameObject o can be spawned, aka if it isnt in the same space as player and thus would result in collision on spawn
        */
        internal static bool CheckPosCompatibility(Player player, GameObject o)
        {
            float pXp = player.Position.X + player.ColliderBox.X / 2f; float pXm = player.Position.X - player.ColliderBox.X / 2f;
            float pZp = player.Position.Z + player.ColliderBox.Z / 2f; float pZm = player.Position.Z - player.ColliderBox.Z / 2f;
            float pYp = player.Position.Y + player.ColliderBox.Y / 2f; float pYm = player.Position.Y - player.ColliderBox.Y / 2f;

            float oXp = o.Position.X + o.ColliderBox.X / 2f + o.Offset.X; float oXm = o.Position.X - o.ColliderBox.X / 2f + o.Offset.X;
            float oZp = o.Position.Z + o.ColliderBox.Z / 2f + o.Offset.Z; float oZm = o.Position.Z - o.ColliderBox.Z / 2f + o.Offset.Z;
            float oYp = o.Position.Y + o.ColliderBox.Y / 2f + o.Offset.Y; float oYm = o.Position.Y - o.ColliderBox.Y / 2f + o.Offset.Y;

            // collision with o
            if ((pXm <= oXp && pXp >= oXm) && (pYm <= oYp && pYp >= oYm) && (pZm <= oZp && pZp >= oZm))
                return false;

            return true;
        }
    }
}