﻿using OpenTK;

namespace World
{
    /** Struct with particle data */
    struct ParticleData
    {
        /** Position */
        float x, y, z;
        /** Speed */
        float vx, vy, vz;
        /** life */
        float l, lmax;
        /** id */
        float id;

        /** Constructor */
        public ParticleData(float x, float y, float z, float vx, float vy, float vz, float l, float lmax, float id)
        {
            this.x = x;
            this.y = y;
            this.z = z;

            this.vx = vx;
            this.vy = vy;
            this.vz = vz;

            this.l = l;
            this.id = id;
            this.lmax = lmax;
        }

        /** To string */
        public override string ToString()
        {
            return $"Particle {id}: [{x} {y} {z}] [{vx} {vy} {vz}] [{l} {lmax}]";
        }

        /** Get position */
        public Vector3 GetPosition()
        {
            return new Vector3(x, y, z);
        }
    }


    /** Struct with game object data */
    struct PositionColor
    {
        float x, y, z;
        float r, g, b, a;
        float nx, ny, nz;
        float u, v;
        float tx, ty, tz;

        /** Constructor */
        public PositionColor(float x, float y, float z, float r, float g, float b, float a, float nx, float ny, float nz)
        {
            this.x = x; this.y = y; this.z = z;
            this.r = r; this.g = g; this.b = b;
            this.a = a;
            this.nx = nx; this.ny = ny; this.nz = nz;
            this.u = 0; this.v = 0;
            this.tx = 0; this.ty = 0; this.tz = 0;

        }

        /** Constructor */
        public PositionColor(float x, float y, float z, float r, float g, float b, float a, float nx, float ny, float nz, float u, float v)
        {
            this.x = x; this.y = y; this.z = z;
            this.r = r; this.g = g; this.b = b;
            this.a = a;
            this.nx = nx; this.ny = ny; this.nz = nz;
            this.u = u; this.v = v;
            this.tx = 0; this.ty = 0; this.tz = 0;

        }

        /** Get position */
        public Vector3 GetCoords()
        {
            return new Vector3(x, y, z);
        }

        /** Get UV */
        public Vector2 GetUV()
        {
            return new Vector2(u, v);
        }

        /** Get Normal */
        public Vector3 GetNormal()
        {
            return new Vector3(nx, ny, nz);
        }

        /** Set tangent */
        public void SetTangent(Vector3 t)
        {
            this.tx = t.X;
            this.ty = t.Y;
            this.tz = t.Z;
        }
    }


}
