﻿using OpenTK.Graphics.OpenGL;

namespace World.Core
{
    /** Class representing uniform information */
    class UniformInfo
    {
        /** Name */
        public string Name { get; set; }
        /** Location */
        public int Location { get; set; }
        /** Size */
        public int Size { get; set; }
        /** Type */
        public ActiveUniformType Type { get; set; }
    }
}
