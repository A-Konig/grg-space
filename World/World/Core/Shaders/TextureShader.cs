﻿using OpenTK.Graphics.OpenGL;
using World.Core.Helpers;
using World.Objects;

namespace World.Core
{
    /** Texture shader */
    class TextureShader : Shader
    {
        bool isDynamicTexture;

        /** Constructor */
        public TextureShader(ShaderProgram program, bool isDynamicTexture = false)
        {
            this.isDynamicTexture = isDynamicTexture;
            this.Program = program;
        }

        /** Use shader */
        override public void Use(RenderPass pass, GameObject model)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Trying to use texture shader";

            Program.Use();

            GL.Uniform1(Program.uniforms["texture1"].Location, 0);

            if (isDynamicTexture)
                GL.Uniform1(Program.uniforms["pathTex"].Location, 2);

            SetUniforms(model.Transform, pass);
        }


    }
}
