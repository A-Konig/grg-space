﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core.Helpers;
using World.Objects;

namespace World.Core
{
    /** Class representing a shader */
    abstract class Shader
    {
        /** Shader program */
        ShaderProgram program;
        
        /** Constructor */
        internal ShaderProgram Program { get => program; set => program = value; }

        /** Use shader */
        virtual public void Use(RenderPass pass, GameObject model)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Trying to use shader";
            Program.Use();
            SetUniforms(model.Transform, pass);
        }

        /** Set matrices to uniforms */
        internal void SetUniforms(Matrix4 model, RenderPass renderPass)
        {
            Program.RegisterUniform("matrixProjection");
            Program.RegisterUniform("matrixView");
            Program.RegisterUniform("matrixModel");

            GL.UniformMatrix4(Program.uniforms["matrixProjection"].Location, false, ref renderPass.cam.projection);
            GL.UniformMatrix4(Program.uniforms["matrixView"].Location, false, ref renderPass.cam.view);
            GL.UniformMatrix4(Program.uniforms["matrixModel"].Location, false, ref model);
        }

        /** Stop shader */
        public void Stop()
        {
            Program.Stop();
        }

    }
}
