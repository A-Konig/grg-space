﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core.Helpers;
using World.Objects;

namespace World.Core
{
    /** Light shader */
    class LightShader : Shader
    {
        /** Does shader program use specularmap */
        bool isSpecularMap;
        /** Does shader program use normalmap */
        bool isNormalMap;

        /** Constructor */
        public LightShader(ShaderProgram program, bool isSpecularMap = false, bool isNormalMap = false)
        {
            this.Program = program;
            this.isSpecularMap = isSpecularMap;
            this.isNormalMap = isNormalMap;
        }

        /** Use shader */
        override public void Use(RenderPass pass, GameObject model)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Trying to use light shader";

            Program.Use();

            Program.RegisterUniform("material.intensity");

            // material of object
            GL.Uniform1(Program.uniforms["material.dampening"].Location, model.Dampening);
            GL.Uniform1(Program.uniforms["material.diffuseTex"].Location, 0);
            GL.Uniform1(Program.uniforms["material.intensity"].Location, model.Intensity);
            GL.Uniform3(Program.uniforms["material.specColor"].Location, model.SpecColor);

            // if shader has specular map calculation
            if (isSpecularMap)
                GL.Uniform1(Program.uniforms["material.specularTex"].Location, 3);

            // if shader has normal map calculation
            if (isNormalMap)
                GL.Uniform1(Program.uniforms["material.normalTex"].Location, 2);

            // ambient light
            GL.Uniform1(Program.uniforms["brightness"].Location, Config.AmbientLight);

            // room lights
            for (int l = 0; l < Config.LightCount - 1; l++)
            {
                Matrix4 lightMatrix = pass.lights[l].BuildViewMatrix() * pass.lights[l].BuildProjectionMatrix();

                GL.Uniform4(Program.uniforms[$"light[{l}].position"].Location, pass.lights[l].position);
                GL.Uniform3(Program.uniforms[$"light[{l}].attenuation"].Location, pass.lights[l].attenuation);
                GL.Uniform3(Program.uniforms[$"light[{l}].direction"].Location, pass.lights[l].direction);
                GL.Uniform1(Program.uniforms[$"light[{l}].cutoff"].Location, pass.lights[l].cutoff);
                GL.Uniform1(Program.uniforms[$"light[{l}].brightness"].Location, pass.lights[l].brightness);
                GL.UniformMatrix4(Program.uniforms[$"light[{l}].matrix"].Location, false, ref lightMatrix);
            }

            // flashlight
            int index = Config.LightCount - 1;
            Matrix4 flightMatrix = pass.lights[index].BuildViewMatrix() * pass.lights[index].BuildProjectionMatrix();

            GL.Uniform4(Program.uniforms[$"light0.position"].Location, pass.lights[index].position);
            GL.Uniform3(Program.uniforms[$"light0.attenuation"].Location, pass.lights[index].attenuation);
            GL.Uniform3(Program.uniforms[$"light0.direction"].Location, pass.lights[index].direction);
            GL.Uniform1(Program.uniforms[$"light0.cutoff"].Location, pass.lights[index].cutoff);
            GL.Uniform1(Program.uniforms[$"light0.brightness"].Location, pass.lights[index].brightness);
            GL.UniformMatrix4(Program.uniforms[$"light0.matrix"].Location, false, ref flightMatrix);
            
            // stíny
            GL.Uniform1(Program.uniforms["shadowTex"].Location, 1);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2DArray, pass.lights[0].shadowBuffer.depthBuffer);

            // set projection, view and model matrices
            SetUniforms(model.Transform, pass);
        }


    }
}
