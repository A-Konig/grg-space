﻿using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace World.Core.Shaders
{
    /** Render shader used in particle system */
    class RenderShader
    {
        /** ID */
        int ID;
        /** Uniforms */
        private Dictionary<string, int> uniforms;

        /** Sprite for the particles */
        Texture sprite = new Texture("Textures/particle2.png");

        /** Constructor */
        public RenderShader()
        {
            // vertex shader
            int shaderVertex = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(shaderVertex, System.IO.File.ReadAllText("Shaders/ParticleShaders/ParticleShader.vert"));

            // fragment shader
            int shaderFragment = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(shaderFragment, System.IO.File.ReadAllText("Shaders/ParticleShaders/ParticleShader.frag"));

            // geometry shader
            int shaderGeom = GL.CreateShader(ShaderType.GeometryShader);
            GL.ShaderSource(shaderGeom, System.IO.File.ReadAllText("Shaders/ParticleShaders/ParticleShader.geom"));

            // link program
            ID = GL.CreateProgram();
            GL.AttachShader(ID, shaderVertex);
            GL.AttachShader(ID, shaderFragment);
            GL.AttachShader(ID, shaderGeom);

            GL.LinkProgram(ID);

            GL.GetProgram(ID, GetProgramParameterName.LinkStatus, out var code);
            //Console.WriteLine("Program: " + GL.GetProgramInfoLog(ID));

            GL.DetachShader(ID, shaderVertex);
            GL.DetachShader(ID, shaderFragment);
            GL.DetachShader(ID, shaderGeom);

            GL.DeleteShader(shaderVertex);
            GL.DeleteShader(shaderFragment);
            GL.DeleteShader(shaderGeom);

            // uniforms
            GL.GetProgram(ID, GetProgramParameterName.ActiveUniforms, out var numberOfUniforms);
            uniforms = new Dictionary<string, int>();
            for (var i = 0; i < numberOfUniforms; i++)
            {
                var key = GL.GetActiveUniform(ID, i, out _, out _);
                var location = GL.GetUniformLocation(ID, key);
                uniforms.Add(key, location);
            }

            // setup textures
            sprite.Use(TextureUnit.Texture0);
        }

        /** Stop shader */
        internal void Stop()
        {
            GL.UseProgram(0);
        }

        /** Use shader */
        public void Use(Matrix4 model, Camera camera)
        {
            GL.UseProgram(ID);

            GL.UniformMatrix4(uniforms["projectionMatrix"], false, ref camera.projection);
            GL.UniformMatrix4(uniforms["viewMatrix"], false, ref camera.view);
            GL.UniformMatrix4(uniforms["modelMatrix"], false, ref model);
            GL.Uniform1(uniforms["billboard"], 1);

            // texture
            GL.Uniform1(uniforms["sprite"], 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, sprite.Handle);
        }

    }
}