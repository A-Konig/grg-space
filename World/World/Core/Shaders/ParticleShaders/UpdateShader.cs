﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using World.Core.Helpers;

namespace World.Core.Shaders
{
    /** Update shader used in particle system */
    class UpdateShader : Shader
    {
        /** ID */
        int ID;
        /** Uniforms */
        private Dictionary<string, int> uniforms;
        
        /** Textures used for the forces that are moving the particles */
        Texture noiseTex = new Texture("Textures/image.png");
        Texture forceTex = new Texture("Textures/force.png");

        /** Constructor */
        public UpdateShader()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Creating update shader for particle system";

            int shaderVertex = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(shaderVertex, System.IO.File.ReadAllText("Shaders/ParticleShaders/UpdateShader.vert"));

            int shaderFragment = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(shaderFragment, System.IO.File.ReadAllText("Shaders/ParticleShaders/UpdateShader.frag"));

            // transform feedback variables
            ID = GL.CreateProgram();
            GL.TransformFeedbackVaryings(ID, 5,
                    new string[] { "positionNew", "velocityNew", "lifeNew", "newId", "lmaxNew" },
                    TransformFeedbackMode.InterleavedAttribs);

            // link
            GL.AttachShader(ID, shaderVertex);
            GL.AttachShader(ID, shaderFragment);
            GL.LinkProgram(ID);

            GL.GetProgram(ID, GetProgramParameterName.LinkStatus, out var code);
            //Console.WriteLine("Program: " + GL.GetProgramInfoLog(ID));

            GL.DetachShader(ID, shaderVertex);
            GL.DetachShader(ID, shaderFragment);
            GL.DeleteShader(shaderVertex);
            GL.DeleteShader(shaderFragment);

            // uniforms
            GL.GetProgram(ID, GetProgramParameterName.ActiveUniforms, out var numberOfUniforms);
            uniforms = new Dictionary<string, int>();
            for (var i = 0; i < numberOfUniforms; i++)
            {
                var key = GL.GetActiveUniform(ID, i, out _, out _);
                var location = GL.GetUniformLocation(ID, key);
                uniforms.Add(key, location);
            }

            // setup textures
            noiseTex.Use(TextureUnit.Texture0);
            forceTex.Use(TextureUnit.Texture1);
            
        }

        /** Stop shader */
        internal void Stop()
        {
            GL.UseProgram(0);
        }

        /** Use shader */
        public void Use()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Using update shader for particle system";

            GL.UseProgram(ID);

            GL.Uniform1(uniforms["dt"], Config.LastUpdateDelta / 5f); //0.002f
            GL.Uniform3(uniforms["sizeBox"], new Vector3(Config.CorridorWidth, Config.CorridorWidth, Config.CorridorWidth));
            GL.Uniform1(uniforms["minTheta"], (float)-Math.PI / 6);
            GL.Uniform1(uniforms["maxTheta"], (float)Math.PI / 6);
            GL.Uniform1(uniforms["minSpeed"], -1f);
            GL.Uniform1(uniforms["maxSpeed"], 1f);

            // noise texture
            GL.Uniform1(uniforms["noise"], 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, noiseTex.Handle);

            // vector field
            GL.Uniform1(uniforms["force"], 1);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, forceTex.Handle);
        }

    }
}
