﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK.Graphics.OpenGL;

namespace World.Core
{
    /** Class representing a shader program */
    class ShaderProgram
    {
        /** ID */
        public int Handle;
        /** Vertex and fragment shader */
        int VertexShader, FragmentShader;
        /** Vertex and fragment shader path */
        string vertexFile, fragmentFile;

        /** cleanup shader programs */
        private bool disposedValue = false;

        /** Uniforms */
        public Dictionary<string, UniformInfo> uniforms;

        /**  Constructor */
        public ShaderProgram(string vertexFile, string fragmentFile)
        {
            this.vertexFile = vertexFile;
            this.fragmentFile = fragmentFile;

            Init();
        }

        /** Initilize */
        public void Init()
        {
            // load shaders
            string VertexShaderSource = File.ReadAllText(vertexFile);
            string FragmentShaderSource = File.ReadAllText(fragmentFile);

            // generate shaders
            VertexShader = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(VertexShader, VertexShaderSource);

            FragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(FragmentShader, FragmentShaderSource);

            // compile shaders
            GL.CompileShader(VertexShader);

            // errors
            string infoLogVert = GL.GetShaderInfoLog(VertexShader);
            if (infoLogVert != String.Empty)
                Console.WriteLine(infoLogVert);

            GL.CompileShader(FragmentShader);

            // errors
            string infoLogFrag = GL.GetShaderInfoLog(FragmentShader);
            if (infoLogFrag != String.Empty)
                Console.WriteLine(infoLogFrag);

            // link shaders into program
            Handle = GL.CreateProgram();

            GL.AttachShader(Handle, VertexShader);
            GL.AttachShader(Handle, FragmentShader);

            GL.LinkProgram(Handle);

            // get uniforms
            uniforms = new Dictionary<string, UniformInfo>();
            GL.GetProgram(Handle, GetProgramParameterName.ActiveUniforms, out var count);
            for (int i = 0; i < count; i++)
            {
                string name = GL.GetActiveUniform(Handle, i, out var size, out var type);
                int location = GL.GetUniformLocation(Handle, name);
                uniforms.Add(name, new UniformInfo() { Name = name, Location = location, Size = size, Type = type });
            }

            GL.GetProgramInfoLog(Handle, out string programLog);
            Console.WriteLine("Link log " + vertexFile + " and " + fragmentFile + " : \n" + programLog);

            // cleanup
            GL.DetachShader(Handle, VertexShader);
            GL.DetachShader(Handle, FragmentShader);
            GL.DeleteShader(FragmentShader);
            GL.DeleteShader(VertexShader);
        }

        /** Use program */
        public void Use()
        {
            GL.UseProgram(Handle);
        }

        /** Stop program */
        public void Stop()
        {
            GL.UseProgram(0);
        }

        /** Clean */
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                GL.DeleteProgram(Handle);
                disposedValue = true;
            }
        }

        /** Clean */
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /** Register uniform */
        public void RegisterUniform(string key)
        {
            if (!uniforms.ContainsKey(key))
                uniforms.Add(key, new UniformInfo() { Location = -1 });
        }
    }
}
