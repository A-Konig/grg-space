﻿using OpenTK;
using System;

namespace World.Core
{
    /** Class representing a reflector light */
    class Light
    {
        /** Position of light */
        public Vector4 position = new Vector4(0, 1, 0, 0);
        /** Attenation of light */
        public Vector3 attenuation = new Vector3(1, 0.05f, 0.1f);
        /** Direction of light */
        public Vector3 direction = new Vector3(0, 0, 1);
        /** Cutoff angle */
        public float cutoff = (float)(20 * Math.PI / 180);
        /** Brightness of light */
        public float brightness = 2;

        /** Framebuffer for shadows */
        public FrameBuffer shadowBuffer = new FrameBuffer(1024, 1024);

        /** Build view matrix of light */
        public Matrix4 BuildViewMatrix()
        {
            return Matrix4.LookAt(position.Xyz, position.Xyz + direction, new Vector3(direction.Y, -direction.X, 0));
        }

        /** Build projection matrix of light */
        public Matrix4 BuildProjectionMatrix()
        {
            float noLightIntensity = 0.1f;
            float a = noLightIntensity * attenuation.Z;
            float b = noLightIntensity * attenuation.Y;
            float c = noLightIntensity * attenuation.X - 1;

            float maxDistance = (float)((-b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a));

            return Matrix4.CreatePerspectiveFieldOfView(2 * cutoff, 1, 0.1f, maxDistance);
        }
    }
}
