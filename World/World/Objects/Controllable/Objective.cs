﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World.Objects
{
    /** Class representing game objective - a flat interactable game object */
    class Objective : GameObject
    {
        /** Number of vertices */
        int pointsCount = 4;
        /** Real number of vertices that will be rendered */
        int drawingPointCount = 12;
        /** Width and Height of object */
        public float width, height;
        /** Color of objective */
        float[] color; 

        /** Constructor
            type - 1 if green color, 2 if red color
            size - size of game object */
        public Objective(int type, Vector2 size)
        {
            if (type == 1)
                color = new float[] { 0, 1, 0 };
            else if (type == 2)
                color = new float[] { 1, 0, 0 };

            width = size.X;
            height = size.Y;

            Reach = width;
            Interactalbe = true;
        }

        /** Sets transform matrix of objective according to position in space pos and rotation in space rot */
        public void SetTransform(Vector3 pos, Vector3 rot)
        {
            Position = pos;
            Rotation = rot;

            // rotation around axes xy only
            Transform = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(Rotation.X));
            Transform *= Matrix4.CreateRotationY(MathHelper.DegreesToRadians(Rotation.Y));
            Transform *= Matrix4.CreateTranslation(Position);
        }

        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);

            PositionColor[] data;
            byte[] indices = new byte[1];

            indices = new byte[] { 0, 2, 3, 0, 1, 2,
                                   0, 3, 2, 0, 2, 1
                                 };

            data = new PositionColor[pointsCount];

            data[0] = new PositionColor(-width / 2f, -height/2, 0, color[0], color[1], color[2], 0, 0, 0, 1, 0, 1);
            data[1] = new PositionColor(width / 2f, -height / 2, 0, color[0], color[1], color[2], 0, 0, 0, 1, 1, 1);
            data[2] = new PositionColor(width / 2f, height/2, 0, color[0], color[1], color[2], 0, 0, 0, 1, 1, 0);
            data[3] = new PositionColor(-width / 2f, height/2, 0, color[0], color[1], color[2], 0, 0, 0, 1, 0, 0);

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            // 0 - positions
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            // 2 - normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            // 3 - colors
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));
            // 8 - texture
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            SetupTextures();

            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Objective init " + GL.GetError());
            GL.DeleteBuffer(vb);
        }
        
        /** Render game object */
        public override void Render(RenderPass pass)
        {
            Shader.Use(pass, this);
            ActivateTextures();

            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);
            GL.DrawElements(PrimitiveType.Triangles, drawingPointCount, DrawElementsType.UnsignedByte, 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }

    }
}
