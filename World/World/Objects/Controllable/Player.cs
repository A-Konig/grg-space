﻿using OpenTK;
using World.Core;

namespace World.Objects
{
    /** Class representing the player */
    class Player
    {
        /** Position in 2D grid representing the map */
        public int x, y; // will be used in future pathing

        /** Position of player in 3D  */
        public Vector3 Position { get => position; set => position = value; }
        Vector3 position;
        /** Collider box of player */
        public Vector3 ColliderBox { get => colliderBox; set => colliderBox = value; }
        Vector3 colliderBox;

        /** Reach of player's hands */
        public float Reach { get => reach; }
        float reach = Config.CorridorWidth / 2f;
        
        /** Constructor
            position - Vector3 representing the position of player in 3D
            int[] pos - first two values are the x and y position in 2D grid */
        public Player(Vector3 position, Vector2 pos)
        {
            this.Position = position;
            SetMapPosition((int)pos[0], (int)pos[1]);
            
            colliderBox = Config.PlayerCollider;
        }

        /** Set position in 2D */
        internal void SetMapPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /** Get position in 2D */
        internal Vector2 GetMapPosition()
        {
            return new Vector2(x, y);
        }

    }
}
