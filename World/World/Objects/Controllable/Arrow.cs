﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World.Objects
{
    /** Class representing the navigation arrow
      - to be used in future pathing */
    class Arrow : GameObject
    {
        /** Width of arrow base */
        private float width;
        /** Number of vertices */
        int pointsCount = 3;
        /** Number of vertices that will be drawn */
        int realPointCount = 6;

        /** Constructor */
        public Arrow(float width)
        {
            this.width = width;
        }

        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);

            PositionColor[] data;
            byte[] indices = new byte[] { 1, 0, 2,
                                          0, 1, 2 };

            data = new PositionColor[pointsCount];

            #region MeshData
            data[0] = new PositionColor(-width / 2f, 0, -2 * Config.Padding, 1, 0, 0, 0, 0, -1, 0, 0, 0);
            data[1] = new PositionColor(width / 2f, 0, -2 * Config.Padding, 1, 0, 0, 0, 0, -1, 0, 1, 0);
            data[2] = new PositionColor(0, 0, -2 * Config.Padding - 2* width, 1, 0, 0, 0, 0, -1, 0, 1, 1);
            #endregion

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            // 0 - positions
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            // 2 - normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            // 3 - colors
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));

            // no textures
            //SetupTextures();


            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Box init " + GL.GetError());

            GL.DeleteBuffer(vb);
            GL.DeleteBuffer(ib);

        }

        /** Render game object
            - currently using old pipeline */
        public override void Render(RenderPass pass)
        {
            Shader.Use(pass, this);
            ActivateTextures();

            GL.PushMatrix();

            //GL.Translate(Position);
            //GL.Rotate(-Rotation.Y, 0, 1, 0);

            GL.BindVertexArray(vao);
            GL.DrawElements(PrimitiveType.Triangles, realPointCount, DrawElementsType.UnsignedByte, 0);
            GL.BindVertexArray(0);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            GL.PopMatrix();

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();

        }

        public void UpdateMatrix()
        {
            Transform = Matrix4.CreateRotationY(-MathHelper.DegreesToRadians(Rotation.Y));
            Transform *= Matrix4.CreateTranslation(Position);
        }
    }
}
