﻿using OpenTK;
using World.Controllers;

namespace World.Objects.Controllable
{
    /**
        Class representing a random spawnable object in scene
     */
    class RandomSpawns
    {
        /** Game object */
        public GameObject randomObject;
        /** Life */
        public int life;

        /** Constructor */
        public RandomSpawns(GameObject randomObj, int life)
        {
            this.randomObject = randomObj;
            this.life = life;
        }

        /** Decrease life by one */
        public void DecreaseLife()
        {
            life--;
        }

        /** Remove self from objects in scene */
        public void RemoveSelfFromScene(Scene scene)
        {
            scene.Objects.Remove(randomObject);
        }

    }
}
