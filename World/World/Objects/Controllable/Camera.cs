﻿using OpenTK;
using System;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World
{
    /** 
        Class representing the camera. Camera is able to move in 3D.
     */
    class Camera
    {
        /** Projection matrix */
        public Matrix4 projection;
        /** View matrix */
        public Matrix4 view;

        /** Position of camera in space */
        public Vector3 Position { get => position; set => position = value; }
        Vector3 position;
        /** Camera direction - which way is camera looking */
        public Vector3 CameraDirection { get => cameraDirection; set => cameraDirection= value; }
        Vector3 cameraDirection;
        /** Camera right - vector pointing to the right of  the camera */
        Vector3 cameraRight;
        /** Forward - vector pointing forward from the camera (in plane xz) */
        public Vector3 Forward { get => forward; set => forward = value; }
        Vector3 forward;

        /** Yaw */
        public float yaw = 90;
        /** Pitch */
        public float pitch;

        /** Constructor */
        public Camera()
        {
            view = Matrix4.Identity;
            projection = Matrix4.Identity;
        }
        
        /**
            Constructor
            position - Vector3 determining the position of camera in space
            target - Vector3 determining which way is the camera facing
         */
        public Camera(Vector3 position, Vector3 target)
        {
            Position = position;

            cameraDirection = Vector3.Normalize(target - Position);
            Forward = cameraDirection;
            cameraRight = new Vector3(0, 0, 1);

            view = Matrix4.LookAt(Position, Position + cameraDirection, Config.WorldUp);
        }

        /** Updates axes of camera according to the pitch and yaw */
        internal void UpdateAxes()
        {
            // compute forward
            forward.Z = -(float)Math.Cos(MathHelper.DegreesToRadians(yaw));
            forward.X = (float)Math.Sin(MathHelper.DegreesToRadians(yaw));
            Forward = Vector3.Normalize(forward);

            // compute right
            cameraRight.Z = -(float)Math.Cos(MathHelper.DegreesToRadians(yaw - 90));
            cameraRight.X = (float)Math.Sin(MathHelper.DegreesToRadians(yaw - 90));
            cameraRight = Vector3.Normalize(cameraRight);

            // where is camera looking
            Vector3 forwardNew;
            forwardNew.X = (float)Math.Cos(MathHelper.DegreesToRadians(pitch)) * (float)Math.Cos(MathHelper.DegreesToRadians(yaw-90));
            forwardNew.Y = (float)Math.Sin(MathHelper.DegreesToRadians(pitch));
            forwardNew.Z = (float)Math.Cos(MathHelper.DegreesToRadians(pitch)) * (float)Math.Sin(MathHelper.DegreesToRadians(yaw-90));
            cameraDirection = forwardNew.Normalized();

            // new view matrix
            view = Matrix4.LookAt(Position, Position + cameraDirection, Config.WorldUp);
        }

        /**
            Computes new projection matrix
            width - width of window
            height - height of window
         */
        internal void ComputeProjectionMatrix(int width, int height)
        {
            GL.Viewport(0, 0, width, height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            // view pyramid
            float near = Config.Padding - 0.1f;
            float far = 100;
            float horiz = (float) Math.Tan((90 * Math.PI / 180) / 2);
            float vert = horiz * height / width;

            GL.Frustum(-near * horiz, near * horiz, -near * vert, near * vert, near, far);
            projection = Matrix4.CreatePerspectiveOffCenter(-near * horiz, near * horiz, -near * vert, near * vert, near, 100);
        }

        /** Computes new view matrix */
        public void ComputeViewMatrix()
        {
            view = Matrix4.LookAt(Position, Position + cameraDirection, Config.WorldUp);
        }
    }

}
