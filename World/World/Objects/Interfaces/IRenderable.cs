﻿using System.Collections.Generic;
using World.Objects;

namespace World.Controllers.Interfaces
{
    /** Interface for a scene */
    interface IRenderable
    {
        /** Creates all game objects */
        bool CreateObjects();
        
        /** Draws scene */
        bool DrawScene(bool shadow);

        /** Renders shadowmaps */
        bool DrawLights();

        /** Update scene */
        bool Update(float fps, string msg);

        /** Toggle flashlight */
        void ToggleFlash();

    }
}
