﻿using OpenTK;
using System;
using System.Collections.Generic;
using World.Core;
using World.Core.Helpers;
using World.Objects.GameObjects;

namespace World.Objects
{
    /** Class used for creating game objects in scene */
    class MapSetup
    {
        /** Particle systems */
        internal List<ParticleSystem> Systems { get => systems; set => systems = value; }
        List<ParticleSystem> systems;
        /** Solid game objects */
        internal List<GameObject> Objects { get => objects; set => objects = value; }
        List<GameObject> objects;
        /** Transparent game objects */
        internal List<GameObject> TransparentObjects { get => transparentObjects; set => transparentObjects = value; }
        List<GameObject> transparentObjects;
        /** Gameplay objectives */
        internal List<Objective> Objectives { get => objectives; set => objectives = value; }
        List<Objective> objectives;
        /** Ship walls */
        internal List<GameObject> Walls { get => walls; set => walls = value; }
        List<GameObject> walls;
        /** Spawnable objects */
        internal List<GameObject> RandomObjects { get => randomObjects; set => randomObjects = value; }
        List<GameObject> randomObjects;
        /** Text */
        internal List<Character> Text { get => text; set => text = value; }
        List<Character> text;
        /** List of text characters */
        List<Character> allText;

        /** Navigation arrow - currently off */
        Arrow arr;

        /** Shader used for rendering text and textured objects */
        TextureShader fontShader, texShader, mapShader;
        /** Shader used for rendering lights + shadows, lights + shadows + normalmap, lights + shadows + specularmap */
        LightShader lightShader, normalShader, specularShader;

        /** Box textures */
        Texture boxTexture, boxTextureSpecular, tournicetTexture;
        /** Ship textures */
        Texture corridorTexture, roomTexture, outTexture, doorTexture;
        /** Button textures */
        Texture buttonTexture, buttonOnTexture, buttonOffTexture;
        /** Sky texture */
        Texture skyTexture;
        /** Font textures */
        Texture fontTexture;
        /** Vent textures */
        Texture greyTexture, ventTexture;
        /** Transparent object textures */
        Texture windowTexture, pedestalTexture;
        /** Art textures */
        Texture burstTexture, fireTexture, identityTexture, frameSpecularTexture;
        /** Map texture */
        Texture mapTexture, plainTexture;

        Texture orange = new Texture("Textures/orangle.png");

        /** Spaceship model */
        MeshObject shipModel;
        /** Spaceship movement direction */
        Vector3 dir;
        /** Position FROM from whuch is the ship moving to TO */
        Vector3 from, to;
        /** Is the ship moving from FROM to TO */
        bool plus = true;
        /** Speed of ship */
        float shipspeed = 1.5f;

        /** Ship */
        Ship map;

        /** Create and initialize game objects */
        public void CreateObjects()
        {
            TransparentObjects = new List<GameObject>();
            Objects = new List<GameObject>();
            Objectives = new List<Objective>();
            Text = new List<Character>();
            allText = new List<Character>();
            Systems = new List<ParticleSystem>();
            RandomObjects = new List<GameObject>();

            Shaders();
            Textures();

            ErrorCheck.CurrentlyProcessingMSG = "Creating objects";
            Outside();
            Map();
            Corridors();
            Navigation();

            ObjectivesSet();
            DynamicObjects();

            CreateText();

            ErrorCheck.CurrentlyProcessingMSG = "Initilizing objects";
            foreach (GameObject o in objects)
                o.Init();

            foreach (GameObject o in allText)
                o.Init();

            foreach (GameObject o in transparentObjects)
                o.Init();

            foreach (GameObject o in randomObjects)
                o.Init();

            foreach (ParticleSystem o in systems)
                o.Init();

            Walls = map.Walls;
        }

     
        /** Create textures */
        private void Textures()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Creating textures";

            corridorTexture = new Texture("Textures/corridor3.png");
            roomTexture = new Texture("Textures/room2.png");
            boxTexture = new Texture("Textures/metal_crate.jpg");
            boxTextureSpecular = new Texture("Textures/metal_crate_specular.png");

            greyTexture = new Texture("Textures/grey.png");
            ventTexture = new Texture("Textures/vent-normal.jpg");

            skyTexture = new Texture("Textures/sky.jpg");
            outTexture = new Texture("Textures/light.png");
            buttonTexture = new Texture("Textures/button.png");
            buttonOffTexture = new Texture("Textures/off_button.png");
            buttonOnTexture = new Texture("Textures/on_button.png");

            fontTexture = new Texture("Textures/font3.png");

            windowTexture = new Texture("Textures/window.png");
            pedestalTexture = new Texture("Textures/case.png");

            identityTexture = new Texture("Textures/Identity.jpg");
            burstTexture = new Texture("Textures/burst.jpg");
            fireTexture = new Texture("Textures/fire.jpg");

            frameSpecularTexture = new Texture("Textures/frame.jpg");

            tournicetTexture = new Texture("Textures/tournicet.png");
            doorTexture = new Texture("Textures/doors.jpg");

            mapTexture = new Texture(Config.CoarseMap, Config.CoarseMap.GetLength(1), Config.CoarseMap.GetLength(0));
            plainTexture = new Texture(new byte[6, 6], 6, 6);
        }

        /** Create shaders */
        private void Shaders()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Creating shaders";

            ShaderProgram texProgram = new ShaderProgram("Shaders/BasicShaders/OutsideShader.vert", "Shaders/BasicShaders/OutsideShader.frag");
            texShader = new TextureShader(texProgram);

            ShaderProgram lightProgram = new ShaderProgram("Shaders/LightShaders/LightShader.vert", "Shaders/LightShaders/LightShader.frag");
            lightShader = new LightShader(lightProgram);

            ShaderProgram fontProgram = new ShaderProgram("Shaders/2DShaders/2DShader.vert", "Shaders/2DShaders/2DShader.frag");
            fontShader = new TextureShader(fontProgram);

            ShaderProgram mapProgram = new ShaderProgram("Shaders/2DShaders/2DShader.vert", "Shaders/2DShaders/MapShader.frag");
            mapShader = new TextureShader(mapProgram, true);

            ShaderProgram normalProgram = new ShaderProgram("Shaders/LightShaders/NormalShader.vert", "Shaders/LightShaders/NormalShader.frag");
            normalShader = new LightShader(normalProgram, false, true);

            ShaderProgram specularProgram = new ShaderProgram("Shaders/LightShaders/SpecularShader.vert", "Shaders/LightShaders/SpecularShader.frag");
            specularShader = new LightShader(specularProgram, true);
        }

        /** Create dynamic objects */
        private void DynamicObjects()
        {
            // doors
            Plane doors1 = new Plane(new Vector2(Config.CorridorWidth, Config.CorridorWidth), new Vector3(14 * Config.CorridorWidth, 0.5f * Config.CorridorWidth, 4.5f * Config.CorridorWidth), new Vector3(90, 90, 0));
            doors1.ColliderBox = new Vector3(0.1f, Config.CorridorWidth, Config.CorridorWidth);
            doors1.Shader = lightShader;
            doors1.Texture = doorTexture;
            RandomObjects.Add(doors1);

            doors1 = new Plane(new Vector2(Config.CorridorWidth, Config.CorridorWidth), new Vector3(3 * Config.CorridorWidth, 0.5f * Config.CorridorWidth, 4.5f * Config.CorridorWidth), new Vector3(90, 90, 0));
            doors1.ColliderBox = new Vector3(0.1f, Config.CorridorWidth, Config.CorridorWidth);
            doors1.Shader = lightShader;
            doors1.Texture = doorTexture;
            RandomObjects.Add(doors1);

            doors1 = new Plane(new Vector2(Config.CorridorWidth, Config.CorridorWidth), new Vector3(9.5f * Config.CorridorWidth, 0.5f * Config.CorridorWidth, 2 * Config.CorridorWidth), new Vector3(90, 0, 0));
            doors1.ColliderBox = new Vector3(Config.CorridorWidth, Config.CorridorWidth, 0.1f);
            doors1.Shader = lightShader;
            doors1.Texture = doorTexture;
            RandomObjects.Add(doors1);

            doors1 = new Plane(new Vector2(Config.CorridorWidth, Config.CorridorWidth), new Vector3(9.5f * Config.CorridorWidth, 0.5f * Config.CorridorWidth, 7 * Config.CorridorWidth), new Vector3(90, 0, 0));
            doors1.ColliderBox = new Vector3(Config.CorridorWidth, Config.CorridorWidth, 0.1f);
            doors1.Shader = lightShader;
            doors1.Texture = doorTexture;
            RandomObjects.Add(doors1);

            // tournicets
            Cube cube1 = new Cube(new Vector3(8.25f * Config.CorridorWidth, 0.3f, 3.5f * Config.CorridorWidth), 0.5f, 0.2f, 0.2f);
            cube1.Shader = lightShader;
            cube1.Texture = tournicetTexture;
            RandomObjects.Add(cube1);

            cube1 = new Cube(new Vector3(8.75f * Config.CorridorWidth, 0.3f, 3.5f * Config.CorridorWidth), 0.5f, 0.2f, 0.2f);
            cube1.Shader = lightShader;
            cube1.Texture = tournicetTexture;
            RandomObjects.Add(cube1);

            cube1 = new Cube(new Vector3(9.25f * Config.CorridorWidth, 0.3f, 3.5f * Config.CorridorWidth), 0.5f, 0.2f, 0.2f);
            cube1.Shader = lightShader;
            cube1.Texture = tournicetTexture;
            RandomObjects.Add(cube1);

            cube1 = new Cube(new Vector3(9.75f * Config.CorridorWidth, 0.3f, 3.5f * Config.CorridorWidth), 0.5f, 0.2f, 0.2f);
            cube1.Shader = lightShader;
            cube1.Texture = tournicetTexture;
            RandomObjects.Add(cube1);

            cube1 = new Cube(new Vector3(10.25f * Config.CorridorWidth, 0.3f, 3.5f * Config.CorridorWidth), 0.5f, 0.2f, 0.2f);
            cube1.Shader = lightShader;
            cube1.Texture = tournicetTexture;
            RandomObjects.Add(cube1);

            cube1 = new Cube(new Vector3(10.75f * Config.CorridorWidth, 0.3f, 3.5f * Config.CorridorWidth), 0.5f, 0.2f, 0.2f);
            cube1.Shader = lightShader;
            cube1.Texture = tournicetTexture;
            RandomObjects.Add(cube1);

        }

        /** Create objectives */
        private void ObjectivesSet()
        {
            // buttons
            for (int i = 0; i < Config.ButtonPositions.Length; i++)
            {
                Vector3 pos = Config.ButtonPositions[i];
                Vector3 rot = new Vector3(90, Config.ObjectiveRotations[i], 0);

                Plane buttonPanel = new Plane(new Vector2(Config.CorridorWidth * 0.2f, Config.CorridorWidth * 0.15f), pos, rot);
                buttonPanel.Shader = lightShader;
                buttonPanel.Texture = buttonTexture;
                Objects.Add(buttonPanel);
            }

            // objectives
            Objective objG = new Objective(1, new Vector2(Config.CorridorWidth * 0.2f, Config.CorridorWidth * 0.15f));
            objG.Texture = buttonOnTexture;
            objG.Shader = lightShader;
            objG.SetTransform(Config.ObjectivePositions[0], new Vector3(0, Config.ObjectiveRotations[0], 0));

            Objective objR = new Objective(2, new Vector2(Config.CorridorWidth * 0.2f, Config.CorridorWidth * 0.15f));
            objR.Active = false;
            objR.Texture = buttonOffTexture;
            objR.Shader = lightShader;
            objR.SetTransform(Config.ObjectivePositions[1], new Vector3(0, Config.ObjectiveRotations[1], 0));

            Objects.Add(objG);
            Objects.Add(objR);

            Objectives.Add(objG);
            Objectives.Add(objR);
        }

        /** Create objects outside of the ship */
        private void Outside()
        {
            // thos objects not shaded

            // skybox
            float height = 50;
            Skybox sky = new Skybox(new Vector3(height / 2f - Config.Padding, -height / 2f, 0), height, height, height);
            sky.Shader = texShader;
            sky.Texture = skyTexture;

            // spaceship
            shipModel = new MeshObject(new Vector3(Config.CorridorWidth * 30, Config.CorridorWidth, Config.CorridorWidth * 14),
                                       new Vector3(0, 180, 0),
                                       0.5f,
                                       "Models/spaceship.x", "Textures/spaceship_color.jpg", null);
            shipModel.Shader = texShader;

            from = new Vector3(Config.CorridorWidth * 30, Config.CorridorWidth, Config.CorridorWidth * 14);
            to = new Vector3(Config.CorridorWidth * 30, Config.CorridorWidth, Config.CorridorWidth * -5);
            dir = new Vector3(0, 0, -1);

            // Ship walls
            Plane plane = new Plane(new Vector2(Config.CorridorWidth * 5, Config.CorridorWidth * 2 + 0.2f), new Vector3(Config.CorridorWidth * 12, 0, Config.CorridorWidth * 1.5f), new Vector3(90, -45, 0));
            plane.Shader = texShader;
            plane.Texture = outTexture;
            Objects.Add(plane);

            plane = new Plane(new Vector2(Config.CorridorWidth * 5, Config.CorridorWidth * 2 + 0.2f), new Vector3(Config.CorridorWidth * 12, 0, Config.CorridorWidth * 7.5f), new Vector3(90, 45, 0));
            plane.Shader = texShader;
            plane.Texture = outTexture;
            Objects.Add(plane);

            Objects.Add(shipModel);
            Objects.Add(sky);
        }

        /** Create ship */
        private void Map()
        {
            // so 0, 0, 0 is in the left corner of the ship
            map = new Ship(new Vector3(3 * Config.CorridorWidth + Config.CorridorWidth / 2f, 0f, 4 * Config.CorridorWidth + Config.CorridorWidth / 2f));
            map.Shader = lightShader; //texShader;
            map.CorridorTexture = corridorTexture;
            map.RoomTexture = roomTexture;
            map.Interactalbe = false;
            Objects.Add(map);

            RoomOne();
            RoomTwo();
            RoomThree();
        }

        private void Navigation()
        {
            // navigation arrow
            arr = new Arrow(0.2f);
            arr.Position = new Vector3(Config.StartPos.X, Config.StartPos.Y / 2f, Config.StartPos.Z);
            arr.Rotation = new Vector3(0, 90, 0);
            arr.UpdateMatrix();
            arr.Interactalbe = true;
            arr.Texture = orange;
            arr.Shader = texShader;
            // Objects.Add(arr);

            // minimap
            quad = new ScreenQuad(new Vector2(0.5f, 0.4f));
            quad.NormalMap = plainTexture;
            quad.Texture = mapTexture;
            quad.Shader = mapShader;

            quad.Position = new Vector3(-1 + 0.3f, -1 + 0.25f, 0);
            quad.Transform = Matrix4.CreateTranslation(quad.Position);

            quad.Init();
        }
        public ScreenQuad quad;


        /** Create objects in room one */
        private void RoomOne()
        {

            // boxes

            // accross door
            Cube box1 = new Cube(new Vector3(Config.CorridorWidth / 2f, 0, Config.CorridorWidth * 4), 1f, 0.5f, 0.5f);
            box1.Shader = specularShader;
            box1.Texture = boxTexture;
            box1.SpecularMap = boxTextureSpecular;

            Cube box6 = new Cube(new Vector3(1 * Config.CorridorWidth, 0, Config.CorridorWidth * 4), 0.5f, 0.5f, 1f);
            box6.Shader = specularShader;
            box6.Texture = boxTexture;
            box6.SpecularMap = boxTextureSpecular;

            //right by the door
            Cube box2 = new Cube(new Vector3(Config.CorridorWidth * 2.5f, 0, Config.CorridorWidth * 5), 0.5f, 0.5f, 0.5f);
            box2.Shader = specularShader;
            box2.Texture = boxTexture;
            box2.SpecularMap = boxTextureSpecular;

            Cube box3 = new Cube(new Vector3(2 * Config.CorridorWidth / 2f, 0.5f, Config.CorridorWidth * 4), 0.5f, 0.5f, 0.5f);
            box3.Shader = specularShader;
            box3.Texture = boxTexture;
            box3.SpecularMap = boxTextureSpecular;

            // left back corner
            Cube box7 = new Cube(new Vector3(Config.CorridorWidth - 0.25f, 0, Config.CorridorWidth * 6f), 0.5f, 0.5f, 1f);
            box7.Shader = specularShader;
            box7.Texture = boxTexture;
            box7.SpecularMap = boxTextureSpecular;

            Cube box8 = new Cube(new Vector3(Config.CorridorWidth - 0.25f, 0, Config.CorridorWidth * 6.65f), 0.5f, 0.5f, 1f);
            box8.Shader = specularShader;
            box8.Texture = boxTexture;
            box8.SpecularMap = boxTextureSpecular;

            Cube box9 = new Cube(new Vector3(Config.CorridorWidth - 0.25f, 0.5f, Config.CorridorWidth * 6f + 0.5f), 0.5f, 0.5f, 1f);
            box9.Shader = specularShader;
            box9.Texture = boxTexture;
            box9.SpecularMap = boxTextureSpecular;

            Cube box10 = new Cube(new Vector3(Config.CorridorWidth - 0.25f, 0.5f, Config.CorridorWidth * 6.65f + 0.25f), 0.5f, 0.5f, 0.5f);
            box10.Shader = specularShader;
            box10.Texture = boxTexture;
            box10.SpecularMap = boxTextureSpecular;

            Cube box11 = new Cube(new Vector3(Config.CorridorWidth - 0.25f, 1f, Config.CorridorWidth * 6.65f), 0.5f, 0.5f, 0.5f);
            box11.Shader = specularShader;
            box11.Texture = boxTexture;
            box11.SpecularMap = boxTextureSpecular;

            // left by the door
            Cube box4 = new Cube(new Vector3(0.25f, 0, Config.CorridorWidth * 3), 0.5f, 0.5f, 1f);
            box4.Shader = specularShader;
            box4.Texture = boxTexture;
            box4.SpecularMap = boxTextureSpecular;

            Cube box5 = new Cube(new Vector3(0.25f, 0.5f, Config.CorridorWidth * 3), 0.5f, 0.5f, 0.5f);
            box5.Shader = specularShader;
            box5.Texture = boxTexture;
            box5.SpecularMap = boxTextureSpecular;

            // scp
            MeshObject scp = new MeshObject(new Vector3(Config.CorridorWidth * 2.5f, 0, Config.CorridorWidth * 2.5f),
                                            new Vector3(0, 0, 0),
                                            0.06f,
                                            "Models/173.x", "Textures/173texture.jpg", "Textures/173_Norm.jpg");
            scp.Offset = new Vector3(0, scp.ColliderBox.Y / 2f, 0);
            scp.Intensity = 0.02f;
            scp.Shader = normalShader;

            Objects.Add(scp);
            Objects.Add(box1);
            Objects.Add(box2);
            Objects.Add(box3);
            Objects.Add(box4);
            Objects.Add(box5);
            Objects.Add(box6);
            Objects.Add(box7);
            Objects.Add(box8);
            Objects.Add(box9);
            Objects.Add(box10);
            Objects.Add(box11);
        }

        /** Create game objects in room two */
        private void RoomTwo()
        {
            // boxes

            Cube box1 = new Cube(new Vector3(Config.CorridorWidth * 11, 0, Config.CorridorWidth * 3.5f), 0.3f, 0.5f, 0.8f);
            box1.Shader = specularShader;
            box1.Texture = boxTexture;
            box1.SpecularMap = boxTextureSpecular;

            Cube box2 = new Cube(new Vector3(Config.CorridorWidth * 10, 0, Config.CorridorWidth * 3.5f), 0.3f, 0.5f, 0.8f);
            box2.Shader = specularShader;
            box2.Texture = boxTexture;
            box2.SpecularMap = boxTextureSpecular;

            Cube box3 = new Cube(new Vector3(Config.CorridorWidth * 9, 0, Config.CorridorWidth * 3.5f), 0.3f, 0.5f, 0.8f);
            box3.Shader = specularShader;
            box3.Texture = boxTexture;
            box3.SpecularMap = boxTextureSpecular;

            Cube box4 = new Cube(new Vector3(Config.CorridorWidth * 8, 0, Config.CorridorWidth * 3.5f), 0.3f, 0.5f, 0.8f);
            box4.Shader = specularShader;
            box4.Texture = boxTexture;
            box4.SpecularMap = boxTextureSpecular;

            // in middle
            Cube box5 = new Cube(new Vector3(Config.CorridorWidth * 10 + Config.CorridorWidth / 2f, 0, Config.CorridorWidth * 3.5f), 0.3f, 0.5f, 0.8f);
            box5.Shader = specularShader;
            box5.Texture = boxTexture;
            box5.SpecularMap = boxTextureSpecular;

            Cube box6 = new Cube(new Vector3(Config.CorridorWidth * 9 + Config.CorridorWidth / 2f, 0, Config.CorridorWidth * 3.5f), 0.3f, 0.5f, 0.8f);
            box6.Shader = specularShader;
            box6.Texture = boxTexture;
            box6.SpecularMap = boxTextureSpecular;

            Cube box7 = new Cube(new Vector3(Config.CorridorWidth * 8.5f, 0, Config.CorridorWidth * 3.5f), 0.3f, 0.5f, 0.8f);
            box7.Shader = specularShader;
            box7.Texture = boxTexture;
            box7.SpecularMap = boxTextureSpecular;

            // busts
            MeshObject bust = new MeshObject(new Vector3(Config.CorridorWidth * 10.5f, Config.CorridorWidth * 0.3f, Config.CorridorWidth * 6f),
                                             new Vector3(0, 180, 0),
                                             0.03f,
                                             "Models/heliosbust.x", "Textures/color.png", "Textures/normal.png");
            bust.Offset = new Vector3(0, bust.ColliderBox.Y / 2f, 0);
            bust.Shader = normalShader;
            bust.Dampening = 150;
            bust.Intensity = 5f;

            Objects.Add(bust);

            bust = new MeshObject(new Vector3(Config.CorridorWidth * 8.5f, Config.CorridorWidth * 0.3f, Config.CorridorWidth * 6f),
                                  new Vector3(0, 180, 0),
                                  0.03f,
                                  "Models/heliosbust.x", "Textures/color.png", null);
            bust.Offset = new Vector3(0, bust.ColliderBox.Y / 2f, 0);
            bust.Shader = lightShader;
            bust.Dampening = 150;
            bust.Intensity = 5f;
            Objects.Add(bust);

            // pictures - second room
            Plane picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 10.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 2 + 0.001f), new Vector3(90, 0, 0));
            picture.Shader = specularShader;
            picture.Texture = fireTexture;
            picture.SpecularMap = frameSpecularTexture;
            picture.SpecColor = new Vector3(0.6f, 0.2f, 0.2f);
            Objects.Add(picture);

            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 8.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 2 + 0.001f), new Vector3(90, 0, 0));
            picture.Shader = specularShader;
            picture.Texture = fireTexture;
            picture.SpecularMap = frameSpecularTexture;
            picture.SpecColor = new Vector3(0.6f, 0.2f, 0.2f);
            Objects.Add(picture);

            Objects.Add(box1);
            Objects.Add(box2);
            Objects.Add(box3);
            Objects.Add(box4);
            Objects.Add(box5);
            Objects.Add(box6);
            Objects.Add(box7);
        }

        /** Create game objects in room three */
        private void RoomThree()
        {

            // box
            Cube box1 = new Cube(new Vector3(Config.CorridorWidth * 14.5f, 0, Config.CorridorWidth * 5.5f), 0.5f, 0.5f, 0.5f);
            box1.Shader = specularShader;
            box1.Texture = boxTexture;
            box1.SpecularMap = boxTextureSpecular;
            Objects.Add(box1);

            // transparent
            Cube pedestal = new Cube(new Vector3(Config.CorridorWidth * 16.5f, 0, Config.CorridorWidth * 4.5f), Config.CorridorWidth * 0.5f, Config.CorridorWidth * 0.28f, Config.CorridorWidth * 0.5f);
            pedestal.Shader = texShader;
            pedestal.Texture = pedestalTexture;
            TransparentObjects.Add(pedestal);

            Plane window = new Plane(new Vector2(Config.CorridorWidth * 3, Config.CorridorWidth), new Vector3(Config.CorridorWidth * 17, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 4.5f), new Vector3(90, 90, 0));
            window.Shader = texShader;
            window.Texture = windowTexture;
            TransparentObjects.Add(window);

            Plane windowL = new Plane(new Vector2(Config.CorridorWidth, Config.CorridorWidth), new Vector3(Config.CorridorWidth * 16.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 3), new Vector3(90, 0, 0));
            windowL.Shader = texShader;
            windowL.Texture = windowTexture;
            TransparentObjects.Add(windowL);

            Plane windowR = new Plane(new Vector2(Config.CorridorWidth, Config.CorridorWidth), new Vector3(Config.CorridorWidth * 16.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 6), new Vector3(90, 0, 0));
            windowR.Shader = texShader;
            windowR.Texture = windowTexture;
            TransparentObjects.Add(windowR);

            // bust
            MeshObject bust = new MeshObject(new Vector3(Config.CorridorWidth * 16.5f, Config.CorridorWidth * 0.3f, Config.CorridorWidth * 4.5f),
                                                       new Vector3(0, 90, 0),
                                                       0.03f,
                                                       "Models/heliosbust.x", "Textures/color.png", "Textures/normal.png");
            bust.Offset = new Vector3(0, bust.ColliderBox.Y / 2f, 0);
            bust.Shader = normalShader;
            bust.Dampening = 2;

            Objects.Add(bust);

            // pictures - control room
            Plane picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 15.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 6 - 0.001f), new Vector3(90, 180, 0));
            picture.Shader = specularShader;
            picture.Texture = identityTexture;
            picture.SpecularMap = frameSpecularTexture;
            picture.SpecColor = new Vector3(1, 0, 1);
            Objects.Add(picture);

            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 15.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 3 + 0.001f), new Vector3(90, 0, 0));
            picture.Shader = specularShader;
            picture.Texture = identityTexture;
            picture.SpecColor = new Vector3(1, 0, 1);
            picture.SpecularMap = frameSpecularTexture;
            Objects.Add(picture);

            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 14.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 6 - 0.001f), new Vector3(90, 180, 0));
            picture.Shader = specularShader;
            picture.Texture = identityTexture;
            picture.SpecColor = new Vector3(1, 0, 1);
            picture.SpecularMap = frameSpecularTexture;
            Objects.Add(picture);

            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 14.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 3 + 0.001f), new Vector3(90, 0, 0));
            picture.Shader = specularShader;
            picture.Texture = identityTexture;
            picture.SpecColor = new Vector3(1, 0, 1);
            picture.SpecularMap = frameSpecularTexture;
            Objects.Add(picture);
        }

        /** Create objects in corridors */
        private void Corridors()
        {
            // paricle systems
            ParticleSystem system = new ParticleSystem(500, new Vector3(Config.CorridorWidth * 5, 0, Config.CorridorWidth * 0.5f));
            Systems.Add(system);

            //system = new ParticleSystem(500, new Vector3(Config.CorridorWidth * 1.5f, 0, Config.CorridorWidth * 5.5f));
            //Systems.Add(system);

            system = new ParticleSystem(500, new Vector3(Config.CorridorWidth * 4.5F, 0, Config.CorridorWidth * 6.5f));
            Systems.Add(system);

            // vents
            Plane vent = new Plane(new Vector2(Config.CorridorWidth * 0.3f, Config.CorridorWidth * 0.3f), new Vector3(Config.CorridorWidth * 8, Config.CorridorWidth * 0.3f, Config.CorridorWidth - 0.001f), new Vector3(-90, 0, 0));
            vent.Shader = normalShader;
            vent.Texture = greyTexture;
            vent.NormalMap = ventTexture;
            Objects.Add(vent);

            vent = new Plane(new Vector2(Config.CorridorWidth * 0.3f, Config.CorridorWidth * 0.3f), new Vector3(Config.CorridorWidth * 3, Config.CorridorWidth * 0.3f, Config.CorridorWidth * 8 + 0.001f), new Vector3(90, 0, 0));
            vent.Shader = normalShader;
            vent.Texture = greyTexture;
            vent.NormalMap = ventTexture;
            Objects.Add(vent);

            // pictures top
            Plane picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 3f, Config.CorridorWidth * 0.5f, 0.001f), new Vector3(90, 0, 0));
            picture.Shader = specularShader;
            picture.Texture = burstTexture;
            picture.SpecularMap = frameSpecularTexture;
            picture.SpecColor = new Vector3(0, 0, 1);
            Objects.Add(picture);

            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 8f, Config.CorridorWidth * 0.5f, 0.001f), new Vector3(90, 0, 0));
            picture.Shader = specularShader;
            picture.Texture = burstTexture;
            picture.SpecColor = new Vector3(0, 0, 1);
            picture.SpecularMap = frameSpecularTexture;
            Objects.Add(picture);

            // pictures middle
            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 4.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 4 + 0.001f), new Vector3(90, 0, 0));
            picture.Shader = specularShader;
            picture.Texture = burstTexture;
            picture.SpecColor = new Vector3(0, 0, 1);
            picture.SpecularMap = frameSpecularTexture;
            Objects.Add(picture);

            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 6.5f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 5 - 0.001f), new Vector3(90, 180, 0));
            picture.Shader = specularShader;
            picture.Texture = burstTexture;
            picture.SpecColor = new Vector3(0, 0, 1);
            picture.SpecularMap = frameSpecularTexture;
            Objects.Add(picture);


            // pictures bottom
            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 3f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 9 - 0.001f), new Vector3(90, 180, 0));
            picture.Shader = specularShader;
            picture.Texture = burstTexture;
            picture.SpecColor = new Vector3(0, 0, 1);
            picture.SpecularMap = frameSpecularTexture;
            Objects.Add(picture);

            picture = new Plane(new Vector2(Config.CorridorWidth * 0.4f, Config.CorridorWidth * 0.5f), new Vector3(Config.CorridorWidth * 7f, Config.CorridorWidth * 0.5f, Config.CorridorWidth * 9 - 0.001f), new Vector3(90, 180, 0));
            picture.Shader = specularShader;
            picture.Texture = burstTexture;
            picture.SpecularMap = frameSpecularTexture;
            picture.SpecColor = new Vector3(0, 0, 1);
            Objects.Add(picture);
        }

        /** Create text objects */
        private void CreateText()
        {
            for (int i = 0; i < 'z'; i++)
            {
                Character letter = new Character(i, new Vector2(0.05f, 0.05f), (new Vector3(0, 0, 0))); //start + i*0.1f
                letter.Shader = fontShader;
                letter.Texture = fontTexture;
                allText.Add(letter);
            }
        }

        /** Update text in the center of the screen */
        internal void UpdateCentreText(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                Character c = allText[text[i]];
                c.Position = new Vector3(-text.Length / 2 * 0.1f + i * 0.05f, -0.3f, 0);
                c.Transform = Matrix4.CreateTranslation(c.Position);

                Text.Add(c);
            }
        }

        /** Update text with FPS */
        internal void UpdateFPSText(float fps)
        {
            // destroy temporary characters so they're not left in memory
            for (int i = 0; i < Text.Count; i++)
                if (Text[i].temp == 1)
                    Text[i].Destroy();

            Text = new List<Character>();

            String text = "FPS: " + fps;

            for (int i = 0; i < text.Length; i++)
            {
                // take from dictionary
                if (i < 6)
                {
                    Character c = allText[text[i]];
                    c.Position = new Vector3(-1 + 0.05f + i * 0.05f, 1 - 0.05f, 0);
                    c.Transform = Matrix4.CreateTranslation(c.Position);
                    Text.Add(c);
                }
                // create rest, to avoid the risk of duplicate characters
                else
                {
                    Character letter = new Character(text[i], new Vector2(0.05f, 0.05f), new Vector3(-1 + 0.05f + i * 0.05f, 1 - 0.05f, 0));
                    letter.Shader = fontShader;
                    letter.Texture = fontTexture;
                    letter.Init();
                    Text.Add(letter);
                }
            }

        }

        /** Update position of ship */
        public void UpdateShipPosition()
        {
            if (plus)
            {
                shipModel.Position += Config.LastUpdateDelta * shipspeed * dir;
                shipModel.CreateTransform();

                if (Vector3.Distance(shipModel.Position, to) < 1)
                {
                    shipModel.Rotation = new Vector3(0, 0, 0);
                    shipModel.CreateTransform();
                    plus = false;
                }
            }
            else
            {
                shipModel.Position -= Config.LastUpdateDelta * shipspeed * dir;
                shipModel.CreateTransform();

                if (Vector3.Distance(shipModel.Position, from) < 1)
                {
                    shipModel.Rotation = new Vector3(0, 180, 0);
                    shipModel.CreateTransform();
                    plus = true;
                }
            }

        }

        /** Update navigation
            - TODO for future pathfinding */
        internal void UpdateNavigation(Vector3 playerPos, List<Vector2> currentpath)
        {
            // TODO create array once, just update

            //create a new  texture for quad from currentpath
            int x = Config.CoarseMap.GetLength(1) * 6;
            int y = Config.CoarseMap.GetLength(0) * 6;

            byte[,] data = new byte[y, x];
            for (int i = 0; i < y; i++)
                for (int j = 0; j < x; j++)
                    if (currentpath.Contains(new Vector2(i, j)))
                    {
                        data[i, j] = 1; // path
                    }
                    else
                        data[i, j] = 0; // else

            Vector2 pp = MathFunc.GetPositionInMap(playerPos);
            pp[0] = Math.Min(Math.Max(0, pp[0]), data.GetLength(1) - 1);
            pp[1] = Math.Min(Math.Max(0, pp[1]), data.GetLength(0) - 1);
            data[(int)pp[1], (int)pp[0]] = 127;

            quad.NormalMap = new Texture(data, x, y);
        }
    }
}
