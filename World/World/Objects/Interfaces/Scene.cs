﻿using System.Collections.Generic;
using World.Objects;
using World.Controllers.Interfaces;
using OpenTK;
using World.Core;
using OpenTK.Graphics.OpenGL;
using System;
using World.Objects.GameObjects;
using World.Core.Helpers;

namespace World.Controllers
{
    /** Class representing the currently rendered scene */
    class Scene : IRenderable
    {
        /** Game objects in scene */
        internal List<GameObject> Objects { get => objects; set => objects = value; }
        List<GameObject> objects;
        /** Walls in the scene */
        internal List<GameObject> Walls { get => walls; set => walls = value; }
        List<GameObject> walls;
        /** Transparent objects in the scene */
        internal List<GameObject> TransparentObjects { get => transparentObjects; set => transparentObjects = value; }
        List<GameObject> transparentObjects;
        /** Particle systems in the scene */
        List<ParticleSystem> systems;
        /** List of characters that can be rendered */
        List<Character> text;

        /** Current renderpass */
        RenderPass pass;
        /** Flashlight */
        Light flashlight;
        /** Main camera */
        public Camera mainCam;
        /** Player */
        public Player player;

        /** Map setup */
        public MapSetup mapSetup;
        /** Comparer used for sorting transparent objects */
        GameObjectComparer cmp;

        /** Constructor */
        public Scene(Camera cam, Player player)
        {
            this.mainCam = cam;
            this.player = player;
            pass = new RenderPass();
            pass.cam = cam;
            pass.lights = new Light[Config.LightCount];
            cmp = new GameObjectComparer(player);
        }

        /** Creates all game objects */
        public bool CreateObjects()
        {
            mapSetup = new MapSetup();

            // catching exceptions if texture doesnt exist, shader file doesnt exist or shader program cannot be created
            try
            {
                CreateLights();
                mapSetup.CreateObjects();
            }
            catch (Exception e)
            {
                return false;
            }

            Objects = mapSetup.Objects;
            Walls = mapSetup.Walls;
            text = mapSetup.Text;
            TransparentObjects = mapSetup.TransparentObjects;
            systems = mapSetup.Systems;

            return true;
        }

        /** Create lights */
        private void CreateLights()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Creating lights";

            pass.lights = new Light[Config.LightCount];

            flashlight = new Light();
            flashlight.position = new Vector4(2 * Config.CorridorWidth, Config.CorridorWidth * 0.5f, 6.5f * Config.CorridorWidth, 1);
            flashlight.attenuation = new Vector3(1, 0.01f, 0.5f);
            flashlight.direction = ((flashlight.position.Xyz * new Vector3(0, -0.8f, -1)));
            flashlight.cutoff = (float)(30 * Math.PI / 180);
            flashlight.brightness = 1f;

            Light room1 = new Light();
            room1.attenuation = new Vector3(1, 0.05f, 0.1f);
            room1.position = new Vector4(2.5f * Config.CorridorWidth, Config.CorridorWidth * 0.5f, 7.1f * Config.CorridorWidth, 1);
            room1.direction = (room1.position.Xyz * new Vector3(-0.5f, -0.8f, -1));
            room1.cutoff = (float)(80 * Math.PI / 180);
            room1.brightness = 10;

            Light room2 = new Light();
            room2.position = new Vector4(9.5f * Config.CorridorWidth, Config.CorridorWidth * 0.5f, 2 * Config.CorridorWidth, 1);
            room2.attenuation = new Vector3(1, 0.05f, 0.1f);
            room2.direction = room2.position.Xyz * new Vector3(0, -0.8f, 1);
            room2.cutoff = (float)(80 * Math.PI / 180);
            room2.brightness = 10;

            pass.lights[0] = room1;
            pass.lights[1] = room2;
            pass.lights[2] = flashlight;
        }


        /** Draws scene */
        public bool DrawScene(bool shadow)
        {

            ErrorCheck.CurrentlyProcessingMSG = "Rendering scene";

            try
            {
                RenderSolidObjects();
                RenderTransparentObjects();
                if (!shadow)
                    DrawText();
            }
            // catch if shader cannot be used
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        /** Render solid objects */
        private void RenderSolidObjects()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Rendering solid objects";

            // render solid objects
            foreach (GameObject o in Objects)
            {
                if (o.Active && !o.VirtualObj)
                    o.Render(pass);
            }
        }

        /** Render transparent objects */
        private void RenderTransparentObjects()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Rendering transparent objects";

            // render particle systems
            foreach (ParticleSystem s in systems)
            {
                if (s.alive)
                {
                    s.Update();
                    s.Render(pass);
                    s.NextStep();
                }
                else
                    s.Reset(player);
            }

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            TransparentObjects.Sort(cmp);

            // render transparent objects
            foreach (GameObject o in TransparentObjects)
            {
                if (o.Active && !o.VirtualObj)
                    o.Render(pass);
            }

            GL.Disable(EnableCap.Blend);
        }

        /** Sets and cleans render buffer */
        internal void SetBuffer(int bufferInt)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Setting buffer";

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, bufferInt);
            GL.Viewport(0, 0, Config.WindowWidht, Config.WindowHeight);
            GL.ClearStencil(0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
            GL.DrawBuffer(DrawBufferMode.Back);

            //GL.MatrixMode(MatrixMode.Modelview);
            //GL.LoadIdentity();
        }

        /** Render text on screen */
        public void DrawText()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Rendering text";

            mapSetup.quad.Render(pass);

            // render objects
            foreach (Character o in text)
            {
                if (o.Active && !o.VirtualObj)
                    o.Render(pass);
            }
        }

        /** Renders shadowmaps */
        public bool DrawLights()
        {
            ErrorCheck.CurrentlyProcessingMSG = "Drawing ligths";

            try
            {
                // create shadow map
                for (int l = 0; l < Config.LightCount; l++)
                {
                    // set buffer
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, pass.lights[l].shadowBuffer.ID);
                    GL.FramebufferTextureLayer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, pass.lights[0].shadowBuffer.depthBuffer, 0, l);
                    GL.Viewport(0, 0, pass.lights[0].shadowBuffer.width, pass.lights[0].shadowBuffer.height);
                    GL.ClearStencil(0);
                    GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
                    GL.DrawBuffer(DrawBufferMode.None);

                    // set light as camera
                    Camera c = new Camera();
                    c.view = pass.lights[l].BuildViewMatrix();
                    c.projection = pass.lights[l].BuildProjectionMatrix();
                    pass.cam = c;

                    // render
                    DrawScene(true);

                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                }
            
                pass.cam = mainCam;

            }
            catch (Exception e)
            {
                return false;
            }


            return true;
        }

        /** Update scene */
        public bool Update(float fps, string msg)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Updating scene";

            try
            {
                mapSetup.UpdateShipPosition();
                mapSetup.UpdateFPSText(fps);
                mapSetup.UpdateCentreText(msg);

            } catch
            {
                return false;
            }

            flashlight.position = new Vector4(player.Position, 1);
            flashlight.direction = pass.cam.CameraDirection;

            text = mapSetup.Text;

            return true;
        }

        /** Update navigation display */
        public void UpdateNavigation(List<Vector2> currentpath)
        {
            ErrorCheck.CurrentlyProcessingMSG = "Updating navigation";
            mapSetup.UpdateNavigation(player.Position, currentpath);
        }

        /** Toggle flashlight */
        public void ToggleFlash()
        {
            if (flashlight.brightness > 0)
                flashlight.brightness = 0;
            else
                flashlight.brightness = 1f;
        }

    }
}
