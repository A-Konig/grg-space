﻿using OpenTK.Graphics.OpenGL;
using OpenTK;
using System;
using World.Core;

namespace World.Objects
{
    /** Class representing a quad that will be rendered on screen */
    class ScreenQuad : GameObject
    {
        /** Number of vertices */
        int pointsCout = 4;
        /** Real number of vertices */
        int realPointCount = 12;

        /** X and Y width */
        float widthX, widthY;

        /** Constructor
            size - X and Y size of rendered quad
        */
        public ScreenQuad(Vector2 size)
        {
            widthX = size.X;
            widthY = size.Y;
        }

        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);

            PositionColor[] data;
            byte[] indices = new byte[] { 0, 2, 3, 0, 1, 2,
                                          0, 3, 2, 0, 2, 1 };

            data = new PositionColor[pointsCout];

            #region MeshData
            data[0] = new PositionColor(-widthX / 2f, -widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1);
            data[1] = new PositionColor(widthX / 2f, -widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1);
            data[2] = new PositionColor(widthX / 2f, widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0);
            data[3] = new PositionColor(-widthX / 2f, widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);
            #endregion

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            SetupTextures();

            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Box init " + GL.GetError());

            GL.DeleteBuffer(vb);
            GL.DeleteBuffer(ib);
        }


        /** Render game object */
        public override void Render(RenderPass pass)
        {
            SetupTextures();

            Shader.Use(pass, this);
            ActivateTextures();


            #region DrawMesh
            GL.BindVertexArray(vao);
            GL.DrawElements(PrimitiveType.Triangles, realPointCount, DrawElementsType.UnsignedByte, 0);
            GL.BindVertexArray(0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            #endregion

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }

    }
}
