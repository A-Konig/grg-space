﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World.Objects
{
    class Skybox : GameObject
    {
        /** Number of points per wall */
        int pointsCout = 4;
        /** Number of walls */
        int wallCount = 6;
        /** Real number of vertices */
        int drawingPointCount;

        /** XYZ width of skybox cube */
        float widthX, widthY, widthZ;

        /** Constructor
            position - Vector3 specifying the position of the center in space */
        public Skybox(Vector3 position) : this()
        {
            this.Position = position;
            Transform = Matrix4.CreateTranslation(Position);
        }

        /** Constructor
            position - Vector3 specifying the position of the center in space 
            widhtX, widthY, widthZ - xyz width */
        public Skybox(Vector3 position, float widthX, float widthY, float widthZ) : this(position)
        {
            this.widthX = widthX;
            this.widthY = widthY;
            this.widthZ = widthZ;
        }

        /** Constructor */
        public Skybox()
        {
            widthX = 1;
            widthY = 1;
            widthZ = 1;

            Transform = Matrix4.Identity;
            Interactalbe = false;
        }

        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);

            PositionColor[] data;
            byte[] indices = new byte[] { 0, 3, 2, 0, 2, 1,
                                          4, 6, 7, 4, 5, 6,
                                          8, 11, 10, 8, 10, 9,
                                          12, 13, 14, 13, 15, 14,
                                          16, 17, 19, 18, 16, 19,
                                          20, 23, 21, 20, 22, 23   };

            data = new PositionColor[pointsCout * wallCount];

            #region MeshData
            // floor
            data[0] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 0);
            data[1] = new PositionColor(widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 1, 0);
            data[2] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 1, 1);
            data[3] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 1);

            // ceiling
            data[4] = new PositionColor(-widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 1f, 0f);//
            data[5] = new PositionColor(widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 0f, 0f);//
            data[6] = new PositionColor(widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 0f, 1f);
            data[7] = new PositionColor(-widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 1f, 1f);//

            // front wall
            data[8] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 0, 0); //
            data[9] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 1, 0);//
            data[10] = new PositionColor(widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 1, 1);
            data[11] = new PositionColor(-widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 0, 1);//

            // back wall
            data[12] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 0, 0); //
            data[13] = new PositionColor(widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 1, 0);//
            data[14] = new PositionColor(-widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 0, 1);//
            data[15] = new PositionColor(widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 1, 1);//

            // right wall
            data[16] = new PositionColor(widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, -1, 0, 0, 1, 0);//
            data[17] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, -1, 0, 0, 1, 1);//
            data[18] = new PositionColor(widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, -1, 0, 0, 0, 0);//
            data[19] = new PositionColor(widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, -1, 0, 0, 0, 1);

            // left wall
            data[20] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 1, 0, 0, 1, 0);//
            data[21] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 1, 0, 0, 1, 1); //
            data[22] = new PositionColor(-widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 1, 0, 0, 0, 0, 0);//
            data[23] = new PositionColor(-widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 1, 0, 0, 0, 0, 1);//
            #endregion
            drawingPointCount = Config.PointCountOnWall * wallCount;

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            // 0 - position
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            // 2 - normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            // 3 - colors
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));
            // 8 - texture
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            SetupTextures();

            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Box init " + GL.GetError());

            GL.DeleteBuffer(vb);
        }

        /** Render game object */
        public override void Render(RenderPass pass)
        {
            Shader.Use(pass, this);
            ActivateTextures();

            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);
            GL.DrawElements(PrimitiveType.Triangles, drawingPointCount, DrawElementsType.UnsignedByte, 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }
    }

}
