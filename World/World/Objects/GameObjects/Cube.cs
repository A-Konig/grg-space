﻿using System;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using World.Core;


namespace World.Objects
{
    /** Class representing a cube */
    class Cube : GameObject
    {
        /** Number of vertices per wall */
        int pointsCout = 4;
        /** Number of walls */
        int wallCount = 6;
        /** Real number of vertices */
        int realPointCount;

        /** XYZ width of cube */
        float widthX, widthZ, widthY;

        /** Constructor
            position - Vector3 representing the position of the center of the bottom wall of the cube in space
         */
        public Cube(Vector3 position) : this()
        {
            this.Position = position;

            Transform = Matrix4.CreateTranslation(Position);
        }

        /** Constructor
            position - Vector3 representing the position of the center of the bottom wall of the cube in space
            widthX, widthY, widthZ - width XYZ of cube  
        */
        public Cube(Vector3 position, float widthX, float widthY, float widthZ) : this(position)
        {
            this.widthX = widthX;
            this.widthY = widthY;
            this.widthZ = widthZ;

            ColliderBox = new Vector3(widthX + 0.1f, widthY + 0.1f, widthZ + 0.1f);
            Offset = new Vector3(0, widthY / 2f, 0);

            Reach = Math.Max(Math.Max(widthX, widthY), widthZ);
        }

        /** Constructor */
        public Cube()
        {
            widthX = 1;
            widthY = 1;
            widthZ = 1;

            Reach = 1f;
            ColliderBox = new Vector3(widthX + 0.1f, widthY + 0.1f, widthZ + 0.1f);
            Offset = new Vector3(0, widthY / 2f, 0);

            Transform = Matrix4.Identity;
        }

        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);

            PositionColor[] data;
            byte[] indices = new byte[] { 0, 2, 3, 0, 1, 2,
                                          4, 7, 6, 4, 6, 5,
                                          8, 10, 11, 8, 9, 10,
                                          12, 14, 13, 13, 14, 15,
                                          16, 19, 17, 18, 19, 16,
                                          20, 21, 23, 20, 23, 22    };

            data = new PositionColor[pointsCout * wallCount];

            #region MeshData
            // floor
            data[0] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 0, 0);
            data[1] = new PositionColor(widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 1, 0);
            data[2] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 1, 1);
            data[3] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 0, 1);

            // ceiling
            data[4] = new PositionColor(-widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 1f, 0f);//
            data[5] = new PositionColor(widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 0f, 0f);//
            data[6] = new PositionColor(widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 0f, 1f);
            data[7] = new PositionColor(-widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 1f, 1f);//

            // front wall
            data[8] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 0, 0); //
            data[9] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 1, 0);//
            data[10] = new PositionColor(widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 1, 1);
            data[11] = new PositionColor(-widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 0, 0, 1, 0, 1);//

            // back wall
            data[12] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 0, 0); //
            data[13] = new PositionColor(widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 1, 0);//
            data[14] = new PositionColor(-widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 0, 1);//
            data[15] = new PositionColor(widthX / 2f, widthY, -widthZ / 2f, 0, 0, 0, 0, 0, 0, -1, 1, 1);//

            // right wall
            data[16] = new PositionColor(widthX / 2f, 0, -widthZ / 2f,0, 0, 0, 0, 1, 0, 0, 1, 0);//
            data[17] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 1, 0, 0, 1, 1);//
            data[18] = new PositionColor(widthX / 2f, widthY, -widthZ / 2f,0, 0, 0, 0, 1, 0, 0, 0, 0);//
            data[19] = new PositionColor(widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, 0, 1, 0, 0, 0, 1);

            // left wall
            data[20] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f,0, 0, 0, 0, -1, 0, 0, 1, 0);//
            data[21] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, -1, 0, 0, 1, 1); //
            data[22] = new PositionColor(-widthX / 2f, widthY, -widthZ / 2f,0, 0, 0, -1, 0, 0, 0, 0, 0);//
            data[23] = new PositionColor(-widthX / 2f, widthY, widthZ / 2f, 0, 0, 0, -1, 0, 0, 0, 0, 1);//
            #endregion
            realPointCount = wallCount * 6;

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            // 0 - position
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            // 2 - normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            // 3 - normals
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));
            // 4 - tangents
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TangentOffset * sizeof(float));
            // 8 - texture
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            SetupTextures();

            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Box init " + GL.GetError());

            GL.DeleteBuffer(vb);
            GL.DeleteBuffer(ib);
        }

        /** Render game object */
        public override void Render(RenderPass pass)
        {
            // use shader
            Shader.Use(pass, this);
            ActivateTextures();

            #region DrawMesh
            GL.BindVertexArray(vao);
            GL.DrawElements(PrimitiveType.Triangles, realPointCount, DrawElementsType.UnsignedByte, 0);
            GL.BindVertexArray(0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            #endregion

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }
    }
}
