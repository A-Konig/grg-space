﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;
using World.Core.Shaders;

namespace World.Objects.GameObjects
{
    /** Class representing a particle system emittor */
    class ParticleSystem : GameObject
    {
        /** Vertex buffers, vaos */
        int[] vb;
        int[] vao;
        /** Currently used buffer */
        int buffer = 0;

        /** Is particle system alive */
        public bool alive;
        /** Time to live, time to stay dead */
        int life, death;
        /** Time left to stay dead, time left to live */
        int timeToDeath, timeToLife;

        /** Update shader */
        UpdateShader updateShader;
        /** Render shader */
        RenderShader renderShader;

        /** Number of particles */
        int particleCount;
        /** Random */
        Random r = new Random();

        /** Constructor
            count - number of particles
            pos - position */
        public ParticleSystem(int count, Vector3 pos)
        {
            this.particleCount = count;
            this.Position = pos;

            updateShader = new UpdateShader();
            renderShader = new RenderShader();

            Transform = Matrix4.CreateTranslation(pos);
            ColliderBox = new Vector3(Config.CorridorWidth, Config.CorridorWidth, Config.CorridorWidth);
            Offset = new Vector3(0, Config.CorridorWidth / 2f, 0);

            death = r.Next(500, 1500);
            life = r.Next(2000, 3000);
        }

        /** Initialize game object */
        override public void Init()
        {
            timeToDeath = life;
            alive = true;
            buffer = 0;

            ParticleData[] data = new ParticleData[particleCount];

            for (int i = 0; i < particleCount; i++)
            {
                float l = r.Next(0, 1000);
                float lmax = r.Next(50, 1000);
                float x = 0;
                float y = 0f;
                float z = 0;
                float id = i;

                data[i] = new ParticleData(x, y, z, 0.0f, 0f, 0, l, lmax, id);
            }

            vb = new int[2];
            vb[0] = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb[0]);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * 9 * sizeof(float), data, BufferUsageHint.DynamicDraw);

            vb[1] = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb[1]);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * 9 * sizeof(float), IntPtr.Zero, BufferUsageHint.DynamicDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            vao = new int[4];
            GL.CreateVertexArrays(4, vao);

            for (int i = 0; i < 4; i++)
            {
                GL.BindVertexArray(vao[i]);
                GL.BindBuffer(BufferTarget.ArrayBuffer, vb[i % 2]);
                EnablePointers();
            }

        }

        /** Enable vertex atrrib pointers */
        private void EnablePointers()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 9 * sizeof(float), 0);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 9 * sizeof(float), 3 * sizeof(float));
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 1, VertexAttribPointerType.Float, false, 9 * sizeof(float), 6 * sizeof(float));
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 1, VertexAttribPointerType.Float, false, 9 * sizeof(float), 8 * sizeof(float));
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 1, VertexAttribPointerType.Float, false, 9 * sizeof(float), 7 * sizeof(float));
        }

        /** Update particles */
        internal void Update()
        {
            GL.Enable(EnableCap.RasterizerDiscard);
            updateShader.Use();

            GL.BindVertexArray(vao[buffer]);
            GL.BindBufferBase(BufferRangeTarget.TransformFeedbackBuffer, 0, vb[(buffer + 1) % 2]);
            GL.BeginTransformFeedback(TransformFeedbackPrimitiveType.Points);
            GL.DrawArrays(PrimitiveType.Points, 0, particleCount);
            GL.EndTransformFeedback();

            updateShader.Stop();
            GL.Disable(EnableCap.RasterizerDiscard);
        }

        /** Swap buffers */
        internal void NextStep()
        {
            buffer = (buffer + 1) % 2;
        }

        /** Render particles */
        public override void Render(RenderPass pass)
        {
            timeToDeath--;
            if (timeToDeath == 0)
            {
                alive = false;
                timeToLife = death;
            }

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);

            renderShader.Use(Transform, pass.cam);

            GL.BindVertexArray(vao[buffer + 2]);
            GL.DrawArrays(PrimitiveType.Points, 0, particleCount);

            renderShader.Stop();

            GL.Disable(EnableCap.Blend);
        }

        /** Try to reset particle system */
        internal void Reset(Player player)
        {
            
            timeToLife--;
            if (timeToLife <= 0 && MathFunc.CheckPosCompatibility(player, this))
            {
                Destroy();
                Init();
            }

        }

    }
}
