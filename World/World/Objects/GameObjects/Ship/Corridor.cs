﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World.Objects
{
    /** Class representing one section of the corridors */
    class Corridor : GameObject
    {
        /** Type of corridor */
        int type;
        /** Number of points per wall */
        int pointsCout = 5;
        /** Number of walls */
        int wallCount = 2;
        /** Real number of vertices */
        int drawingPointCount;
        /** Size of corridor (same for x, y and z */
        float width = Config.CorridorWidth;

        /** Constructor
            type - int representint the type of the corridor, values set as values Config.StraightCorridor, Config.JoinCorridor and Config.CornerCorridor */
        public Corridor(int type)
        {
            this.type = type;

            // Straight
            if (type == Config.StraightCorridor)
                wallCount += 2;
            // Join
            else if (type == Config.JoinCorridor)
                wallCount += 1;
            // Corner
            else if (type == Config.CornerCorridor)
                wallCount += 2;
        }

        /** Constructor
            type - int representing the type of the corridor
            position - Vector3 representing the position of the center of the corridor 
            rotation - Vector3 representing the rotation of the corridor (only Y taken into account) */
        public Corridor(int type, Vector3 position, Vector3 rotation)
        {
            Position = position;
            Rotation = rotation;

            this.type = type;
            // Straight
            if (type == Config.StraightCorridor)
                wallCount += 2;
            // Join
            else if (type == Config.JoinCorridor)
                wallCount += 1;
            // Corner
            else if (type == Config.CornerCorridor)
                wallCount += 2;

            Transform = Matrix4.CreateRotationY(MathHelper.DegreesToRadians(rotation.Y));
            Transform *= Matrix4.CreateTranslation(Position);
        }


        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);


            PositionColor[] data;
            byte[] indices = new byte[1];

            data = new PositionColor[pointsCout * wallCount];

            #region MeshData
            // floor
            data[0] = new PositionColor(0, 0, 0, 
                                        Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                        0, 1, 0, 0.25f, 0.75f);
            data[1] = new PositionColor(-width / 2f, 0, -width / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0, 0, 0.5f);
            data[2] = new PositionColor(width / 2f, 0, -width / 2f, 
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0, 0.5f, 0.5f);
            data[3] = new PositionColor(width / 2f, 0, width / 2f, 
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0, 0.5f, 1);
            data[4] = new PositionColor(-width / 2f, 0, width / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0, 0, 1);

            // ceiling
            data[5] = new PositionColor(0, width, 0,
                                        Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                        0, -1, 0, 0.25f, 0.25f);
            data[6] = new PositionColor(-width / 2f, width, -width / 2f, 
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0, 0, 0);
            data[7] = new PositionColor(width / 2f, width, -width / 2f, 
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0, 0.5f, 0);
            data[8] = new PositionColor(width / 2f, width, width / 2f, 
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0, 0.5f, 0.5f);
            data[9] = new PositionColor(-width / 2f, width, width / 2f, 
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0, 0, 0.5f);

            if (type == 0)
            {
                indices = new byte[] {/* floor */ 
                                   0, 3, 2, 0, 2, 1, 4, 3, 0, 4, 0, 1,
                                  /* ceiling */
                                   8, 5, 7, 5, 6, 7, 9, 5, 8, 9, 6, 5
                                 };
            }
            // straight
            else if (type == Config.StraightCorridor)
            {
                indices = new byte[] {/* floor */ 
                                      0, 3, 2, 0, 2, 1, 4, 3, 0, 4, 0, 1,
                                      /* ceiling */
                                      8, 5, 7, 5, 6, 7, 9, 5, 8, 9, 6, 5,
                                      /* bottom */
                                      10, 13, 12, 10, 12, 11, 14, 13, 10, 14, 10, 11, 
                                      /* top */
                                      15, 16, 17, 19, 18, 15, 15, 18, 16, 15, 17, 19 //15, 17, 16, 19, 18, 15, 19, 15, 16
                                     };

                // front wall
                data[10] = new PositionColor(0, width / 2f, width / 2f,
                                            Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                            0, 0, -1, 0.75f, 0.75f);
                data[11] = new PositionColor(-width / 2f, 0, width / 2f, 
                                            Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                            0, 0, -1, 0.5f, 1);
                data[12] = new PositionColor(width / 2f, 0, width / 2f,
                                            Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                            0, 0, -1, 1, 1);
                data[13] = new PositionColor(width / 2f, width, width / 2f,
                                            Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                            0, 0, -1, 1, 0.5f);
                data[14] = new PositionColor(-width / 2f, width, width / 2f,
                                            Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                            0, 0, -1, 0.5f, 0.5f);

                // back wall
                data[15] = new PositionColor(0, width / 2f, -width / 2f, 
                                             Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                             0, 0, 1, 0.75f, 0.75f);
                data[16] = new PositionColor(-width / 2f, 0, -width / 2f, 
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 0.5f, 1);
                data[17] = new PositionColor(width / 2f, 0, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 1, 1);
                data[18] = new PositionColor(-width / 2f, width, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 0.5f, 0.5f);   
                data[19] = new PositionColor(width / 2f, width, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 1, 0.5f);
            }
            else if (type == Config.JoinCorridor)
            {
                indices = new byte[] {/* floor */ 
                                      0, 3, 2, 0, 2, 1, 4, 3, 0, 4, 0, 1,
                                      /* ceiling */
                                      8, 5, 7, 5, 6, 7, 9, 5, 8, 9, 6, 5,
                                      /* top */
                                      10, 11, 12, 14, 13, 10, 10, 13, 11, 10, 12, 14 
                                     };

                // back wall
                data[10] = new PositionColor(0, width / 2f, -width / 2f,
                                             Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                             0, 0, 1, 0.75f, 0.75f);
                data[11] = new PositionColor(-width / 2f, 0, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 0.5f, 1);
                data[12] = new PositionColor(width / 2f, 0, -width / 2f, 
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 1, 1);
                data[13] = new PositionColor(-width / 2f, width, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 0.5f, 0.5f);
                data[14] = new PositionColor(width / 2f, width, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 1, 0.5f);
            }
            else if (type == Config.CornerCorridor)
            {
                indices = new byte[] {/* floor */ 
                                      0, 3, 2, 0, 2, 1, 4, 3, 0, 4, 0, 1,
                                      /* ceiling */
                                      8, 5, 7, 5, 6, 7, 9, 5, 8, 9, 6, 5,
                                      /* top */
                                      10, 11, 12, 14, 13, 10, 10, 13, 11, 10, 12, 14,
                                      /* left */
                                      15, 17, 16,  19,15, 18, 15, 16, 18, 15, 19, 17
                                     };

                // back wall
                data[10] = new PositionColor(0, width / 2f, -width / 2f,
                                             Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                             0, 0, 1, 0.75f, 0.75f);
                data[11] = new PositionColor(-width / 2f, 0, -width / 2f, 
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 0.5f, 1);
                data[12] = new PositionColor(width / 2f, 0, -width / 2f, 
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 1, 1);
                data[13] = new PositionColor(-width / 2f, width, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 0.5f, 0.5f);
                data[14] = new PositionColor(width / 2f, width, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             0, 0, 1, 1, 0.5f);

                // right wall
                data[15] = new PositionColor(width / 2f, width / 2f, 0,
                                             Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                             -1, 0, 0, 0.75f, 0.75f);
                data[16] = new PositionColor(width / 2f, 0, width / 2f, 
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             -1, 0, 0, 1, 1);
                data[17] = new PositionColor(width / 2f, 0, -width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             -1, 0, 0, 0.5f, 1);
                data[18] = new PositionColor(width / 2f, width, width / 2f,
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             -1, 0, 0, 1, 0.5f);
                data[19] = new PositionColor(width / 2f, width, -width / 2f, 
                                             Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                             -1, 0, 0, 0.5f, 0.5f);
            }
            #endregion

            drawingPointCount = Config.PointCountOnWall * wallCount;

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            // 0 - positions
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            // 2 - normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            // 3 - colors
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));
            // 8 - texture
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            SetupTextures();

            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Corridor init " + GL.GetError());

            GL.DeleteBuffer(vb);
        }

        /** Render game object */
        public override void Render(RenderPass pass)
        {
            Shader.Use(pass, this);
            ActivateTextures();

            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);
            GL.DrawElements(PrimitiveType.Triangles, drawingPointCount, DrawElementsType.UnsignedByte, 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }
    }
}
