﻿using OpenTK;
using World.Core;

namespace World.Objects
{
    /** Class representing a wall - a virtual game object */
    class Wall : GameObject
    {
        /** Constructor
            position - Vector3 representing position in space (specifies the center of the wall)
            collider - collider box of the wall */
        public Wall(Vector3 position, Vector3 collider)
        {
            Position = position;
            ColliderBox = collider;

            VirtualObj = true;
        }

        /** Render game object */
        public override void Render(RenderPass pass)
        {
            // no action
        }
    }
}
