﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World.Objects
{
    /** Class representing the control room */
    class ControlRoom : GameObject
    {
        /** Number of vertices */
        int pointsCout = 30;
        /** Number of walls */
        int wallCount = 9;
        /** Real number of vetrices */
        int pointDrawCount;


        /** Width (X), depth (Z) and height (Y) of room */
        public float width = Config.RoomWidth;
        public float depth = Config.RoomWidth;
        public float ceil = Config.CorridorWidth;

        /** Constructor
            position - Vertex3 representing the position of the center of the room 
            rotation - Vertex3 representing the rotation of the room (only Y taken into account) */
        public ControlRoom(Vector3 position, Vector3 rotation)
        {
            Position = position;
            Rotation = rotation;

            Transform = Matrix4.CreateRotationY(MathHelper.DegreesToRadians(rotation.Y));
            Transform *= Matrix4.CreateTranslation(Position);

        }

        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);


            PositionColor[] data = new PositionColor[pointsCout];
            byte[] indices = {/* floor */ 
                                   0, 3, 2, 0, 2, 1, 4, 3, 0, 4, 0, 1,
                                  /* ceiling */
                                   8, 5, 7, 5, 6, 7, 9, 5, 8, 9, 6, 5,
                                   /* left wall */
                                   10, 11, 13, 10, 13, 14, 10, 14, 12, 10, 12, 11,
                                   /* top walls */
                                   15, 16, 18, 15, 18, 19, 15, 19, 17, 15, 17, 16,
                                   20, 23, 21, 20, 24, 23, 20, 22, 24, 20, 21, 22,
                                   /* bottom walls */
                                   25, 28, 26, 25, 29, 28, 25, 27, 29, 25, 26, 27, 
                                 };

            #region MeshData
            // floor
            data[0] = new PositionColor(0, 0, 0,
                                        Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                        0, 1, 0,
                                        0.25f, 0.25f);
            data[1] = new PositionColor(-width / 2f, 0, -depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0,
                                        0.5f, 0f);
            data[2] = new PositionColor(width / 2f, 0, -depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0,
                                        0.5f, 0.5f);
            data[3] = new PositionColor(width / 2f, 0, depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0,
                                        0f, 0.5f);
            data[4] = new PositionColor(-width / 2f, 0, depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 1, 0,
                                        0, 0f);

            // ceiling
            data[5] = new PositionColor(0, ceil, 0, 
                                        Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                        0, -1, 0,
                                        0.75f, 0.25f);
            data[6] = new PositionColor(-width / 2f, ceil, -depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0,
                                        1, 0);
            data[7] = new PositionColor(width / 2f, ceil, -depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0,
                                        1, 0.5f);
            data[8] = new PositionColor(width / 2f, ceil, depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0,
                                        0.5f, 0.5f);
            data[9] = new PositionColor(-width / 2f, ceil, depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, -1, 0,
                                        0.5f, 0);

            // left wall
            data[10] = new PositionColor(-width/2f, ceil / 2f, Config.CorridorWidth,
                                         Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                         1, 0, 0,
                                         0.75f, 0.75f);
            data[11] = new PositionColor(-width / 2f, ceil, Config.CorridorWidth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         1, 0.5f);
            data[12] = new PositionColor(-width / 2f, 0, Config.CorridorWidth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         1, 1);
            data[13] = new PositionColor(-width / 2f, ceil, depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         0.5f, 0.5f);
            data[14] = new PositionColor(-width / 2f, 0, depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         0.5f, 1);

            data[15] = new PositionColor(-width / 2f, ceil / 2f, - Config.CorridorWidth,
                                         Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                         1, 0, 0,
                                         0.75f, 0.75f);
            data[16] = new PositionColor(-width / 2f, ceil, -depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         1, 0.5f);
            data[17] = new PositionColor(-width / 2f, 0, -depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         1, 1);
            data[18] = new PositionColor(-width / 2f, ceil, -Config.CorridorWidth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         0.5f, 0.5f);
            data[19] = new PositionColor(-width / 2f, 0, -Config.CorridorWidth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         1, 0, 0,
                                         0.5f, 1);

            // back walls
            data[20] = new PositionColor(-Config.CorridorWidth/2f, ceil/2f, -depth/2f, 
                                         Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                         0, 0, 1,
                                         0.75f, 0.75f);
            data[21] = new PositionColor(-width / 2f, ceil, -depth / 2f, 
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         0, 0, 1,
                                         0.5f, 0.5f);

            data[22] = new PositionColor(-width / 2f, 0, -depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         0, 0, 1,
                                         0.5f, 1);
            data[23] = new PositionColor(width / 2f - Config.CorridorWidth, ceil, -depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         0, 0, 1,
                                         1, 0.5f);
            data[24] = new PositionColor(width / 2f - Config.CorridorWidth, 0, -depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         0, 0, 1,
                                         1, 1);

            // front walls
            data[25] = new PositionColor(-Config.CorridorWidth / 2f, ceil / 2f, depth / 2f,
                                         Config.DarkWall[0], Config.DarkWall[1], Config.DarkWall[2], Config.DarkWall[3],
                                         0, 0, -1,
                                         0.75f, 0.75f);
            data[26] = new PositionColor(-width / 2f, ceil, depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         0, 0, -1,
                                         0.5f, 0.5f);
            data[27] = new PositionColor(width / 2f - Config.CorridorWidth, ceil, depth / 2f,
                                        Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                        0, 0, -1,
                                        1, 0.5f);
            data[28] = new PositionColor(-width / 2f, 0, depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         0, 0, -1,
                                         0.5f, 1);
           
            data[29] = new PositionColor(width / 2f - Config.CorridorWidth, 0, depth / 2f,
                                         Config.LightWall[0], Config.LightWall[1], Config.LightWall[2], Config.LightWall[3],
                                         0, 0, -1,
                                         1, 1);
            #endregion
            pointDrawCount = wallCount * Config.PointCountOnWall;

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, pointsCout * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            // 0 - positions
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            // 2 - normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), 7 * sizeof(float));
            // 3 - colors
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 3 * sizeof(float));
            // 8 - texture
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            SetupTextures();

            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Room init " + GL.GetError());

            GL.DeleteBuffer(vb);
        }

        /** Render game object */
        public override void Render(RenderPass pass)
        {
            Shader.Use(pass, this);
            ActivateTextures();

            // vykrelsení vao
            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);
            GL.DrawElements(PrimitiveType.Triangles, pointDrawCount, DrawElementsType.UnsignedByte, 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }

    }
}
