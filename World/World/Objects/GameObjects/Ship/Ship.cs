﻿using System.Collections.Generic;
using OpenTK;
using World.Core;

namespace World.Objects
{
    /** Class representing the spaceship */
    class Ship : GameObject
    {
        /** List containing all walls of the ship */
        internal List<GameObject> Walls { get => walls; set => walls = value; }
        List<GameObject> walls;
        /** List containing all corridors of the ship */
        internal List<GameObject> Corridors { get => corridors; set => corridors = value; }
        List<GameObject> corridors;
        /** List containing all the rooms of the ship */
        internal List<GameObject> Rooms { get => rooms; set => rooms = value; }
        List<GameObject> rooms;

        /** Room texture */
        internal Texture RoomTexture { get => roomTexture; set => roomTexture = value; }
        Texture roomTexture;
        /** Corridor texture */
        internal Texture CorridorTexture { get => corridorTexture; set => corridorTexture = value; }
        Texture corridorTexture;

        /** Current position while rendering */
        Vector3 currentPos;
        /** Current rotation while rendering */
        Vector3 currentRot;

        /** Constructor
            position - Vector3 specifying the position of [0,0] of ship on space */
        public Ship(Vector3 position)
        {
            Position = position;
            Corridors = new List<GameObject>();
            Walls = new List<GameObject>();
            Rooms = new List<GameObject>();
        }

        /** Initilizing game object */
        public override void Init()
        {
            InitAllCorridors();
            InitRooms();

            // init created rooms and corridors
            foreach (GameObject o in corridors)
                o.Init();

            foreach (GameObject o in rooms)
                o.Init();
        }

        /** Initilaize rooms */
        private void InitRooms()
        {
            #region Room1
            // create room
            currentPos = new Vector3(Position.X - Config.CorridorWidth/2f - Config.RoomWidth/2f, Position.Y, Position.Z);
            Room room1 = new Room(new Vector3(currentPos.X, currentPos.Y, currentPos.Z), new Vector3(0, 0, 0));
            room1.Shader = Shader;
            room1.Texture = RoomTexture;
            rooms.Add(room1);

            // create walls
            // top walls
            Vector3 wallPos = new Vector3(currentPos.X - Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.RoomDepth / 2f);
            Vector3 wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X + Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.RoomDepth / 2f);
            wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            // bottom walls
            wallPos = new Vector3(currentPos.X - Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.RoomDepth / 2f);
            wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X + Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.RoomDepth / 2f);
            wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            //right wall
            wallPos = new Vector3(currentPos.X - 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.RoomDepth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            // left wall
            wallPos = new Vector3(currentPos.X + 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - 1.5f * Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, 2 * Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X + 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + 1.5f * Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, 2 * Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));
            #endregion

            #region Room2
            // create room
            currentPos = new Vector3(Position.X + Config.CorridorWidth / 2f + Config.CorridorWidth * 4 + Config.RoomWidth / 2f, Position.Y, Position.Z);
            Room room2 = new Room(new Vector3(currentPos.X, currentPos.Y, currentPos.Z), new Vector3(0, 180, 0), true);
            room2.Shader = Shader;
            room2.Texture = RoomTexture;
            rooms.Add(room2);

            // create walls
            // top walls
            wallPos = new Vector3(currentPos.X - Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.RoomDepth / 2f);
            wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X + Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.RoomDepth / 2f);
            wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            // bottom walls
            wallPos = new Vector3(currentPos.X - Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.RoomDepth / 2f);
            wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X + Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.RoomDepth / 2f);
            wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));


            // left wall
            wallPos = new Vector3(currentPos.X + 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - 1.5f * Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, 2 * Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X + 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + 1.5f * Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, 2 * Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            //right wall
            wallPos = new Vector3(currentPos.X - 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - 1.5f * Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, 2 * Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X - 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + 1.5f * Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, 2 * Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            #endregion

            #region Room3
            // create rooms
            currentPos = new Vector3(Position.X + Config.CorridorWidth / 2f + Config.CorridorWidth * 7 + Config.RoomWidth + Config.CtrlRoomWidht / 2f, Position.Y, Position.Z);
            ControlRoom room3 = new ControlRoom(new Vector3(currentPos.X, currentPos.Y, currentPos.Z), new Vector3(0, 0, 0));
            room3.Shader = Shader;
            room3.Texture = RoomTexture;
            rooms.Add(room3);

            // create walls
            // top walls
            wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.RoomWidth / 2f);
            wallColl = new Vector3(Config.RoomWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));


            // bottom walls
            wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.RoomWidth / 2f);
            wallColl = new Vector3(Config.RoomWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
            walls.Add(new Wall(wallPos, wallColl));


            // left wall
            wallPos = new Vector3(currentPos.X - 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            wallPos = new Vector3(currentPos.X - 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.CorridorWidth);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));

            //right wall
            wallPos = new Vector3(currentPos.X + 1.5f * Config.CorridorWidth, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
            wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.RoomWidth + 0.1f);
            walls.Add(new Wall(wallPos, wallColl));
            

            #endregion
        }

        /** Initialize corridors */
        private void InitAllCorridors()
        {
            // set starting position and rotation
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);
            currentRot = new Vector3(Rotation.X, Rotation.Y, Rotation.Z);

            #region CorridorCreation
            // middle horizontal
            InitCorridor(true, 1, Config.StraightCorridor);
            InitCorridor(true, 1, Config.JoinCorridor);
            InitCorridor(true, 1, Config.StraightCorridor);

            Vector3 prevPos = currentPos;
            currentRot.Y += 180;
            InitCorridor(true, 1, Config.JoinCorridor);
            currentRot.Y -= 180;
            currentPos = prevPos;

            currentPos.X += Config.CorridorWidth;
            InitCorridor(true, 1, Config.StraightCorridor);

            currentPos = new Vector3(Position.X, Position.Y, Position.Z);

            // midle down
            currentPos.X += Config.CorridorWidth;
            currentPos.Z += Config.CorridorWidth;
            InitCorridor(false, 3, Config.StraightCorridor);
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);

            // bottom horizontal
            currentPos.X -= 2 * Config.CorridorWidth;
            currentPos.Z += 4 * Config.CorridorWidth;
            prevPos = currentPos;
            currentRot.Y -= 180;
            InitCorridor(true, 1, Config.CornerCorridor);
            currentRot.Y += 180;
            currentPos = prevPos;
            currentPos.X += Config.CorridorWidth;
            InitCorridor(true, 2, Config.StraightCorridor);
            prevPos = currentPos;
            currentRot.Y += 180; 
            InitCorridor(true, 1, Config.JoinCorridor);
            currentRot.Y -= 180;
            currentPos = prevPos;
            currentPos.X += Config.CorridorWidth;
            InitCorridor(true, 4, Config.StraightCorridor);
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);
            
            // bottom single
            currentPos.X -= 2 * Config.CorridorWidth;
            currentPos.Z += 3 * Config.CorridorWidth;
            InitCorridor(false, 1, Config.StraightCorridor);
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);

            // top left single
            currentPos.X -= 2 * Config.CorridorWidth;
            currentPos.Z -= 3 * Config.CorridorWidth;
            InitCorridor(false, 1, Config.StraightCorridor);
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);
            
            // top right single
            currentPos.X += 6 * Config.CorridorWidth;
            currentPos.Z -= 3 * Config.CorridorWidth;
            InitCorridor(false, 1, Config.StraightCorridor);
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);

            // top horizontal
            currentPos.X -= 2 * Config.CorridorWidth;
            currentPos.Z -= 4 * Config.CorridorWidth;
            prevPos = currentPos;
            currentRot.Y += 90;
            InitCorridor(true, 1, Config.CornerCorridor);
            currentRot.Y -= 90;
            currentPos = prevPos;
            currentPos.X += Config.CorridorWidth;
            InitCorridor(true, 4, Config.StraightCorridor);
            InitCorridor(true, 1, Config.JoinCorridor);
            InitCorridor(true, 2, Config.StraightCorridor);
            InitCorridor(true, 1, Config.CornerCorridor);
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);

            // top down
            currentPos.X += 3 * Config.CorridorWidth;
            currentPos.Z -= 3 * Config.CorridorWidth;
            InitCorridor(false, 3, Config.StraightCorridor);
            currentPos = new Vector3(Position.X, Position.Y, Position.Z);

            // right down
            currentPos.X += 6 * Config.CorridorWidth;
            currentPos.Z += 3 * Config.CorridorWidth;
            InitCorridor(false, 1, Config.StraightCorridor);
            prevPos = currentPos;
            currentRot.Y -= 90;
            InitCorridor(true, 1, Config.CornerCorridor);
            currentRot.Y += 90;
            currentPos = prevPos;

            currentPos = new Vector3(Position.X + Config.CorridorWidth * 8, Position.Y, Position.Z);
         
            InitCorridor(true, 1, Config.StraightCorridor);
            InitCorridor(true, 1, Config.StraightCorridor);
            InitCorridor(true, 1, Config.StraightCorridor);
            #endregion

        }

        /** Create corridors, position depends on currentPos and rotation on curentRot. Position will be shifted after creation
            horizontal - is corridor horizontal
            count - how many should be created 
            type - type of corridor */
        private void InitCorridor(bool horizontal, int count, int type)
        {
            if (horizontal)
                for (int i = 0; i < count; i++)
                {
                    Corridor c = new Corridor(type, new Vector3(currentPos.X, currentPos.Y, currentPos.Z), new Vector3(currentRot.X, currentRot.Y, currentRot.Z));
                    c.Shader = Shader;
                    c.Texture = CorridorTexture;
                    Corridors.Add(c);
                    AddWalls(horizontal, type);
                    currentPos.X += Config.CorridorWidth;
                }
            else
                for (int i = 0; i < count; i++)
                {
                    Corridor c = new Corridor(type, new Vector3(currentPos.X, currentPos.Y, currentPos.Z), new Vector3(currentRot.X, currentRot.Y + 90, currentRot.Z));
                    c.Shader = Shader;
                    c.Texture = CorridorTexture;
                    Corridors.Add(c);
                    AddWalls(horizontal, type);
                    currentPos.Z += Config.CorridorWidth;
                }
        }

        /** Add walls of corridor, position depends on currentPos and rotation on curentRot
            horizontal - is corridor horizontal
            type - type of corridor */
        private void AddWalls(bool horizontal, int type)
        {
            if (horizontal)
            {
                if (type == Config.StraightCorridor)
                {
                    walls.Add(new Wall(new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.CorridorWidth / 2f),
                               new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f)));
                    walls.Add(new Wall(new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.CorridorWidth / 2f),
                               new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f)));
                }
                else if (type == Config.JoinCorridor)
                {
                    Vector3 wallPos = new Vector3();
                    Vector3 wallColl = new Vector3();
                    if (currentRot.Y == 0 || currentRot.Y == 360 || currentRot.Y == -360)
                    {
                        wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.CorridorWidth / 2f);
                        wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
                    }
                    if (currentRot.Y == -90 || currentRot.Y == 270)
                    {
                        wallPos = new Vector3(currentPos.X + Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
                        wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
                    }
                    if (currentRot.Y == 180 || currentRot.Y == -180)
                    {
                        wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.CorridorWidth / 2f);
                        wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
                    }
                    if (currentRot.Y == -270 || currentRot.Y == 90)
                    {
                        wallPos = new Vector3(currentPos.X - Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
                        wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
                    }

                    walls.Add(new Wall(wallPos, wallColl));
                }
                else if (type == Config.CornerCorridor)
                {
                    Vector3 wallPos = new Vector3();
                    Vector3 wallColl = new Vector3();
                    if (currentRot.Y == 0 || currentRot.Y == 360 || currentRot.Y == -360)
                    {
                        wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.CorridorWidth / 2f);
                        wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));

                        wallPos = new Vector3(currentPos.X + Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
                        wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));
                    }
                    if (currentRot.Y == -90 || currentRot.Y == 270)
                    {
                        wallPos = new Vector3(currentPos.X + Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
                        wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));

                        wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.CorridorWidth / 2f);
                        wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));
                    }
                    if (currentRot.Y == 180 || currentRot.Y == -180)
                    {
                        wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z + Config.CorridorWidth / 2f);
                        wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));

                        wallPos = new Vector3(currentPos.X - Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
                        wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));
                    }
                    if (currentRot.Y == -270 || currentRot.Y == 90)
                    {
                        wallPos = new Vector3(currentPos.X - Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z);
                        wallColl = new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));

                        wallPos = new Vector3(currentPos.X, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z - Config.CorridorWidth / 2f);
                        wallColl = new Vector3(Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f, 0.1f);
                        walls.Add(new Wall(wallPos, wallColl));
                    }
                }
            }
            else
            {
                if (type == Config.StraightCorridor)
                {
                    walls.Add(new Wall(new Vector3(currentPos.X - Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z),
                               new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f)));
                    walls.Add(new Wall(new Vector3(currentPos.X + Config.CorridorWidth / 2f, currentPos.Y + Config.CorridorWidth / 2f, currentPos.Z),
                               new Vector3(0.1f, Config.CorridorWidth + 0.1f, Config.CorridorWidth + 0.1f)));
                }
            }
        }

        /** Render game object */
        public override void Render(RenderPass pass)
        {
            foreach (GameObject o in Corridors)
                o.Render(pass);

            foreach (GameObject o in Rooms)
                o.Render(pass);
        }

    }
}
