﻿using OpenTK;
using System;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World.Objects
{
    /** Class representing a plane */
    class Plane : GameObject
    {
        /** Number of vertices per wall */
        int pointsCout = 4;
        /** Real number of vertices */
        int realPointCount = 12;

        /** Width X and Z of plane */
        float widthX, widthZ;

        /** Constructor
            size - 2D vector of X and Z width of plane 
            position - Vector3 specifying the position of the center of the plane 
            rotation - Vector3 specifying the rotation of the plane (only X and Y considered) */
        public Plane(Vector2 size, Vector3 position, Vector3 rotation)
        {
            widthX = size.X;
            widthZ = size.Y;

            Position = position;
            Rotation = rotation;

            Transform = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(rotation.X));
            Transform *= Matrix4.CreateRotationY(MathHelper.DegreesToRadians(rotation.Y));
            Transform *= Matrix4.CreateTranslation(Position);
        }

        /** Initilizing game object */
        public override void Init()
        {
            // get buffers
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);

            // setup data
            PositionColor[] data;
            byte[] indices = new byte[] {
                                          0, 3, 2, 0, 2, 1,
                                          7, 4, 6, 6, 4, 5
                                        };

            data = new PositionColor[pointsCout * 2];

            #region MeshData
            // floor
            data[0] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 0);
            data[1] = new PositionColor(widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 1, 0);
            data[2] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 1, 1);
            data[3] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, 1, 0, 0, 1);
            data[4] = new PositionColor(-widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 0, 0);
            data[5] = new PositionColor(widthX / 2f, 0, -widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 1, 0);
            data[6] = new PositionColor(widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 1, 1);
            data[7] = new PositionColor(-widthX / 2f, 0, widthZ / 2f, 0, 0, 0, 0, 0, -1, 0, 0, 1);
            #endregion

            #region TangentCalc
            Vector3[] tmptangents = new Vector3[data.Length];
            for (int i = 0; i < indices.Length; i += 3)
            {
                Vector3 v1 = data[indices[i]].GetCoords();
                Vector3 v2 = data[indices[i + 1]].GetCoords();
                Vector3 v3 = data[indices[i + 2]].GetCoords();

                Vector2 t1 = data[indices[i]].GetUV();
                Vector2 t2 = data[indices[i + 1]].GetUV();
                Vector2 t3 = data[indices[i + 2]].GetUV();

                Vector3 deltaV1 = v2 - v1;
                Vector3 deltaV2 = v3 - v1;

                Vector2 deltaUV1 = t2 - t1;
                Vector2 deltaUV2 = t3 - t1;

                float r = 1 / (deltaUV1.X * deltaUV2.Y - deltaUV1.Y * deltaUV2.X);

                Vector3 tangent1 = new Vector3();
                tangent1.X = r * (deltaUV2.Y * deltaV1.X - deltaUV1.Y * deltaV2.X);
                tangent1.Y = r * (deltaUV2.Y * deltaV1.Y - deltaUV1.Y * deltaV2.Y);
                tangent1.Z = r * (deltaUV2.Y * deltaV1.Z - deltaUV1.Y * deltaV2.Z);

                Vector3 bitangent1 = new Vector3();
                bitangent1.X = r * (-deltaUV2.X * deltaV1.X + deltaUV1.X * deltaV2.X);
                bitangent1.Y = r * (-deltaUV2.X * deltaV1.Y + deltaUV1.X * deltaV2.Y);
                bitangent1.Z = r * (-deltaUV2.X * deltaV1.Z + deltaUV1.X * deltaV2.Z);

                Vector3 tang = (deltaV1 * deltaUV2.Y - deltaV2 * deltaUV1.Y) * r;

                tmptangents[indices[i]] += tang;
                tmptangents[indices[i + 1]] += tang;
                tmptangents[indices[i + 2]] += tang;
            }
            for (int i = 0; i < data.Length; i++)
            {
                /*
                Vector3 normal = data[i].GetNormal();
                Vector3 tan = tmptangents[i].Normalized();
                Vector3 tanFin = (tan - normal * Vector3.Dot(normal, tan)).Normalized();
                */
                Vector3 tanFin = tmptangents[i].Normalized();
                data[i].SetTangent(tanFin);
            }
            #endregion

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            // 0 - positions
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            // 2 - normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            // 3 - colors
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));
            // 4 - tangents
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TangentOffset * sizeof(float));
            // 8 - texture
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            // setup textures
            SetupTextures();

            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Box init " + GL.GetError());

            GL.DeleteBuffer(vb);
        }

        public override void Render(RenderPass pass)
        {
            // use shader
            Shader.Use(pass, this);
            // turn on textures
            ActivateTextures();

            #region DrawMesh
            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);
            GL.DrawElements(PrimitiveType.Triangles, realPointCount, DrawElementsType.UnsignedByte, 0);
            #endregion

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }
    }
}
