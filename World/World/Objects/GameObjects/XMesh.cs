﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;
using static World.Core.XFile;

namespace World.Objects
{
    /** Class representing a loaded 3D mesh
        - prevzata z materialu ke cviceni GRG */
    class Xmesh
    {
        internal XFile XFile { get => xFile; set => xFile = value; }
        XFile xFile;
        public bool reverse;
        bool normals;

        int vao;
        int vb, ib;
        int indexcount;

        Vector3[] tangents;

        public Xmesh(string meshPath, bool normals)
        {
            XFile = new XFile();
            XFile.Load(meshPath);

            this.normals = normals;
        }


        public void Init()
        {
            indexcount = XFile.Index.Length;

            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            // need to reverse .x file
            for (int i = 0; i < XFile.Normal.Length; i++) {
                XFile.Vertex[i].z *= -1;

                XFile.Normal[i].x = -1 * XFile.Normal[i].x;
                XFile.Normal[i].y = -1 * XFile.Normal[i].y;
            }

            for (int i = 0; i < XFile.Index.Length; i++)
            {
                uint temp = XFile.Index[i].v1;
                XFile.Index[i].v1 = XFile.Index[i].v3;
                XFile.Index[i].v3 = temp;
            }

            Optimize(XFile);

            if (normals)
                ComputeTangents(XFile);

            

            // vygenerovani ID pro buffer a pripojeni jako ArrayBuffer
            // nahrani dat
            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            GL.BufferData(BufferTarget.ArrayBuffer, XFile.Vertex.Length * 11 * sizeof(float),
                IntPtr.Zero, BufferUsageHint.StaticDraw);

            // nahrani dat pro pozice
            GL.BufferSubData(BufferTarget.ArrayBuffer,
                (IntPtr)0,
                XFile.Vertex.Length * 3 * sizeof(float),
                XFile.Vertex);

            // nahrani dat pro normaly
            GL.BufferSubData(BufferTarget.ArrayBuffer,
                (IntPtr)(XFile.Vertex.Length * 3 * sizeof(float)),
                XFile.Vertex.Length * 3 * sizeof(float),
                XFile.Normal);

            // nahrani dat pro texcoords
            GL.BufferSubData(BufferTarget.ArrayBuffer,
               (IntPtr)(XFile.Vertex.Length * 6 * sizeof(float)),
               XFile.Vertex.Length * 2 * sizeof(float),
               XFile.TexCoord);

            // tangenty
            GL.BufferSubData(BufferTarget.ArrayBuffer,
                (IntPtr)(xFile.Vertex.Length * 8 * sizeof(float)),
                xFile.Vertex.Length * 3 * sizeof(float),
                tangents);


            // v pripade pouziti VAO se musi propojit jednotliva pole
            // s konkretni pozici atributu vrcholu
            // napojeni dat na konkretni poradi atributu vrcholu
            // pro fixni pipeline jsou pravdepodobne nasledujici; 
            // propojeni zavisi na graficke karte, neni standardizovane, 
            // ale melo by byt
            // 0 - pozice
            // 2 - normaly
            // 3 - barvy
            // 8 - texturovaci souradnice 0

            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false,
                0, IntPtr.Zero);

            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false,
                0, (IntPtr)(XFile.Vertex.Length * 3 * sizeof(float)));

            // sem dávam teď tečnu
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false,
                0, (IntPtr)(xFile.Vertex.Length * 8 * sizeof(float)));

            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false,
                0, (IntPtr)(XFile.Vertex.Length * 6 * sizeof(float)));


            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);
            GL.BufferData(BufferTarget.ElementArrayBuffer, XFile.Index.Length * 3 * sizeof(uint), XFile.Index, BufferUsageHint.StaticDraw);

            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            // v pripade pouziti VAO lze oznacit VB a IB jako nepotrebne
            // v okamziku, kdy nebudou nikde pouzite (VAO) dojde 
            // automaticky k uvolneni pameti
            GL.DeleteBuffer(vb);
            GL.DeleteBuffer(ib);

            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        private void ComputeTangents(XFile xFile)
        {
            Vector3[] temp = new Vector3[xFile.Vertex.Length];

            for (int i = 0; i < xFile.Index.Length; i++)
            {
                uint i1 = xFile.Index[i].v1;
                uint i2 = xFile.Index[i].v2;
                uint i3 = xFile.Index[i].v3;

                Vector3 v1 = new Vector3(xFile.Vertex[i1].x, xFile.Vertex[i1].y, xFile.Vertex[i1].z);
                Vector3 v2 = new Vector3(xFile.Vertex[i2].x, xFile.Vertex[i2].y, xFile.Vertex[i2].z);
                Vector3 v3 = new Vector3(xFile.Vertex[i3].x, xFile.Vertex[i3].y, xFile.Vertex[i3].z);

                Vector2f uv1 = xFile.TexCoord[i1];
                Vector2f uv2 = xFile.TexCoord[i2];
                Vector2f uv3 = xFile.TexCoord[i3];

                Vector3 e1 = v2 - v1;
                Vector3 e2 = v3 - v1;

                Vector2f du = new Vector2f() { x = uv2.x - uv1.x, y = uv3.x - uv1.x };
                Vector2f dv = new Vector2f() { x = uv2.y - uv1.y, y = uv3.y - uv1.y };

                // 1 / det
                float det = 1 / (du.x * dv.y - dv.x * du.y);

                // tangenta
                Vector3 t;
                t.X = (dv.y * e1.X - dv.x * e2.X) * det;
                t.Y = (dv.y * e1.Y - dv.x * e2.Y) * det;
                t.Z = (dv.y * e1.Z - dv.x * e2.Z) * det;

                // add
                temp[i1] += t;
                temp[i2] += t;
                temp[i3] += t;
            }

            // need orthogonal (and normal) base for averaged tangents
            // gram-schmidt, get bitangent from the cross product
            tangents = new Vector3[xFile.Vertex.Length];

            for (int i = 0; i < xFile.Vertex.Length; i++)
            {
                Vector3 normal = new Vector3(xFile.Normal[i].x, xFile.Normal[i].y, xFile.Normal[i].z);
                Vector3 tangent = temp[i].Normalized();

                tangents[i] = (tangent - normal * Vector3.Dot(normal, tangent)).Normalized();
            }
        }

        /// <summary>
        /// Removes duplicates in the source file
        /// </summary>
        /// <param name="xFile">Source file</param>
        private void Optimize(XFile xFile)
        {
            List<Vector3f> vertices = new List<Vector3f>();
            List<Vector3f> normals = new List<Vector3f>();
            List<Vector2f> texCoords = new List<Vector2f>();

            // Vertices will be stored in dictonary based on coord-normal-texcoord key
            // Identical vertices mapped to unique ones
            // Faces reassigned
            Dictionary<string, uint> uniqueVertices = new Dictionary<string, uint>();
            uint[] vertexMapping = new uint[xFile.Vertex.Length];
            uint uniqeVertexCount = 0;

            for (int i = 0; i < xFile.Vertex.Length; i++)
            {
                string key = string.Format("{0} {1} {2} {3} {4} {5} {6} {7}",
                    xFile.Vertex[i].x, xFile.Vertex[i].y, xFile.Vertex[i].z,
                    xFile.Normal[i].x, xFile.Normal[i].y, xFile.Normal[i].z,
                    xFile.TexCoord[i].x, xFile.TexCoord[i].y);

                if (uniqueVertices.ContainsKey(key))
                    // Set mapping to existing index
                    vertexMapping[i] = uniqueVertices[key];
                else
                {
                    // Create index, increase counter, add to new lists
                    uniqueVertices.Add(key, uniqeVertexCount);
                    vertexMapping[i] = uniqeVertexCount++;

                    vertices.Add(xFile.Vertex[i]);
                    normals.Add(xFile.Normal[i]);
                    texCoords.Add(xFile.TexCoord[i]);
                }
            }

            XFile.vertex = vertices.ToArray();
            xFile.normal = normals.ToArray();
            xFile.texCoord = texCoords.ToArray();

            // Remap face indices
            for (int i = 0; i < xFile.Index.Length; i++)
            {
                xFile.Index[i].v1 = vertexMapping[xFile.Index[i].v1];
                xFile.Index[i].v2 = vertexMapping[xFile.Index[i].v2];
                xFile.Index[i].v3 = vertexMapping[xFile.Index[i].v3];
            }
        }

        public void Render()
        {

            GL.Enable(EnableCap.Texture2D);
            GL.BindVertexArray(vao);
            GL.DrawElements(PrimitiveType.Triangles, indexcount * 3, DrawElementsType.UnsignedInt, 0);
            GL.BindVertexArray(0);

            GL.Disable(EnableCap.Texture2D);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }
    }
}
