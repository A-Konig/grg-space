﻿using OpenTK.Graphics.OpenGL;
using OpenTK;
using System;
using World.Core;

namespace World.Objects
{
    /** Class representing one ASCII character. Used for rendering text on screen */
    class Character : GameObject
    {
        /** Number of vertices */
        int pointsCout = 4;
        /** Real number of vertices */
        int realPointCount = 12;

        /** X and Y width */
        float widthX, widthY;

        int charType;
        /** Number of characters in one row in texture */
        int charsPerRow = 16;
        /** Width of one character in texture */
        float oneChar = 1/16.0f;

        /** Row and column of currently created character */
        int row, column;

        /** Set to 1 if character is temporary */
        public int temp;

        /** Constructor
            charType - which character will be rendered (ASCII value)
            size - X and Y size of rendered character
            position - position of the character on screen (values from -1 to 1) */
        public Character(int charType, Vector2 size, Vector3 position)
        {
            Position = position;
            this.charType = charType;

            widthX = size.X;
            widthY = size.Y;

            Transform = Matrix4.CreateTranslation(Position);
            ComputeRowCol();
        }

        /** Compute row and column in texture */
        private void ComputeRowCol()
        {
            column = charType % charsPerRow;
            row = (charType / charsPerRow);
        }

        /** Initilizing game object */
        public override void Init()
        {
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            vb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vb);

            ib = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ib);

            PositionColor[] data;
            byte[] indices = new byte[] { 0, 2, 3, 0, 1, 2,
                                          0, 3, 2, 0, 2, 1 };

            data = new PositionColor[pointsCout];

            #region MeshData

            // u = column
            float stU = oneChar * column;
            float endU = stU + oneChar;

            // v = row
            float stV = oneChar * row;
            float endV = stV + oneChar;
            data[0] = new PositionColor(-widthX / 2f, -widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, stU, endV);
            data[1] = new PositionColor(widthX / 2f, -widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, endU, endV);
            data[2] = new PositionColor(widthX / 2f, widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, endU, stV);
            data[3] = new PositionColor(-widthX / 2f, widthY / 2f, 0, 0, 0, 0, 0, 1, 0, 0, stU, stV);
            #endregion

            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(byte), indices, BufferUsageHint.StaticDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, data.Length * Config.StrucLen * sizeof(float), data, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), 0);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, Config.StrucLen * sizeof(float), Config.NormalOffset * sizeof(float));
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 4, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.ColorOffset * sizeof(float));
            GL.EnableVertexAttribArray(8);
            GL.VertexAttribPointer(8, 2, VertexAttribPointerType.Float, false, Config.StrucLen * sizeof(float), Config.TextureOffset * sizeof(float));

            SetupTextures();

            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //Console.WriteLine("After Box init " + GL.GetError());

            GL.DeleteBuffer(vb);
            GL.DeleteBuffer(ib);
        }


        /** Render game object */
        public override void Render(RenderPass pass)
        {
            Shader.Use(pass, this);
            ActivateTextures();
            
            #region DrawMesh
            GL.BindVertexArray(vao);
            GL.DrawElements(PrimitiveType.Triangles, realPointCount, DrawElementsType.UnsignedByte, 0);
            GL.BindVertexArray(0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            #endregion

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindVertexArray(0);
            Shader.Stop();
        }

    }
}
