﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using World.Core;

namespace World.Objects
{
    /** Class representing a game object */
    abstract class GameObject
    {
        /** Indices of vertex buffer, vertex array buffer and index buffer */
        internal int vb, vao, ib;

        /** Dampening of specular light on object */
        public int Dampening { get => dampening; set => dampening = value; }
        int dampening = 32;

        /** Color of specular light reflecting off of object */
        public Vector3 SpecColor { get => specColor; set => specColor = value; }
        Vector3 specColor = new Vector3(1, 1, 1);
        
        /** Intensity of specular light */
        public float Intensity { get => intensity; set => intensity = value; }
        private float intensity = 1;

        /** Transform matrix */
        public Matrix4 Transform { get => transform; set => transform = value; }
        Matrix4 transform;

        /** Position of object in map */
        public Vector2 MapPos { get => mapPos; set => mapPos = value; }
        Vector2 mapPos;

        /** Position of object in space */
        public Vector3 Position { get => position; set => position = value; }
        Vector3 position;
        /** Rotation of object in space */
        public Vector3 Rotation { get => rotation; set => rotation = value; }
        Vector3 rotation;

        /** Collider box of given object */
        public Vector3 ColliderBox { get => colliderBox; set => colliderBox = value; }
        Vector3 colliderBox;
        /** Optional offset of object's collider box from local [0,0,0] */
        public Vector3 Offset { get => offset; set => offset = value; }
        Vector3 offset;

        /** Shader that will be used when rendering */
        public Shader Shader { get => shader; set => shader = value; }
        Shader shader;
        /** Diffuse texture that will be used when rendering */
        public Texture Texture { get => texture; set => texture = value; }
        Texture texture;
        /** Normal map texture that will be used when rendering */
        public Texture NormalMap { get => normalMap; set => normalMap = value; }
        Texture normalMap;
        /** Specular map texture that will be used when rendering */
        public Texture SpecularMap { get => specularMap; set => specularMap = value; }
        Texture specularMap;

        /** Reach of object in space */
        public float Reach { get => reach; set => reach = value; }
        float reach;
        /** Is object active - inactive objects not rendered or subjected to collision detection */
        public bool Active { get => active; set => active = value; }
        bool active = true;
        
        /** Is object interactable - interactable objects are rendered, but not subjected to collision detection */
        public bool Interactalbe { get => interactalbe; set => interactalbe = value; }
        bool interactalbe = false;
        /** Is object virtual - virtual objects are not rendered, but are subjected to collision detection */
        public bool VirtualObj { get => virtualObj; set => virtualObj = value; }
        bool virtualObj = false;

      
        /** Initilizing game object */
        public virtual void Init() { }

        /** Render game object */
        public abstract void Render(RenderPass pass);

        /** Set up all of the game object's textures before using */
        public void SetupTextures()
        {
            if (Texture != null)
                Texture.Use(TextureUnit.Texture0);

            if (NormalMap != null)
                NormalMap.Use(TextureUnit.Texture2);

            if (SpecularMap != null)
                SpecularMap.Use(TextureUnit.Texture3);
        }

        /** Activate all of the game object's textures before using
            diffuse mapped to 0, normal map mapped to 2, specular map mapped to 3 */
        public void ActivateTextures()
        {
            if (Texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, Texture.Handle);
            }

            if (NormalMap != null)
            {
                GL.ActiveTexture(TextureUnit.Texture2);
                GL.BindTexture(TextureTarget.Texture2D, NormalMap.Handle);
            }

            if (SpecularMap != null)
            {
                GL.ActiveTexture(TextureUnit.Texture3);
                GL.BindTexture(TextureTarget.Texture2D, SpecularMap.Handle);
            }
        }

        /** Destroy game object */
        public virtual void Destroy() {

            GL.DeleteBuffer(vao);
            GL.DeleteBuffer(vb);
            GL.DeleteBuffer(ib);
            
        }

    }
}