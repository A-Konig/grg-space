﻿using System;
using OpenTK;
using World.Core;

namespace World.Objects
{
    /** Class representing a loaded .x file */
    class MeshObject : GameObject
    {
        /** Mesh */
        internal Xmesh Mesh { get => mesh; set => mesh = value; }
        Xmesh mesh;

        /** Scale of the object */
        float scale;
        /** Rotation of the object */
        Vector3 rotate;


        /** Constructor 
            position - Vector3 representing the position of the object in space
            rotation - Vector3 representing the rotation of the object (only Y and X considered)
            scale - scale of the object
            meshPath - path to the .x file
            texturePath - path to the texture of the file
            normalPath - path to the normal map of the file
            */
        public MeshObject(Vector3 position, Vector3 rotation, float scale, string meshPath, string texturePath, string normalPath)
        {
            this.scale = scale;
            Position = position;
            this.rotate = rotation;
            
            Texture = new Texture(texturePath);

            bool tang = false;
            if (normalPath != null)
            {
                tang = true;
                NormalMap = new Texture(normalPath);
            }

            Mesh = new Xmesh(meshPath, tang);
            FindBoundingBox(Mesh.XFile);

            CreateTransform();
        }

        /** Create transform matrix */
        internal void CreateTransform()
        {
            Transform = Matrix4.CreateRotationY(MathHelper.DegreesToRadians(rotate.Y));
            Transform *= Matrix4.CreateRotationX(MathHelper.DegreesToRadians(rotate.X));
            Transform *= Matrix4.CreateScale(scale);
            Transform *= Matrix4.CreateTranslation(Position);
        }

        /** Find bounding box for mesh */
        private void FindBoundingBox(XFile xFile)
        {
            float ymin = float.MaxValue;
            float ymax = float.MinValue;
            float xmin = float.MaxValue;
            float xmax = float.MinValue;
            float zmin = float.MaxValue;
            float zmax = float.MinValue;

            for (int i = 0; i < xFile.Vertex.Length; i++)
            {
                if (xFile.Vertex[i].x > xmax)
                    xmax = xFile.Vertex[i].x;
                if (xFile.Vertex[i].y > ymax)
                    ymax = xFile.Vertex[i].y;
                if (xFile.Vertex[i].z > zmax)
                    zmax = xFile.Vertex[i].z;

                if (xFile.Vertex[i].x < xmin)
                    xmin = xFile.Vertex[i].x;
                if (xFile.Vertex[i].y < ymin)
                    ymin = xFile.Vertex[i].y;
                if (xFile.Vertex[i].z < zmin)
                    zmin = xFile.Vertex[i].z;
            }

            ColliderBox = new Vector3(Math.Abs(xmax - xmin), Math.Abs(ymax - ymin), Math.Abs(zmax - zmin));
            ColliderBox = new Vector3(ColliderBox.X * scale, ColliderBox.Y * scale, ColliderBox.Z * scale);

        }

        /** Initilizing game object */
        public override void Init()
        {
            Mesh.Init();
            SetupTextures();
        }

        /** Render game object */
        public override void Render(RenderPass pass)
        {
            Shader.Use(pass, this);
            ActivateTextures();
            Mesh.Render();
            Shader.Stop();
        }

    }
}
