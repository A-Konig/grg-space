﻿#version 420

layout(location=0)in vec3 position;
layout(location=2)in vec3 normal;
layout(location=8)in vec2 texCoord0;

struct Light {
	vec4 position;
	vec3 attenuation;
	vec3 direction;
	float cutoff;
	mat4 matrix;
	float brightness;
};

// in lights
uniform Light light0;
uniform Light[2] light;

// in matrices
uniform mat4 matrixModel;
uniform mat4 matrixView;
uniform mat4 matrixProjection;

// out lights
out Light flashlight;
out Light[2] lightArr;
// position of fragment from lights point of view
out vec4[3] fragLightPos;
// position from cameras point of view
out vec3 camViewModelPos;

// tex coords and normal
out vec2 texCoordPrimary;
out vec3 surfaceNormal;

void main() {
	// transform position
	vec4 viewModelPos = matrixView * matrixModel * vec4(position, 1.0);
	gl_Position = matrixProjection * viewModelPos;

	// transform normal
	surfaceNormal = mat3( matrixView * matrixModel) * normal;

	texCoordPrimary = texCoord0;
	camViewModelPos = (viewModelPos).xyz;
	
	flashlight = light0;
	lightArr = light;
	
	// transform relevant info for lights into camera space
	flashlight.position = matrixView * flashlight.position;
	flashlight.direction = mat3(matrixView) * flashlight.direction;
	flashlight.cutoff = cos(flashlight.cutoff);
	// transform position into lightcamera window space
	fragLightPos[light.length] = flashlight.matrix * matrixModel * vec4(position, 1);
	
	for (int l = 0; l < light.length; l++) {
		// transform relevant info for lights into camera space
		lightArr[l].position = matrixView * light[l].position;
		lightArr[l].direction = mat3(matrixView) * lightArr[l].direction;
		lightArr[l].cutoff = cos(lightArr[l].cutoff);
		// transform position into lightcamera window space
		fragLightPos[l] = light[l].matrix * matrixModel * vec4(position, 1);
	}
}