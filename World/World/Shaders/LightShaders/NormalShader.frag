﻿#version 420

struct Material {
	int dampening;
	float intensity;
	vec3 specColor;
	sampler2D diffuseTex;
	sampler2D normalTex;
};

struct Light {
	vec4 position;
	vec3 attenuation;
	vec3 direction;
	float cutoff;
	mat4 matrix;
	float brightness;
};

// ambient lighting
uniform float brightness;
// object material
uniform Material material;
// shadow texture
uniform sampler2DArrayShadow shadowTex;

// in lights
in Light flashlight;
in Light[2] lightArr;
// position of fragment from lights point of view (projected)
in vec4[3] fragLightPos;
// position of fragment from cameras point of view
in vec3 camViewModelPos;

// tex coord and normal
in vec2 texCoordPrimary;
in vec3 surfaceNormal;

in vec3 tangentO;

// out color
out vec4 glFragColor;

// compute value of shadow in this fragment, take into account only current light
// positionLight - position of the light
// l - index of the light
float ShadowTest(vec4 positionLight, int l) {
	// look at depth at given position - project into depth
	// depth stored [0, 1]
	// offset 0.01 to prevent shadow acne
	vec3 shadowCoords = (positionLight.xyz-vec3(0,0,0.01))/positionLight.w;
	shadowCoords = shadowCoords * 0.5 + 0.5;
	vec3 size = textureSize(shadowTex,0) / 3.1;

	// sampling more neihgbouring fragments
	// using poissonDisk sampling
	float c = cos(positionLight.length*100);
	float s = sin(positionLight.length*100);
	mat3 rotZ = mat3(c, -s, 0, s, c, 0, 0, 0, 1);

	vec3 poissonDisk[8];
	poissonDisk[0] = rotZ * vec3(-0.613392 / size.x, 0.617481 / size.y, 0);
	poissonDisk[1] = rotZ * vec3(0.170019 / size.x, -0.040254 / size.y, 0);
	poissonDisk[2] = rotZ * vec3(-0.299417 / size.x, 0.791925 / size.y, 0);
	poissonDisk[3] = rotZ * vec3(0.645680 / size.x, 0.493210 / size.y, 0);
	poissonDisk[4] = rotZ * vec3(-0.651784 / size.x, 0.717887 / size.y, 0);
	poissonDisk[5] = rotZ * vec3(0.421003 / size.x, 0.027070 / size.y, 0);
	poissonDisk[6] = rotZ * vec3(-0.817194 / size.x, -0.271096 / size.y, 0);
	poissonDisk[7] = rotZ * vec3(-0.705374 / size.x, -0.668203 / size.y, 0);

	float lightDepth = 0;
	for (int s = 0; s <poissonDisk.length;s++) {
		vec4 coord;
		coord.xyw = shadowCoords.xyz + poissonDisk[s];
		coord.z = l;
		// lookup shadow
		lightDepth += texture(shadowTex, coord);
	}
	
	return lightDepth/poissonDisk.length;
}


// compute the value of lighting at this fragment
// light - light
// normal - normal
// tex1 - diffuse texture
// color - light color
// posLight - index of light
vec4 ComputeLight(Light light, vec3 normal, vec4 tex1, vec4 color, int posLight) {
	float distance = 1;

	vec3 lightDir = normalize(light.position.xyz);
	vec3 viewer = normalize(camViewModelPos);

	float attenuation = light.attenuation.x;

	// if light has position in space (is not a vector)
	if (light.position.w > 0.1)
	{
		// direction from fragment to light
		lightDir = light.position.xyz - camViewModelPos;

		// distance between fragment and light
		distance = length(lightDir);
		lightDir = normalize(lightDir);

		// compute attenuation
		attenuation = 1/(light.attenuation.x + light.attenuation.y * distance + light.attenuation.z * distance * distance);
		
		// angle between direction and light direction
		float angle = dot(-lightDir, normalize(light.direction));
		if (angle < light.cutoff) {
			attenuation = 0;
		}
	}

	// shadows
	float shadow = 1; 
	shadow = ShadowTest(fragLightPos[posLight], posLight);

	// diffuse lighting - diffuse texture times computed lighting
	float diffuseLighting = max(0, dot(normal, lightDir));
	vec4 diffuseTot =  light.brightness *  tex1 * diffuseLighting;

	// specular lighting - specular texture times computed lighting
	vec3 reflected = reflect(lightDir, normal);
	float specularLighting = pow(max(0, dot(reflected, viewer)), material.dampening);
	vec4 specularTot = vec4(material.specColor, 1) * vec4(specularLighting * tex1);
	
	vec4 res = ((diffuseTot + specularTot) * attenuation * shadow * color);
	return res;
}

// main
void main() {
	// 0 - color of flashlight, 1 - color of lights in rooms
	vec4[2] colors;
	colors[0] = vec4(1, 1, 1, 0);
	colors[1] = vec4(0.6, 0.2, 0.2, 0);
	
	// ambient lighting
	vec4 ambient = vec4(brightness);

	// read normal from normalmap
	vec3 normal = texture(material.normalTex, texCoordPrimary).xyz * 2 - 1;
	normal = normalize(normal);
	// read diffuse texture
	vec4 tex1 = texture(material.diffuseTex, texCoordPrimary);

	// compute light
	glFragColor = ambient * tex1;
	if (flashlight.brightness > 0)
		glFragColor += ComputeLight(flashlight, normal, tex1, colors[0], fragLightPos.length-1);
	for (int l = 0; l < lightArr.length; l++)
		glFragColor += ComputeLight(lightArr[l], normal, tex1, colors[1], l);
	
}