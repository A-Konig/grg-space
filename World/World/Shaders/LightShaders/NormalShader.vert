﻿#version 420

layout(location=0)in vec3 position;
layout(location=2)in vec3 normal;
layout(location=4)in vec3 tangent;
layout(location=8)in vec2 texCoord0;

struct Light {
	vec4 position;
	vec3 attenuation;
	vec3 direction;
	float cutoff;
	mat4 matrix;
	float brightness;
};

// in lights
uniform Light light0;
uniform Light[2] light;

// in matrices
uniform mat4 matrixModel;
uniform mat4 matrixView;
uniform mat4 matrixProjection;

// out lights
out Light flashlight;
out Light[2] lightArr;
// position of fragment from lights point of view
out vec4[3] fragLightPos;
// position from cameras point of view
out vec3 camViewModelPos;
// tex coords and normal
out vec2 texCoordPrimary;
out vec3 surfaceNormal;

out vec3 tangentO;


void main() {
	tangentO = tangent;

	// transform position
	vec4 viewModelPos = matrixView * matrixModel * vec4(position, 1.0);
	gl_Position = matrixProjection * viewModelPos;

	// compute surface normal
	surfaceNormal = mat3(matrixView * matrixModel) * normal;
	texCoordPrimary = texCoord0;
	
	// compute bitangent
	vec3 bitangent = cross(tangent, normal);
	mat4 modelView4 = matrixView * matrixModel;
	mat3 modelView = mat3(matrixView * matrixModel);

    // define matrix that will transform vectors into tangent space
    vec3 T = normalize(modelView * tangent);
    vec3 B = normalize(modelView * bitangent);
    vec3 N = normalize(modelView * normal);
	mat3 TBN = transpose(mat3(T, B, N));

	vec4 T4 = normalize((modelView4 * vec4(tangent,   0.0)));
	vec4 B4 = normalize((modelView4 * vec4(bitangent, 0.0)));
	vec4 N4 = normalize((modelView4 * vec4(normal,    0.0)));
	mat4 TBN4 = transpose(mat4(T4, B4, N4, vec4(0,0,0,1)));

	// transform into tangent space
	camViewModelPos = TBN * (viewModelPos).xyz;

	flashlight = light0;
	lightArr = light;
	
	// transform relevant info for lights into tangent space
	flashlight.position = TBN4 * matrixView * flashlight.position;
	flashlight.direction = mat3(TBN4 * matrixView) * flashlight.direction;
	flashlight.cutoff = cos(flashlight.cutoff);
	// transform position into lightcamera window space
	fragLightPos[light.length] =  flashlight.matrix * matrixModel * vec4(position, 1);

	for (int l = 0; l < light.length; l++) {
		// transform relevant info for lights into tangent space
		lightArr[l].position = TBN4 * matrixView * light[l].position;
		lightArr[l].direction = mat3(TBN4 * matrixView) * lightArr[l].direction;
		lightArr[l].cutoff = cos(lightArr[l].cutoff);
		// transform position into lightcamera window space
		fragLightPos[l] =  light[l].matrix * matrixModel * vec4(position, 1);
	}
}