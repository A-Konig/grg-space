﻿#version 420
layout(location=0)in vec3 position;
layout(location=2)in vec3 normal;
layout(location=8)in vec2 texCoord0;

out vec3 normalEye;
out vec2 texCoordPrimary;

uniform mat4 matrixModel;
uniform mat4 matrixView;
uniform mat4 matrixProjection;

void main() {
	
	gl_Position = matrixProjection * matrixView * matrixModel * vec4(position, 1.0);

	// normal from eyes view
	normalEye = mat3(matrixView * matrixModel) * normal;
	texCoordPrimary = texCoord0;
}