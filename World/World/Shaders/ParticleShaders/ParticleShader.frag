﻿#version 420
uniform sampler2D sprite;

in vec2 texCoordPrimary;

out vec4 glFragColor;

void main() {
    vec4 color = texture(sprite, texCoordPrimary);
    glFragColor = color;
    
    // discard if transparent
    if (color.a < 0.01)
        discard;

    glFragColor = vec4(vec3(color), color.a);
}