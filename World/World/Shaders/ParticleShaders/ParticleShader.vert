﻿#version 400
layout(location=0)in vec3 position;
layout(location=1)in vec3 velocity;
layout(location=2)in float life;
layout(location=3)in float id;
layout(location=4)in float lmax;

// life of particle
out float lifeC;

void main() {
	lifeC = life;

	gl_Position = vec4(position,1);
}